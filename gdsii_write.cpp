/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include "layout_shapes.h"
#include "gdsii.h"
#include <assert.h>
#include <float.h>
#include <math.h>
#include <set>
#include <string.h>

using namespace liblayout;

/************************************************************************
  Correct problem with MSVC 6
************************************************************************/

#ifndef M_PI_2
#define M_PI_2 1.5707963267949
#endif

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

/************************************************************************
  These routines are used to write integral types
************************************************************************/

static void write_short( std::ostream& out, short value )
{
	out.put( (value >> 8) & 0xFF );
	out.put( value & 0xFF );
}

static void write_string( std::ostream& out, char const* value )
{
	assert( value );
	out << value;
	if ( strlen(value) % 2 ) out.put( '\0' );
}

static void write_long( std::ostream& out, long value )
{
	out.put( (value >> 24) & 0xFF );
	out.put( (value >> 16) & 0xFF );
	out.put( (value >> 8) & 0xFF );
	out.put( value & 0xFF );
}

static void write_double( std::ostream& out, double value )
{
	int exponent;
	double mantissa;

	/* find the exponent and mantissa */
	double in_num = fabs(value);
	if ( fabs(in_num) < DBL_EPSILON )
 	{
 		exponent = 0;
		mantissa = 0;
	}
	else
	{
		for ( exponent = -64; exponent < 64; ++exponent)
 		{
			mantissa = in_num / pow( (double)16, exponent);
			if ( mantissa <= 1 ) break;
		}
	}

	/* GDSII used an "excess 64" notation for the exponent */
	exponent += 64;
	/* if it's less than 0, set the high bit */
	if ( value < 0 ) exponent |= 0x80;

	/* find the mantissa as the numerator of a binary fraction */
    mantissa *= pow ( (double)2, 56);
    /* we can consider it an int now, but it has to be long long (64 bit)
     * for the real_8 type */
    double /*long long*/ int_mantissa = mantissa;
	/* write out the exponent */
    out.put(exponent);
    /* write the mantissa one byte at a time */
    for ( int mantissa_ktr = 6; mantissa_ktr >= 0; mantissa_ktr--)
	{
		/* make a temp copy and shift it the app. number of bytes */
        double /*long long*/ part_mantissa = int_mantissa;
        for (int sm_ktr = 0; sm_ktr < mantissa_ktr; sm_ktr++)
        {
			//part_mantissa >>= 8;
			part_mantissa /= pow( (double)2,8);
        }
        /* mask off all but the right byte */
        part_mantissa = fmod( part_mantissa, 0x100 );
        /* putchar doesn't seem to like long long */
        out.put( (unsigned char) part_mantissa );
	}
}

/************************************************************************
  Utility function
************************************************************************/

static void get_layouts( std::set<layout const*>& layouts, layout const& view )
{
	if ( layouts.find(&view) != layouts.end() ) return;

	// OK, add the layout
	layouts.insert( &view );

	layout::const_composite_iterator lp;
	for ( lp = view.composites_begin(); lp != view.composites_end(); ++lp )
	{
		layout_composite const* ptr = dynamic_cast<layout_composite const*>( lp->get() );
		if ( ptr ) {
			assert( ptr->get_layout() );
			get_layouts( layouts, *ptr->get_layout() );
		}
	};
}

/************************************************************************
  These routines are used to write out GDSII records
************************************************************************/

static void export_sname_record( std::ostream& out, char const* name )
{
	short sl = strlen( name );
	if ( sl % 2 ) sl += 1;

	write_short( out, 4 + sl );
	write_short( out, SNAME );
	write_string( out, name );
}

static void export_strname_record( std::ostream& out, char const* name )
{
	size_t len = strlen(name);
	if ( len % 2 ) len += 1;

	write_short( out, 4 + len );
	write_short( out, STRNAME );
	write_string( out, name );
}

static void export_boundary_record( std::ostream& out, layer_id layer, polygon const& view )
{
	write_short( out, 4 );
	write_short( out, BOUNDARY );

	write_short( out, 6 );
	write_short( out, LAYER );
	write_short( out, layer );

	write_short( out, 4 + view.size() * 8 );
	write_short( out, XY );
	for ( int lp = 0; lp < view.size(); ++lp )
	{
		write_long( out, view[lp].x );
		write_long( out, view[lp].y );
	}

	write_short( out, 4 );
	write_short( out, ENDEL );
}

static void calc_gdsii_transform( transform const& t, float& scaling, float& angle, bool& mirror_y )
{
	assert( t.is_manhattan() );
	
	// get the scaling from the transform
	scaling = t.get_manhattan_scaling();
	
	// get any rotation/reflections
	switch ( t.get_manhattan_transform() ) {
		case R0:
			angle = 0;
			mirror_y = false;
			break;
			
		case R90:
			angle = M_PI_2;
			mirror_y = false;
			break;
			
		case R180:
			angle = M_PI;
			mirror_y = false;
			break;
			
		case R270:
			angle = 1.5 * M_PI;
			mirror_y = false;
			break;
			
		case MY:
			angle = 0;
			mirror_y = true;
			break;
			
		case MYR90:
			angle = M_PI_2;
			mirror_y = true;
			break;
			
		case MX:
			angle = M_PI;
			mirror_y = true;
			break;
			
		case MXR90:
			angle = 1.5*M_PI;
			mirror_y = true;
			break;
			
		default:
			assert( false );
	}
}

static void export_sref_records( std::ostream& out, layout_composite const& view )
{
	write_short( out, 2 );
	write_short( out, SREF );

	export_sname_record( out, view.get_layout()->get_name() );

	float scaling, angle;
	bool mirror_y;
	calc_gdsii_transform( view.get_transform(), scaling, angle, mirror_y );

	write_short( out, 6 );
	write_short( out, STRANS );
	write_short( out, mirror_y ? 0x8000 : 0 );
			
	write_short( out, 12 );
	write_short( out, MAG );
	write_double( out, scaling );

	write_short( out, 12 );
	write_short( out, ANGLE );
	write_double( out, angle );

	write_short( out, 12 );
	write_short( out, XY );
	write_long( out, lround( view.get_transform()(0,2) ) );
	write_long( out, lround( view.get_transform()(1,2) ) );

	write_short( out, 4 );
	write_short( out, ENDEL );
}

static void export_aref_records( std::ostream& out, layout_composite const& view )
{
	write_short( out, 2 );
	write_short( out, AREF );

	// interpret SNAME
	export_sname_record( out, view.get_layout()->get_name() );

	float scaling, angle;
	bool mirror_y;
	calc_gdsii_transform( view.get_transform(), scaling, angle, mirror_y );

	write_short( out, 6 );
	write_short( out, STRANS );
	write_short( out, mirror_y ? 0x8000 : 0 );
			
	write_short( out, 12 );
	write_short( out, MAG );
	write_double( out, scaling );

	write_short( out, 12 );
	write_short( out, ANGLE );
	write_double( out, angle );

	write_short( out, 8 );
	write_short( out, COLROW );
	write_short( out, view.get_cols() );
	write_short( out, view.get_rows() );

	write_short( out, 4 + 6*4 );
	write_short( out, XY );
	write_long( out, lround( view.get_transform()(0,2) ) );
	write_long( out, lround( view.get_transform()(1,2) ) );
	write_long( out, lround( view.get_transform()(0,2) + view.get_delta_c().x * view.get_cols() ) );
	write_long( out, lround( view.get_transform()(1,2) + view.get_delta_c().y * view.get_cols() ) );
	write_long( out, lround( view.get_transform()(0,2) + view.get_delta_r().x * view.get_rows() ) );
	write_long( out, lround( view.get_transform()(1,2) + view.get_delta_r().y * view.get_rows() ) );

	write_short( out, 4 );
	write_short( out, ENDEL );
}

static void export_bgnstr_records_shape( std::ostream& out, shape const& s )
{
	polygon_shape const* poly;

	if ( poly = dynamic_cast<polygon_shape const*>( s.get() ) )
	{
		export_boundary_record( out, s.layer(), *poly );
	}
	else
	{
		polygon p;
		s.get_polygon( p );
		export_boundary_record( out, s.layer(), p );
	}
}

static void export_bgnstr_records( std::ostream& out, layout const& view )
{
	// create the BGNSTR record
	write_short( out, 4 );
	write_short( out, BGNSTR );
	export_strname_record( out, view.get_name() );

	layout::const_shape_iterator slp;
	for ( slp = view.shapes_begin(); slp != view.shapes_end(); ++slp )
	{
		export_bgnstr_records_shape( out, *slp );
	}

	layout::const_composite_iterator clp;
	for ( clp = view.composites_begin(); clp != view.composites_end(); ++clp )
	{
		layout_composite const* layout;

		if ( layout = dynamic_cast<layout_composite const*>( clp->get() ) )
		{
			if ( layout->get_cols() > 1 || layout->get_rows() > 1 )
				export_aref_records( out, *layout );
			else
				export_sref_records( out, *layout );
		}
		else
		{
			std::vector<shape> shapes;
			clp->get_shapes(shapes);
			std::vector<shape>::const_iterator lp;
			for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
				export_bgnstr_records_shape( out, *lp );
		}
	}

	// write out end of the structures
	write_short( out, 4 );
	write_short( out, ENDSTR );
}

/************************************************************************
  These routines are the top level routines
************************************************************************/

void gdsii::write( layout const& view, double scale, std::ostream& out )
{
	assert( scale > 0 );

	std::set<layout const*> layouts;

	// get a set of all the necessary layouts
	get_layouts( layouts, view );

	// write out first record
	write_short( out, 6 );
	write_short( out, HEADER );
	write_short( out, 0 );

	// write out beginining of library
	write_short( out, 4 );
	write_short( out, BGNLIB );

	// write out library name
	short reclen = 4 + strlen( view.get_name() );
	if ( reclen % 2 ) reclen+=1;
	write_short( out, reclen );
	write_short( out, LIBNAME );
	write_string( out, view.get_name() );	

	// write out UNITS
	write_short( out, 20 );
	write_short( out, UNITS );
	write_double( out, scale * 100 );
	write_double( out, scale );

	std::set<layout const*>::const_iterator lp;
	for ( lp = layouts.begin(); lp != layouts.end(); ++lp )
		export_bgnstr_records( out, **lp );

	// write out the end of the library
	write_short( out, 4 );
	write_short( out, ENDLIB );
}
