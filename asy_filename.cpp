/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Export to ASY format.
** http://sourceforge.net/projects/asymptote/
**
*/

#include "layout_export.h"
#include <fstream>
#include <stdexcept>

using namespace liblayout;

void	asy::write( layout const& view, map_id_style& layer_map, double mag, char const* filename )
{
	std::ofstream file;

	// create the file
	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	asy::write( view, layer_map, mag, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the ASY file." );
	}
}

void	asy::write( layout const& view, layer_id layer, map_id_style& layer_map, double mag, char const* filename )
{
	std::ofstream file;

	// create the file
	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	asy::write( view, layer, layer_map, mag, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the ASY file." );
	}
}
