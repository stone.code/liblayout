/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>
#include <math.h>

using namespace liblayout;

point transform::operator*( point pt ) const
{
	// check preconditions
	assert( matrix[2][0] == 0 );
	assert( matrix[2][1] == 0 );
	assert( matrix[2][2] == 1 );

	// construct the return value
	return point(
		lround( matrix[0][0] * pt.x + matrix[0][1] * pt.y + matrix[0][2] ),
		lround( matrix[1][0] * pt.x + matrix[1][1] * pt.y + matrix[1][2] )
	);
}

rect transform::operator*( rect const& r ) const
{
	// can only do manhattan transforms
	assert( is_manhattan() );
	point pt1 = operator*( point( r.x1, r.y1) );
	point pt2 = operator*( point( r.x2, r.y2) );
	
	rect ret( pt1.x, pt1.y, pt2.x, pt2.y );
	ret.normalize();
	return ret;
}

polygon transform::operator*( polygon const& p ) const
{
	polygon ret;
	ret.reserve( p.size() );

	for ( polygon::const_iterator lp = p.begin(); lp != p.end(); ++lp )
		ret.push_back( (*this) * (*lp) );

	return ret;
}

void transform::apply( polygon& p ) const
{
	for ( polygon::iterator lp = p.begin(); lp != p.end(); ++lp )
		*lp = (*this) * (*lp);
}
