/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>
#include <stdexcept>
#include <iostream>

using namespace liblayout;

layout_composite::layout_composite( layout* l, transform const& t )
: layout_(l), t_(t), delta_c(1,0), delta_r(0,1)
{
	rows = 1;
	cols = 1;
	
	assert( valid() );
}

layout_composite::layout_composite( layout* l, transform const& t, int rows_, int cols_, point delta_r_, point delta_c_ )
: layout_(l), t_(t), delta_r( delta_r_), delta_c( delta_c_ )
{
	rows = rows_;
	cols = cols_;

	assert( valid() );
}

rect layout_composite::bounds() const
{
	assert( rows > 0 );
	assert( cols > 0 );

	if ( !layout_ ) throw std::runtime_error( "Null pointer." );

	// get bounds for the layout
	rect r = layout_->bounds();
	assert( r.valid() );
	
	// transform the bounds
	if ( t_.is_manhattan() ) {
		r = t_ * r;
	}
	else {
		// these points are the four corners of the bounds
		point a( r.x1, r.y1 );
		point b( r.x1, r.y2 );
		point c( r.x2, r.y1 );
		point d( r.x2, r.y2 );
		
		// use the transform
		a = t_ * a;
		b = t_ * b;
		c = t_ * c;
		d = t_ * d;

		// construct a rectangle enclosing the four points
		r = rect( a.x, a.y, a.x, a.y );
		r += b; r += c; r += d;
	}
	
	// use the row and column offsets to update the bounds
	if ( delta_r.x > 0 ) r.x2 += (rows-1) * delta_r.x;
	if ( delta_r.x < 0 ) r.x1 += (rows-1) * delta_r.x;
	if ( delta_r.y > 0 ) r.y2 += (rows-1) * delta_r.y;
	if ( delta_r.y < 0 ) r.y1 += (rows-1) * delta_r.y;
	if ( delta_c.x > 0 ) r.x2 += (cols-1) * delta_c.x;
	if ( delta_c.x < 0 ) r.x1 += (cols-1) * delta_c.x;
	if ( delta_c.y > 0 ) r.y2 += (cols-1) * delta_c.y;
	if ( delta_c.y < 0 ) r.y1 += (cols-1) * delta_c.y;

	// and we are done
	assert( r.valid() );
	return r;	
}

composite::imp* layout_composite::eval( transform const& t ) const
{
	return new layout_composite( layout_, t * t_, rows, cols, delta_r, delta_c );
}

bool layout_composite::has_bounds() const
{
	assert( layout_ );
	return layout_->has_bounds();
}

int layout_composite::contains_point( point pt ) const
{
	assert( layout_ );
	return layout_->contains_point( pt );
}

void layout_composite::get_layers( std::set<layer_id>& lhs ) const
{
	assert( layout_ );
	layout_->get_layers( lhs );
}

void layout_composite::get_shapes( std::vector<shape>& lhs ) const
{
	assert( layout_ );
	assert( rows>0 && cols>0 );

	if ( rows==1 && cols == 1 )
	{
		size_t size = lhs.size();
		layout_->get_shapes( lhs );
		for ( size_t lp = size; lp < lhs.size(); ++lp )
			lhs[lp] = lhs[lp].transform( t_ );
	}
	else
	{
		int lpx, lpy;

		for ( lpx=0; lpx<cols; ++lpx )
		for ( lpy=0; lpy<rows; ++lpy )
		{
			size_t size = lhs.size();
			layout_->get_shapes( lhs );
			
			transform t( t_ );
			t.translate( lpx*delta_c.x, lpx*delta_c.y );
			t.translate( lpy*delta_r.x, lpy*delta_r.y );

			for ( size_t lp = size; lp < lhs.size(); ++lp )
				lhs[lp] = lhs[lp].transform( t );
		}
	}
}

void layout_composite::get_shapes( std::vector<shape>& lhs, layer_id layer ) const
{
	assert( layout_ );
	assert( rows>0 && cols>0 );

	if ( rows==1 && cols == 1 )
	{
		size_t size = lhs.size();
		layout_->get_shapes( lhs, layer );
		for ( size_t lp = size; lp < lhs.size(); ++lp )
			lhs[lp] = lhs[lp].transform( t_ );
	}
	else
	{
		int lpx, lpy;

		for ( lpx=0; lpx<cols; ++lpx )
		for ( lpy=0; lpy<rows; ++lpy )
		{
			size_t old_size = lhs.size();
			layout_->get_shapes( lhs, layer );

			transform t( t_ );
			t.translate( lpx*delta_c.x, lpx*delta_c.y );
			t.translate( lpy*delta_r.x, lpy*delta_r.y );

			for ( size_t lp = old_size; lp < lhs.size(); ++lp )
				lhs[lp] = lhs[lp].transform( t );
		}
	}
}

void layout_composite::set_cols( int r )
{
	if ( r < 1 ) throw std::logic_error( "In a layout array composite, the number of rows must be at least 1." );
	rows = r;
}

void layout_composite::set_rows( int c )
{
	if ( c < 1 ) throw std::logic_error( "In a layout array composite, the number of columns must be at least 1." );
	cols = c;
}

void layout_composite::set_spacing( point dr, point dc )
{
	delta_r = dr;
	delta_c = dc;
}

bool layout_composite::valid() const
{
	if ( !layout_ ) return false;
	if ( cols<=0 ) return false;
	if ( rows<=0 ) return false;
	if ( rows>1 && delta_r.x==0 && delta_r.y==0 ) return false;
	if ( cols>1 && delta_c.x==0 && delta_c.y==0 ) return false;
	if ( !t_.valid() ) return false;
	
	return true;
}



