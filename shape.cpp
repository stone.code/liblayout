/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include "layout_shapes.h"
#include <assert.h>

using namespace liblayout;

polygon shape::get_polygon() const
{
	polygon p;
	assert( ptr );
	ptr->get_polygon( p );
	return p;
}

void shape::imp::sub_ref()
{
	assert( ref_count>0 );
	--ref_count;
	if ( ref_count==0 ) delete this;
}

