/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include <assert.h>
#include <errno.h>
#include <fstream>
#include <iostream>
#include <stdio.h>

using namespace liblayout;

short psmask::font_size = 12;

#define INCHES_PER_METER (39.37)

static bool get_darkfield( psmask::map_id_darkfield const& id_to_df, layer_id l )
{
	psmask::map_id_darkfield::const_iterator lp = id_to_df.find(l);
	if ( lp == id_to_df.end() ) return false;
	return lp->second;
}

static char const* get_layername( psmask::map_id_name const& id_to_name, layer_id l )
{
	psmask::map_id_name::const_iterator lp = id_to_name.find(l);
	if ( lp == id_to_name.end() ) return 0;
	return lp->second.c_str();
}

static void write_header( std::ostream& out, char const* title, int pages )
{
	out << "%!PS-Adobe-2.0\n";
	out << "%%Creator:  " PACKAGE_NAME " " PACKAGE_VERSION "\n";
	if ( title && *title ) out << "%%Title:  " << title << '\n';
	if ( pages>0 ) out << "%%Pages:  " << pages << '\n';
	out << "%%EndComments\n";
	
	out << "%%BeginProlog\n";
	out << "/page-begin { gsave 0 setgray /Helvetica-Bold findfont " << psmask::font_size << " scalefont setfont } def\n";
	out << "/page-end { grestore showpage } def\n";
	out << "/page-header { exch " << psmask::font_size << ' ' << psmask::font_size << " moveto show\n";
	out << " dup stringwidth pop currentpagedevice /PageSize get 0 get exch sub " << psmask::font_size << " sub " << psmask::font_size*1.5 << " moveto show } def\n";
	out << "/set-darkfield { 0 0 moveto\n";
	out << " currentpagedevice /PageSize get 0 get 0 lineto\n";
	out << " currentpagedevice /PageSize get 0 get currentpagedevice /PageSize get 1 get lineto\n";
	out << " 0 currentpagedevice /PageSize get 1 get lineto\n";
	out << " closepath fill 1 setgray } def\n";
	out << "%%EndProlog\n";
}

static void write_scalefunc( std::ostream& out, rect const& bounds, double scale )
{
	assert( bounds.valid() );
	assert( scale > 0 );

	double s = scale * 72 /*points per inch*/ * INCHES_PER_METER;

	out << "/page-scale { currentpagedevice /PageSize get dup\n";
	out << "1 get 2 div exch 0 get 2 div exch translate\n"; // center the page
	out << s << ' ' << s << " scale\n"; // set the scaling
	out << -bounds.center_x() << ' ' << -bounds.center_y() << " translate } def\n"; // offset to center of layout
}

static void write_footer( std::ostream& out )
{
	out << "%%EOF" << std::endl;
}

static void write_shape( std::ostream& file, shape const& view )
{
	// convert to a polygon
	polygon p;
	view.get_polygon( p );

	polygon::const_iterator lp = p.begin();
	file << lp->x << ' ' << lp->y << " moveto %% polygon " << p.size() << '\n';
	for ( ++lp; lp != p.end(); ++lp ) {
		file << lp->x << ' ' << lp->y << " lineto\n";
	}
	file << "closepath fill\n";
}

static void create_page( std::ostream& out, layout const& view, layer_id layer, bool darkfield, char const* page_title, short page_ndx )
{
	assert( page_ndx > 0 );

	// collect the shapes to be printed on page
	std::vector<shape> shapes;
	view.get_shapes( shapes, layer );
	std::vector<shape>::const_iterator shape;
		
	// print out page header
	out << "%%Page:  " << page_ndx << ' ' << page_ndx << std::endl;
	out << "%%BeginPageSetup\n";
	out << "page-begin\n";
	if( darkfield ) out << "set-darkfield\n";
	out << "%%EndPageSetup\n";
	out << '(' << (view.get_name() ? view.get_name() : "") << ") ";
	if ( page_title )
		out << '(' << page_title << ") page-header\n";
	else
		out << "(ID# " << layer << ") page-header\n";
	out << "page-scale\n";
	
	// write out shapes
	for ( shape = shapes.begin(); shape != shapes.end(); ++shape ) {
		write_shape( out, *shape );
	}

	// finish the page
	out << "page-end" << std::endl;
}

void psmask::write( layout const& view, map_id_darkfield const& id_to_df, map_id_name const& id_to_name, double scale, std::ostream& out )
{
	assert( scale>0 );

	std::vector<layer_id> layers;
	view.get_layers( layers );
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// create the output
	write_header( out, view.get_name(), layers.size() /* pages */ );
	write_scalefunc( out, bounds, scale );
		
	// write out the masks
	int page = 1;
	std::vector<layer_id>::const_iterator layer;
	for( layer = layers.begin(); layer != layers.end(); ++layer, ++page ) {
		create_page( out, view, *layer, get_darkfield(id_to_df,*layer),  get_layername(id_to_name,*layer), page );
	}
		
	// create the file header
	write_footer( out );
}

void psmask::write( layout const& view, layer_id layer, map_id_darkfield const& id_to_df, map_id_name const& id_to_name, double scale, std::ostream& out )
{
	assert( scale > 0 );

	rect bounds = view.bounds();
	assert( bounds.valid() );

	// create the output
	write_header( out, view.get_name(), 1 /* pages */ );
	write_scalefunc( out, bounds, scale );

	// create the page	
	create_page( out, view, layer, get_darkfield(id_to_df,layer),  get_layername(id_to_name,layer), 1 );
		
	// create the file header
	write_footer( out );
}
