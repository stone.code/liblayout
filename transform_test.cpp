/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include "vendor/catch.hpp"

using liblayout::transform;
using liblayout::manhattan_transform;
using namespace liblayout;

TEST_CASE( "transform" )
{
	REQUIRE( transform(R0).is_manhattan() );
	REQUIRE( transform(R90).is_manhattan() );
	REQUIRE( transform(R180).is_manhattan() );
	REQUIRE( transform(R270).is_manhattan() );
	REQUIRE( transform(MX).is_manhattan() );
	REQUIRE( transform(MXR90).is_manhattan() );
	REQUIRE( transform(MY).is_manhattan() );
	REQUIRE( transform(MYR90).is_manhattan() );

	REQUIRE( transform(R0).get_manhattan_transform()==R0 );
	REQUIRE( transform(R90).get_manhattan_transform()==R90 );
	REQUIRE( transform(R180).get_manhattan_transform()==R180 );
	REQUIRE( transform(R270).get_manhattan_transform()==R270 );
	REQUIRE( transform(MX).get_manhattan_transform()==MX );
	REQUIRE( transform(MXR90).get_manhattan_transform()==MXR90 );
	REQUIRE( transform(MY).get_manhattan_transform()==MY );
	REQUIRE( transform(MYR90).get_manhattan_transform()==MYR90 );

	REQUIRE( transform(R0).get_manhattan_scaling()==1 );
	REQUIRE( transform(R90).get_manhattan_scaling()==1 );
	REQUIRE( transform(R180).get_manhattan_scaling()==1 );
	REQUIRE( transform(R270).get_manhattan_scaling()==1 );
	REQUIRE( transform(MX).get_manhattan_scaling()==1 );
	REQUIRE( transform(MXR90).get_manhattan_scaling()==1 );
	REQUIRE( transform(MY).get_manhattan_scaling()==1 );
	REQUIRE( transform(MYR90).get_manhattan_scaling()==1 );
	
	REQUIRE( transform(R90) * transform(R90) == transform(R180) );

}
