/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Export to ASY format.
** http://sourceforge.net/projects/asymptote/
**
*/

#include "layout_export.h"
#include <assert.h>
#include <iostream>

using namespace liblayout;

static void initialize_pens( std::ostream& out, style const& css )
{
	if ( css.has_stroke() ) {
		// extract paint information
		short red = css.stroke.red();
		short green = css.stroke.green();
		short blue = css.stroke.blue();
		short alpha = css.stroke.alpha();
		assert( alpha < 0xFF );
		
		// set pen colour
		out << "drawpen = rgb(" << red << ',' << green << ',' << blue << ") + solid";
		
		// set pen width
		// stroke width less than zero indicates default for driver
		if( css.stroke_width >= 0 )
			out << " + " << css.stroke_width;
		
		// set pen join
		switch ( css.stroke_join ) {
			case style::join_miter: out << " + miterjoin"; break;
			case style::join_round: out << " + roundjoin"; break;
			case style::join_bevel: out << " + beveljoin"; break;
			default: assert( false ); break;
		};
		
		// set pen style
		switch ( css.stroke_pattern ) {
			case style::stroke_solid: out << " + solid"; break;
			case style::stroke_dotted: out << " + dotted"; break;
			case style::stroke_dashed: out << " + dashed"; break;
			case style::stroke_longdashed: out << " + longdashed"; break;
			case style::stroke_dashdotted: out << " + dashdotted"; break;
			case style::stroke_longdashdotted: out << " + longdashdotted"; break;
			default: assert( false ); break;
		}
		
		// and complete pen
		out << ";\n";
	}
	else {
		out << "drawpen = invisible;\n";
	}

	if ( css.has_fill() ) {
		// extract paint information
		short red = css.fill.red();
		short green = css.fill.green();
		short blue = css.fill.blue();
		short alpha = css.fill.alpha();
		assert( alpha < 0xFF );
		
		// set pen colour
		out << "fillpen = rgb(" << red << ',' << green << ',' << blue << ");\n";
	}
	else {
		out << "fillpen = invisiblepen;\n";
	}
}

static void initialize_pens_not_found( std::ostream& out )
{
	out << "drawpen = black + beveljoin;\n";
	out << "fillpen = gray;\n";
}

static void write_header( std::ostream& out, char const* title )
{
	out << "// Layout title " << title << '\n';
	out << "// Conversion of layout data by " PACKAGE_NAME " " PACKAGE_VERSION "\n";
	out << "pen drawpen;\npen fillpen;\npath p;\n";
}

static void write_footer( std::ostream& out )
{
	out << "// End of layout geometry\n";
}

static void write_shape( std::ostream& out, shape const& view )
{
	polygon p;
	view.get_polygon( p );

	polygon::const_iterator lp = p.begin();
	out << "p = (" << lp->x << ',' << lp->y << ')';
	for ( ++lp; lp != p.end(); ++lp ) {
		out << "--(" << lp->x << ',' << lp->y << ')';
	}
	out << "--cycle;\n";
	out << "filldraw( p, fillpen, drawpen );\n";
}

void asy::write( layout const& view, map_id_style& layer_map, double mag, std::ostream& out )
{
	assert( mag >= 0 );

	// get size
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// write out the header
	write_header( out, view.get_name() );
	if ( mag > 0 )
		out << "size(" << bounds.width() * mag * 1000 << "mm,0)\n";

	// get the layers
	std::vector<layer_id> layers;
	view.get_layers( layers );
	std::vector<layer_id>::const_iterator layer;

	for( layer = layers.begin(); layer != layers.end(); ++layer ) {
		out << "// layer id = " << *layer << "\n";

		// get the shapes
		std::vector<shape>	shapes;
		view.get_shapes( shapes, *layer );

		// set the pens
		svg::map_id_style::const_iterator css = layer_map.find( *layer );
		if ( css == layer_map.end() ) 
			initialize_pens_not_found( out );
		else 
			initialize_pens( out, css->second );

		// draw the shapes
		std::vector<shape>::const_iterator lp;
		for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
		{
			write_shape( out, *lp );
		}
	}
	
	// write out the footer
	write_footer( out );
}

void asy::write( layout const& view, layer_id layer, map_id_style& layer_map, double mag, std::ostream& out )
{
	assert( mag >= 0 );

	// get size
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// write out the header
	write_header( out, view.get_name() );
	if ( mag > 0 )
		out << "size(" << bounds.width() * mag * 1000 << "mm,0)\n";

	// get the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes, layer );

	// set the pens
	svg::map_id_style::const_iterator css = layer_map.find( layer );
	if ( css == layer_map.end() ) 
		initialize_pens_not_found( out );
	else 
		initialize_pens( out, css->second );

	// draw the shapes
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		write_shape( out, *lp );
	}
	
	// write out the footer
	write_footer( out );
}

