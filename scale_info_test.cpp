/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2015 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains the methods associated with the scale_info struct.
**
*/

#include "layout.h"
#include "layout_export.h"
#include "vendor/catch.hpp"

using liblayout::rect;
using liblayout::scale_info;

TEST_CASE( "scale_info no scaling" )
{
	liblayout::scale_info test = { 0, 2, 3};
	liblayout::scale_info tmp;
	
	// check fill style
	tmp = test.complete_fields( rect( 0, 0, 100, 100 ) );
	REQUIRE( tmp.scale == 0.02 );
	REQUIRE( tmp.width == 2 );
	REQUIRE( tmp.height == 3 );
	
	tmp = test.complete_fields( rect( 0, 0, 100, 300 ) );
	REQUIRE( tmp.scale == 0.01 );
	REQUIRE( tmp.width == 2);
	REQUIRE( tmp.height == 3 );
}

TEST_CASE( "scale_info partial size" )
{
	liblayout::scale_info test = { 0, 0, 3};
	liblayout::scale_info tmp;
	
	// check fill style
	tmp = test.complete_fields( rect( 0, 0, 100, 100 ) );
	REQUIRE( tmp.scale == 0.03 );
	REQUIRE( tmp.width == 3 );
	REQUIRE( tmp.height == 3 );
	
	std::swap( test.width, test.height );
	tmp = test.complete_fields( rect( 0, 0, 100, 100 ) );
	REQUIRE( tmp.scale == 0.03 );
	REQUIRE( tmp.width == 3 );
	REQUIRE( tmp.height == 3 );
}

TEST_CASE( "scale_info no data")
{
	double const default_size = 0.25;

	liblayout::scale_info zero = { 0, 0, 0 };
	liblayout::scale_info tmp;

	tmp = zero.complete_fields( rect( 0, 0, 100, 300 ), default_size, default_size );
	REQUIRE( tmp.scale == default_size/300 );
	REQUIRE( tmp.height == default_size );
	REQUIRE( tmp.width == default_size );
	tmp = zero.complete_fields( rect( 0, 0, 300, 100 ), default_size, default_size );
	REQUIRE( tmp.scale == default_size/300 );
	REQUIRE( tmp.width == default_size );
	REQUIRE( tmp.height == default_size );

}
