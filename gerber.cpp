/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2007-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains wrappers for the Gerber export routines.  These
** functions overload equivalent functions, except that they take a 
** null terminated string, open a file, and then replace the filename
** with the ostream.
**
*/

#include "layout_export.h"
#include <assert.h>
#include <ctype.h>
#include <iostream>
#include <stdexcept>

using namespace liblayout;

static void write_image_name( std::ostream& out, char const* title )
{
	assert( title && *title );

	out << "%IN";
	for ( char const* ptr = title; *ptr; ++ptr ) {
		out << (isalnum(*ptr) ? *ptr : '_');
	}
	out << "*%\n";
}

static void write_header( std::ostream& out, char const* title, double scale, bool darkfield=false )
{
	assert( scale > 0 );
	
	out << "*\n";
	if ( title && *title ) out << "G04 " << title << "*\n";
	out << "G04 Created by " PACKAGE_NAME " - " PACKAGE_VERSION << "*\n";
	// Directive parameters
	out << "%FSLAX66Y66*%\n";
	out << "%MOMM*%\n"; // set units to millimeters
	out << "%OFA0B0*%\n"; // no offset
	out << "%SFA" << scale*1e6 << 'B' << scale*1e6 << "*%\n"; // scale (note format is 66)
	// Image parameters
	if ( title && *title ) write_image_name( out, title );	// image name
	out << ( darkfield ? "%IPNEG*%\n" : "%IPPOS*%\n" );	// image polarity
	// out << "IJACBC\n"; // center justify the image
}

static void write_footer( std::ostream& out )
{
	out << "M02*\n";
}

static void write_shape( std::ostream& out, shape const& view )
{
	// convert to a polygon
	polygon p;
	view.get_polygon( p );
	assert( p.size() > 2 );

	out << "G36*\n";  // start polygon fill
	polygon::const_iterator lp = p.begin();
	out << 'X' << lp->x << 'Y' << lp->y << "D02*\n";
	++lp;
	out << 'X' << lp->x << 'Y' << lp->y << "D01*\n";
	for ( ++lp; lp != p.end(); ++lp ) {
		out << 'X' << lp->x << 'Y' << lp->y << "*\n";
	}
	out << 'X' << p.begin()->x << 'Y' << p.begin()->y << "*\n";
	out << "G37*\n";
}

static void write_layer( std::ostream& out, layout const& view, layer_id layer )
{
	// get all of the shapes for this layer
	std::vector<shape> shapes;
	view.get_shapes( shapes, layer );
	
	// write out information to start a new layer
	out << "%LNmasklayer" << layer << "*%\n"; // layer name
	out << "%LPD*%\n"; // layer drawing polarity
	out << "G01*\n"; // linear interpolation
	
	// write out shapes
	std::vector<shape>::const_iterator shape;
	for ( shape = shapes.begin(); shape != shapes.end(); ++shape ) {
		write_shape( out, *shape );
	}
}

void gerber::write( layout const& view, layer_id layer, double scale, std::ostream& out )
{
	assert( scale>0 );
	
	// we're doing everything in millimeters in the gerber file
	// so scale against millimeters, not meters
	scale *= 1000;

	// create the header
	write_header( out, view.get_name(), scale );
	// create the single data block for the requested layer
	write_layer( out, view, layer );
	// write the footer
	write_footer( out );	
}

void gerber::write( layout const& view, double scale, std::ostream& out )
{
	assert( scale>0 );
	
	// we're doing everything in millimeters in the gerber file
	// so scale against millimeters, not meters
	scale *= 1000;

	// create the header
	write_header( out, view.get_name(), scale );
	
	// get a list of all of the available layers
	std::vector<layer_id> layers;
	view.get_layers( layers );
	// for each layout layer, write out a gerber layer
	std::vector<layer_id>::const_iterator layer;
	for( layer = layers.begin(); layer != layers.end(); ++layer ) {
		write_layer( out, view, *layer );
	}
	
	// write the footer
	write_footer( out );	
}

