/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
*/

extern "C"
char const* liblayout_version_zstring()
{
	return PACKAGE_NAME " " PACKAGE_VERSION;
}
