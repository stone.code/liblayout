/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include <assert.h>
#include <float.h>
#include <math.h>

using namespace liblayout;

#undef M_PI
#undef M_PI_2
#undef M_PI_3_2

#define M_PI   3.1415926535898 
#define M_PI_2	(M_PI/2)
#define M_PI_3_2 (3*M_PI/2)

transform::transform( manhattan_transform mt )
{
	switch (mt) {
	case R0:
		matrix[0][0] = 1; matrix[0][1] = 0; matrix[0][2] = 0;
		matrix[1][0] = 0; matrix[1][1] = 1; matrix[1][2] = 0;
		matrix[2][0] = 0; matrix[2][1] = 0; matrix[2][2] = 1;
		break;
	case R90:
		matrix[0][0] = 0; matrix[0][1] = -1; matrix[0][2] = 0;
		matrix[1][0] = 1; matrix[1][1] = 0; matrix[1][2] = 0;
		matrix[2][0] = 0; matrix[2][1] = 0; matrix[2][2] = 1;
		break;
	case R180:
		matrix[0][0] = -1; matrix[0][1] = 0; matrix[0][2] = 0;
		matrix[1][0] = 0; matrix[1][1] = -1; matrix[1][2] = 0;
		matrix[2][0] = 0; matrix[2][1] = 0; matrix[2][2] = 1;
		break;
	case R270:
		matrix[0][0] = 0; matrix[0][1] = 1; matrix[0][2] = 0;
		matrix[1][0] = -1; matrix[1][1] = 0; matrix[1][2] = 0;
		matrix[2][0] = 0; matrix[2][1] = 0; matrix[2][2] = 1;
		break;
	case MX:
		matrix[0][0] = 1; matrix[0][1] = 0; matrix[0][2] = 0;
		matrix[1][0] = 0; matrix[1][1] = -1; matrix[1][2] = 0;
		matrix[2][0] = 0; matrix[2][1] = 0; matrix[2][2] = 1;
		break;
	case MXR90:
		matrix[0][0] = 0; matrix[0][1] = 1; matrix[0][2] = 0;
		matrix[1][0] = 1; matrix[1][1] = 0; matrix[1][2] = 0;
		matrix[2][0] = 0; matrix[2][1] = 0; matrix[2][2] = 1;
		break;
	case MY:
		matrix[0][0] = -1; matrix[0][1] = 0; matrix[0][2] = 0;
		matrix[1][0] = 0; matrix[1][1] = 1; matrix[1][2] = 0;
		matrix[2][0] = 0; matrix[2][1] = 0; matrix[2][2] = 1;
		break;
	case MYR90:
		matrix[0][0] = 0; matrix[0][1] = -1; matrix[0][2] = 0;
		matrix[1][0] = -1; matrix[1][1] = 0; matrix[1][2] = 0;
		matrix[2][0] = 0; matrix[2][1] = 0; matrix[2][2] = 1;
		break;
	default:
		assert( false );
	}
}

transform::transform( transform const& rhs )
{
	for ( int i=0; i<3; ++i )
		for ( int j=0; j<3; ++j )
			matrix[i][j] = rhs.matrix[i][j];
}

transform::transform( float a11, float a12, float a13, float a21, float a22, float a23, float a31, float a32, float a33 )
{
	matrix[0][0] = a11;
	matrix[0][1] = a12;
	matrix[0][2] = a13;
	matrix[1][0] = a21;
	matrix[1][1] = a22;
	matrix[1][2] = a23;
	matrix[2][0] = a31;
	matrix[2][1] = a32;
	matrix[2][2] = a33;
}

transform&	transform::operator=( transform const& rhs )
{
	for ( int i=0; i<3; ++i )
		for ( int j=0; j<3; ++j )
			matrix[i][j] = rhs.matrix[i][j];
	return *this;
}

transform&	transform::operator*=( transform const& rhs )
{
	transform t;

	for ( int i=0; i<3; ++i )
		for ( int j=0; j<3; ++j )
		{
			t.matrix[i][j] = 0;
			for ( int k=0; k<3; ++k )
				t.matrix[i][j] += matrix[i][k] * rhs.matrix[k][j];
		}

	operator=( t );
	return *this;
}

transform transform::operator*( transform const& rhs ) const
{
	transform t;

	for ( int i=0; i<3; ++i )
		for ( int j=0; j<3; ++j )
		{
			t.matrix[i][j] = 0;
			for ( int k=0; k<3; ++k )
				t.matrix[i][j] += matrix[i][k] * rhs.matrix[k][j];
		}

	return t;
}

static inline bool eq( float a, float b )
{
	return fabs( a-b ) < FLT_EPSILON;
}

bool transform::operator==( transform const& rhs ) const
{
	for ( int i=0; i<3; ++i )
	for ( int j=0; j<3; ++j ) {
		if ( ! eq( matrix[i][j], rhs.matrix[i][j] ) ) return false;
	}
	return true;
}

manhattan_transform transform::get_manhattan_transform() const
{
	assert( valid() );
	assert( is_manhattan() );
	
	if ( matrix[0][0] > matrix[0][1] ) 
	{
		if ( matrix[1][0] > matrix[1][1] ) 
		{
			if ( eq(matrix[1][0],0) ) return MX;
			return R90;
		}
		else
		{
			if ( eq( matrix[1][0],0) ) return R0;
			return MYR90;
		}
	}
	else 
	{
		if ( matrix[1][0] > matrix[1][1] ) 
		{
			if ( eq(matrix[1][0],0) ) return R180;
			return MXR90;
		}
		else
		{
			if ( eq(matrix[1][0],0) ) return MY;
			return R270;
		}
	}
}

float transform::get_manhattan_scaling() const
{
	assert( valid() );
	assert( is_manhattan() );
	
	float a = fabs( matrix[0][0] );
	if ( a > FLT_EPSILON ) {
		assert( eq( a, fabs(matrix[1][1]) ) );
		assert( fabs(matrix[0][1]) < FLT_EPSILON );
		assert( fabs(matrix[1][0]) < FLT_EPSILON );
		return a;
	}
	
	a = fabs( matrix[0][1] );
	assert( eq( a, fabs(matrix[1][0]) ) );
	assert( fabs(matrix[0][0]) < FLT_EPSILON );
	assert( fabs(matrix[1][1]) < FLT_EPSILON );
	return a;
}

bool transform::is_manhattan() const
{
	assert( valid() );

	if ( eq( matrix[0][0], 0 ) ) {
		if ( ! eq( matrix[1][1],0) ) return false;
		if ( ! eq( fabs(matrix[0][1]),fabs(matrix[1][0])) ) return false;
		return true;
	}
	else if ( eq( matrix[0][1], 0 ) ) {
		if ( ! eq( matrix[1][0],0) ) return false;
		if ( ! eq( fabs(matrix[0][0]),fabs(matrix[1][1])) ) return false;
		return true;
	}
	
	return false;
}

void transform::mirror_x()
{
	transform const t(MX);
	*this = t * *this;
}

void transform::mirror_y()
{
	transform const t(MY);
	*this = t * *this;
}

void transform::rotate( float c, float s )
{
	float len = sqrt( c*c + s*s );
	c /= len;
	s /= len;
	transform const t( c, -s, 0, s, c, 0, 0, 0, 1 );
	*this = t * *this;
}

void transform::rotate( float angle )
{
	rotate( cos(angle), sin(angle ) );
}

void transform::scale( float s )
{
	transform const t( s, 0, 0, 0, s, 0, 0, 0, 1 );
	*this = t * *this;
}

void transform::swap( transform& rhs )
{
	for ( int i=0; i<3; ++i )
		for ( int j=0; j<3; ++j )
			std::swap( matrix[i][j], rhs.matrix[i][j] );
}

void transform::translate( point const pt )
{
	transform const t( 1, 0, pt.x, 0, 1, pt.y, 0, 0, 1 );
	*this = t * *this;
}

void transform::translate( float dx, float dy )
{
	transform const t( 1, 0, dx, 0, 1, dy, 0, 0, 1 );
	*this = t * *this;
}
