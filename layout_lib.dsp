# Microsoft Developer Studio Project File - Name="layout" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=layout - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "layout_lib.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "layout_lib.mak" CFG="layout - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "layout - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "layout - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "layout - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "NDEBUG" /D "_LIB" /D "WIN32" /D "_MBCS" /D VERSION="liblayout" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "layout - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GR /GX /ZI /Od /D "_DEBUG" /D "_LIB" /D "WIN32" /D "_MBCS" /D PACKAGE_NAME="\"liblayout\"" /D PACKAGE_VERSION="\"development\"" /YX /FD /D /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"Debug/layout.bsc"
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"Debug\layout.lib"

!ENDIF 

# Begin Target

# Name "layout - Win32 Release"
# Name "layout - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ansys.cpp
# End Source File
# Begin Source File

SOURCE=.\ansys_filename.cpp
# End Source File
# Begin Source File

SOURCE=.\asy.cpp
# End Source File
# Begin Source File

SOURCE=.\asy_filename.cpp
# End Source File
# Begin Source File

SOURCE=.\cif.cpp
# End Source File
# Begin Source File

SOURCE=.\cif_filename.cpp
# End Source File
# Begin Source File

SOURCE=.\cif_write.cpp
# End Source File
# Begin Source File

SOURCE=.\circle_shape.cpp
# End Source File
# Begin Source File

SOURCE=.\composite.cpp
# End Source File
# Begin Source File

SOURCE=.\css.cpp
# End Source File
# Begin Source File

SOURCE=.\css_io.cpp
# End Source File
# Begin Source File

SOURCE=.\dxf.cpp
# End Source File
# Begin Source File

SOURCE=.\dxf_filename.cpp
# End Source File
# Begin Source File

SOURCE=.\gdsii.cpp
# End Source File
# Begin Source File

SOURCE=.\gdsii_filename.cpp
# End Source File
# Begin Source File

SOURCE=.\gdsii_write.cpp
# End Source File
# Begin Source File

SOURCE=.\gerber.cpp
# End Source File
# Begin Source File

SOURCE=.\gerber_filename.cpp
# End Source File
# Begin Source File

SOURCE=.\layout.cpp
# End Source File
# Begin Source File

SOURCE=.\layout_composite.cpp
# End Source File
# Begin Source File

SOURCE=.\layout_insert.cpp
# End Source File
# Begin Source File

SOURCE=.\layout_name_composite.cpp
# End Source File
# Begin Source File

SOURCE=.\logging.cpp
# End Source File
# Begin Source File

SOURCE=.\manhattan_transform.cpp
# End Source File
# Begin Source File

SOURCE=.\path_shape.cpp
# End Source File
# Begin Source File

SOURCE=.\polygon.cpp
# End Source File
# Begin Source File

SOURCE=.\polygon_shape.cpp
# End Source File
# Begin Source File

SOURCE=.\ps.cpp
# End Source File
# Begin Source File

SOURCE=.\ps_filename.cpp
# End Source File
# Begin Source File

SOURCE=.\psmask.cpp
# End Source File
# Begin Source File

SOURCE=.\psmask_filename.cpp
# End Source File
# Begin Source File

SOURCE=.\rect.cpp
# End Source File
# Begin Source File

SOURCE=.\rect_shape.cpp
# End Source File
# Begin Source File

SOURCE=.\round.cpp
# End Source File
# Begin Source File

SOURCE=.\shape.cpp
# End Source File
# Begin Source File

SOURCE=.\svg.cpp
# End Source File
# Begin Source File

SOURCE=.\svg_filename.cpp
# End Source File
# Begin Source File

SOURCE=.\text_shape.cpp
# End Source File
# Begin Source File

SOURCE=.\transform.cpp
# End Source File
# Begin Source File

SOURCE=.\transform2.cpp
# End Source File
# Begin Source File

SOURCE=.\version.cpp
# End Source File
# Begin Source File

SOURCE=.\vrml.cpp
# End Source File
# Begin Source File

SOURCE=.\vrml_filename.cpp
# End Source File
# Begin Source File

SOURCE=.\wmf.cpp
# End Source File
# Begin Source File

SOURCE=.\wmf_api.cpp
# End Source File
# Begin Source File

SOURCE=.\wmf_filename.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\layout.h
# End Source File
# Begin Source File

SOURCE=.\layout_shapes.h
# End Source File
# Begin Source File

SOURCE=.\logging.h
# End Source File
# Begin Source File

SOURCE=.\wmf.h
# End Source File
# End Group
# End Target
# End Project
