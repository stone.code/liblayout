/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>
#include <math.h>

using namespace liblayout;

/************************************************************************
  Correct problem with MSVC 6
************************************************************************/

#ifndef M_PI_2
#define M_PI_2 1.5707963267949
#endif

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

/************************************************************************
  circle_shape
************************************************************************/

circle_shape::circle_shape( layer_id id, coord_t radius_, point center_ ) 
: shape::imp(id), radius( radius_ ), center( center_ )
{
	assert( radius > 0 );
}

rect circle_shape::bounds() const
{
	return rect( center.x - radius, center.y - radius,
		center.x + radius, center.y + radius );
}

int circle_shape::contains_point( point pt ) const
{
	assert( false );
	return int();
}

void circle_shape::get_polygon( polygon& p ) const
{
	p.empty();
	
	double const tolerance = 1;
	double thetad = 2*acos( 1.0 - tolerance/radius );
	if ( thetad > M_PI_2 ) thetad = M_PI_2;
	
	unsigned count = (unsigned)ceil( 2*M_PI / thetad );
	
	for ( unsigned lp=0; lp<count; ++lp ) {
		double lpt = 2*M_PI * lp / count;
		point pt( center.x + lround( radius*cos(lpt) ), 
			center.y + lround( radius*sin(lpt) ) );
		p.push_back( pt );
	}
}

shape::imp* circle_shape::eval( transform const& t ) const
{
	// can only do manhattan transforms
	if ( t.is_manhattan() )
	{
		// transform center
		point new_center = t * center;

		// transform radius
		point tmp( radius, 0 );
		tmp = t * tmp;
		coord_t new_radius = lround( 
			sqrt( ((double)tmp.x)*tmp.x + ((double)tmp.y)*tmp.y ) 
		);
		
		return new circle_shape( layer, new_radius, new_center );
	}

	polygon p;
	get_polygon(p);
	t.apply( p );
	return new polygon_shape( layer, p );
}
