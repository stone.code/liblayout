/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains the methods associated with the scale_info struct.
**
*/

#include "layout.h"
#include "layout_export.h"
#include <assert.h>

using namespace liblayout;

bool	scale_info::will_crop( rect const& bounds ) const
{
	if ( height < scale*bounds.height() ) return true;
	if ( width < scale*bounds.width() ) return true;
	return false;
}

scale_info	scale_info::complete_fields( rect const& bounds, double default_width, double default_height ) const
{
	scale_info ret = *this;

	if ( ret.scale > 0 ) {
		// The scale has been specified.  Do no change, as scale is 
		// taken as fixed.

		// Increase image height if necessary to contain layout.
		if ( ret.height < ret.scale * bounds.height() ) {
			ret.height = ret.scale * bounds.height();
		};
		// Increase image width if necessary to contain layout.
		if ( ret.width < ret.scale * bounds.width() ) {
			ret.width = ret.scale * bounds.width();
		}
	}
	else {
		// The scale is unknown.  Can we determine the scale from the
		// provided geometry?
		if ( ret.width <= 0 && ret.height <= 0 ) {
			// Use default width and height
			ret.width = default_width;
			ret.height = default_height;
			// calculate the best fit
			double x_scale = ret.width / bounds.width();
			double y_scale = ret.height / bounds.height();
			ret.scale = ( x_scale < y_scale ) ? x_scale : y_scale;
		}
		else if ( ret.width <= 0 ) {
			// auto-fit to the height
			assert( ret.height > 0 );
			ret.scale = ret.height / bounds.height();
			// Calculate the width
			ret.width = ret.scale * bounds.width();
		}
		else if ( ret.height <= 0 ) {
			// auto-fit to the width
			assert( ret.width > 0 );
			ret.scale = ret.width / bounds.width();
			// Calculate the height
			ret.height = ret.scale * bounds.height();
		}
		else {
			// calculate the best fit
			assert( ret.width > 0 );
			assert( ret.height > 0 );
			double x_scale = ret.width / bounds.width();
			double y_scale = ret.height / bounds.height();
			ret.scale = ( x_scale < y_scale ) ? x_scale : y_scale;
		}
	}
	
	// The ret should be no complete
	assert( ret.is_complete() );
	assert( !ret.will_crop( bounds ) );
	return ret;
}
