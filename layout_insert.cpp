/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2007 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include "layout_shapes.h"
#include <assert.h>

using namespace liblayout;

void layout::insert( layer_id id, rect const& r )
{
	assert( r.valid() );
	insert( new rect_shape( id, r ) );
}

void layout::insert( layer_id id, coord_t radius, point center )
{
	assert( radius > 0 );
	insert( new circle_shape( id, radius, center ) );
}

void layout::insert( layer_id id, std::vector<point> const& pts, coord_t width, path_end_t path_end )
{
	assert( pts.size() >= 2 );
	insert( new path_shape( id, pts, width, path_end ) );
}

void layout::insert( layer_id id, std::vector<point> const& pts )
{
	assert( pts.size() >= 3 );
	insert( new polygon_shape( id, pts ) );
}

void layout::insert( layout* layout, transform const& t )
{
	insert( new layout_composite( layout, t ) );
}

void layout::insert( layout* layout, transform const& t, int rows, int cols, point delta_r, point delta_c )
{
	insert( new layout_composite( layout, t, rows, cols, delta_r, delta_c ) );
}						

void layout::insert( char const* name, transform const& t )
{
	insert( new layout_name_composite( name, t ) );
}

void layout::insert( char const* name, transform const& t, int rows, int cols, point delta_r, point delta_c )
{
	insert( new layout_name_composite( name, t, rows, cols, delta_r, delta_c ) );
}
