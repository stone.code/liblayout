/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#ifndef __LAYOUT_WMF_H_
#define __LAYOUT_WMF_H_

#include "layout.h"
#include <ostream>
#include <vector>

namespace liblayout 
{
namespace wmf 
{
	typedef unsigned long color_t;
	typedef unsigned handle_t;

	class writer
	{
	public:
		writer();

		handle_t	create_pen( color_t );
		handle_t	create_brush( color_t );
		handle_t	create_null_brush();
		void		select_pen( handle_t );
		void		select_brush( handle_t );
		void		create_rect( coord_t x1, coord_t y1, coord_t x2, coord_t y2 );
		void		create_polygon( std::vector<point> const& points );

		void		write( std::ostream& ) const;

	private:
		unsigned	records;
		unsigned	handles;
		unsigned long	width;
		unsigned long	height;
		std::vector<unsigned char>	data;
	};

} // namespace wmf
} // namespace liblayout

#endif // __LAYOUT_WMF_H_
