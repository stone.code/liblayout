/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include "layout_shapes.h"
#include <algorithm>
#include <assert.h>
#include <ctype.h>
#include <iostream>
#include <stdexcept>

using namespace liblayout;

static void export_shape( shape const& shape, double z, double mag, std::ostream& out )
{
	// get a polygon
	polygon p;
	shape.get_polygon( p );

	out << "\tCoordinate3 {\n";
	out << "\t\tpoint [\n";
	polygon::iterator lp; 
	for ( lp = p.begin(); lp != p.end(); ++lp )	{
		out << "\t\t\t" << (lp->x * mag) << ' ' << (lp->y * mag) << ' ' << z << ",\n";
	}
	out << "\t\t]\n";
	out << "\t}\n";

	out << "\tIndexedFaceSet {\n";
	out << "\t\tcoordIndex [ 0";
	for ( size_t lp2 = 1; lp2 < p.size(); ++lp2 ) {
		out << ", " << lp2;
	}
	out << " ]\n";
	out << "\t}\n";
}

static void write_material( std::ostream& out, vrml::map_id_style const& layer_map, layer_id layer )
{
	// find the CSS styling information for this layer
	vrml::map_id_style::const_iterator style_ = layer_map.find( layer );
	if ( style_ != layer_map.end() )
	{
		// reference the css info
		style const& pen = style_->second;
			
		// do we have fill information?
		if ( pen.has_fill() ) {
			short r = pen.fill.red();
			short g = pen.fill.green();
			short b = pen.fill.blue();
			short a = pen.fill.alpha();
			assert( a<0xFF );
			out << "\tMaterial {\n";
			out << "\t\tdiffuseColor " << r / 255.0 << ' ' << g / 255.0 << ' ' << b / 255.0 << '\n';
			if ( a>0 ) out << "\t\ttransparency " << a / 255.0 << '\n';
			out << "\t}\n";
		}
	}
}

void vrml::write( layout const& view, map_id_style& layer_map, double mag, std::ostream& out )
{
	assert( mag > 0 );

	// get size
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// write the header
	out << "#VRML V1.0 ascii\n";
	out << "Separator {\n";
	// write out the light
	out << "\tDirectionalLight {\n";
	out << "\t\tdirection\t2 .5 -1\n";
	out << "\t}\n";
	out << "\tDirectionalLight {\n";
	out << "\t\tdirection\t1 -2 .5\n";
	out << "\t}\n";

	double x = bounds.center_x() * mag;
	double y = bounds.center_y() * mag;
	double z = bounds.width();
	if ( bounds.height() > z ) z = bounds.height();
	z *= mag;

	out << "\tSwitch {\n";
	out << "\t\twhichChild\t0\n";
	out << "\t\tDEF FrontView PerspectiveCamera {\n";
	out << "\t\t\tposition\t" << x << ' ' << y << ' ' << z << std::endl;
	out << "\t\t\torientation\t0 0 1 0\n";
	out << "\t\t\tfocalDistance\t" << z << std::endl;
	out << "\t\t}\n";
	out << "\t}\n";

	// iterate over all present layers
	std::vector<layer_id> layers;
	view.get_layers( layers );
	std::vector<layer_id>::const_iterator layer;
	for ( layer = layers.begin(); layer != layers.end(); ++layer )
	{
		short order = 0;
		double h = 0;
		
		// set the colour for the layer
		write_material( out, layer_map, *layer );
		
		// set the height
		map_id_style::const_iterator style = layer_map.find( *layer );
		if ( style != layer_map.end() )
		{
			h = style->second.order * 0.01 * z;
		}

		// get the shapes
		std::vector<shape>	shapes;
		view.get_shapes( shapes, *layer );

		// write out all of the shapes on this layer
		std::vector<shape>::const_iterator lp;
		for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
		{
			assert( lp->layer() == *layer );
			export_shape( *lp, h, mag, out );
		}
	}


	// and the footer
	out << "}\n";
}

void vrml::write( layout const& view, layer_id layer, map_id_style& layer_map, double mag, std::ostream& out )
{
	assert( mag > 0 );

	// get size
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// get the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes, layer );

	// write the header
	out << "#VRML V1.0 ascii\n";
	out << "Separator {\n";
	// write out the light
	out << "\tDirectionalLight {\n";
	out << "\t\tdirection\t2 .5 -1\n";
	out << "\t}\n";
	out << "\tDirectionalLight {\n";
	out << "\t\tdirection\t1 -2 .5\n";
	out << "\t}\n";

	double x = bounds.center_x() * mag;
	double y = bounds.center_y() * mag;
	double z = bounds.width();
	if ( bounds.height() > z ) z = bounds.height();
	z *= mag;

	out << "\tSwitch {\n";
	out << "\t\twhichChild\t0\n";
	out << "\t\tDEF FrontView PerspectiveCamera {\n";
	out << "\t\t\tposition\t" << x << ' ' << y << ' ' << z << std::endl;
	out << "\t\t\torientation\t0 0 1 0\n";
	out << "\t\t\tfocalDistance\t" << z << std::endl;
	out << "\t\t}\n";
	out << "\t}\n";

	// set the colour
	write_material( out, layer_map, layer );

	// write out all of the shapes on this layer
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		assert( lp->layer() == layer );
		export_shape( *lp, 0, mag, out );
	}

	// and the footer
	out << "}\n";
}

