/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include <algorithm>
#include <assert.h>
#include <ctype.h>
#include <map>
#include <stdexcept>
#include <string.h>
#include <string>
#include <utility>

#define ALPHAMASK 0xFF000000
#define COLOUR(name,r,g,b) colours.insert( std::make_pair(#name,colour(r,g,b)) )

using namespace liblayout;

/***********************************************************************/

static inline short get_hex_value( char t )
{
	// this is a precondition to calling this function
	assert( isxdigit(t) );
	// handle the first case, and convert decimal digits
	if ( isdigit(t) ) return t-'0';
	// handle this second case
	return tolower(t)-'a' + 10;
}

namespace liblayout {
namespace detail {

struct ltstr
{
	bool operator()( char const* s1, char const* s2 ) const
	{
		return strcmp( s1, s2 ) < 0;
	}
};

}
}

liblayout::colour liblayout::colour::from_text( char const* text_ )
{
	assert( text_ );

	typedef std::map<char const*,colour,detail::ltstr> colourmap;
	static bool is_initialized = false;
	static colourmap colours;

	if ( ! is_initialized )
	{
		COLOUR(aliceblue,240, 248, 255) ;
		COLOUR(antiquewhite,250, 235, 215) ;
		COLOUR(aqua, 0, 255, 255) ;
		COLOUR(aquamarine,127, 255, 212) ;
		COLOUR(azure,240, 255, 255) ;
		COLOUR(beige,245, 245, 220) ;
		COLOUR(bisque,255, 228, 196) ;
		COLOUR(black, 0, 0, 0) ;
		COLOUR(blanchedalmond,255, 235, 205) ;
		COLOUR(blue,0,0,255);
		COLOUR(blueviolet,138, 43, 226) ;
		COLOUR(brown,165, 42, 42) ;
		COLOUR(burlywood,222, 184, 135) ;
		COLOUR(cadetblue, 95, 158, 160) ;
		COLOUR(chartreuse,127, 255, 0) ;
		COLOUR(chocolate,210, 105, 30) ;
		COLOUR(coral,255, 127, 80) ;
		COLOUR(cornflowerblue,100, 149, 237) ;
		COLOUR(cornsilk,255, 248, 220) ;
		COLOUR(crimson,220, 20, 60) ;
		COLOUR(cyan,0,255,255);
		COLOUR(darkblue,0,0,139);
		COLOUR(darkcyan,0,139,139);
		COLOUR(darkgoldenrod,184,134,11);
		COLOUR(darkgray,169,169,169);
		COLOUR(darkgrey,169,169,169);
		COLOUR(darkgreen,0,100,0);	 
		COLOUR(darkkhaki,189,183,107);
		COLOUR(darkmagenta,139,0,139);
		COLOUR(darkolivegreen,85,107,47);	 
		COLOUR(darkorange,255,140,0);
		COLOUR(darkorchid,153,50,204);
		COLOUR(darkred,139,0,0);
		COLOUR(darksalmon,233,150,122);
		COLOUR(darkseagreen,143,188,143);
		COLOUR(darkslateblue,72,61,139);
		COLOUR(darkslategray,47,79,79);	 
		COLOUR(darkslategrey,47,79,79);
		COLOUR(darkturquoise,0,206,209);	 
		COLOUR(darkviolet,148,0,211);	 
		COLOUR(deeppink,255,20,147);
		COLOUR(deepSkyblue,0,191,255);	 
		COLOUR(dimgray,105,105,105);
		COLOUR(DimGrey,105,105,105);
		COLOUR(dodgerblue,30,144,255);
		COLOUR(firebrick,178,34,34);
		COLOUR(floralwhite,255,250,240);
		COLOUR(forestgreen,34,139,34);
		COLOUR(fuchia,255,0,255);
		COLOUR(gainsboro,220,220,220);
		COLOUR(ghostwhite,248,248,255);
		COLOUR(gold, 255, 215, 0);
		COLOUR(goldenrod,218,165,32);
		COLOUR(gray,190, 190, 190);
		COLOUR(grey,190, 190, 190);
		COLOUR(green,0,255,0);
		COLOUR(greenyellow,173,255,47);
		COLOUR(honeydew,240,255,240);
		COLOUR(hotpink,255,105,180);
		COLOUR(indianred,205,92,92);
		COLOUR(indigo,75,0,130);
		COLOUR(ivory,255,240,240);
		COLOUR(khaki,240,230,140);
		COLOUR(lavender,230,230,250);
		COLOUR(lavenderblush,255,240,245);
		COLOUR(lawngreen,124,252,0);
		COLOUR(lemonchiffon,255,250,205);
		COLOUR(lightblue,173,216,230);
		COLOUR(lightcoral,240,128,128);
		COLOUR(lightcyan,224,255,255),
		COLOUR(lightgoldenrodyellow,250,250,210);
		COLOUR(lightgreen,144,238,144);
		COLOUR(lightgray,211,211,211);
		COLOUR(lightgrey,211,211,211);
		COLOUR(lightpink,255,182,193);
		COLOUR(lightsalmon,255,160,122);
		COLOUR(lightseagreen,32,178,170);
		COLOUR(lightskyblue,135,206,250);
		COLOUR(lightslategray,119,136,153);
		COLOUR(lightslategrey,119,136,153);
		COLOUR(lightsteelblue,176,196,222);
		COLOUR(lightyello,255,255,224);
		COLOUR(lime,0,255,0);
		COLOUR(linen,250,240,230);
		COLOUR(magenta,255,0,255);
		COLOUR(maroon,128,0,0);
		COLOUR(mediumaquamarine,102,205,170);
		COLOUR(mediumblue,0,0,205);
		COLOUR(mediumorchid,186,85,211);
		COLOUR(mediumpurple,147,112,219);
		COLOUR(mediumseagreen,60,179,113);
		COLOUR(mediumslateblue,123,104,238);
		COLOUR(mediumspringgreen,0,250,154);
		COLOUR(mediumturquoise,72,209,204);
		COLOUR(mediumvioletred,199,21,133);
		COLOUR(midnightblue,25,25,112);
		COLOUR(mintcream,245,255,250);
		COLOUR(mistyrose,255,228,225);
		COLOUR(moccasin,255,228,181);
		COLOUR(navajowhite,255,222,173);
		COLOUR(navy,0,0,128);
		COLOUR(oldlace,253,245,230);
		COLOUR(olive,128,128,0);
		COLOUR(olivedrab,107,142,35);
		COLOUR(orange,255,165,0);
		COLOUR(orangered,255,69,0);
		COLOUR(orchid,218,112,214);
		COLOUR(palegoldenrod,238,232,170);
		COLOUR(palegreen,152,251,152);
		COLOUR(paleturquoise,175,23,238);
		COLOUR(palevioletred,219,112,147);
		COLOUR(papayawhip,255,239,213);
		COLOUR(peachpuff,255,239,213);
		COLOUR(peru,205,133,63);
		COLOUR(pink,255,192,203);
		COLOUR(plum,221,160,221);
		COLOUR(powderblue,176,224,230);
		COLOUR(purple,128,0,128);
		COLOUR(red,255,0,0);
		COLOUR(rosybrown,188,143,143);
		COLOUR(royalblue,65,105,225);
		COLOUR(saddlebrown,139,69,19);
		COLOUR(salmon,250,128,114);
		COLOUR(sandybrown,244,164,96);
		COLOUR(seagreen,46,139,87);
		COLOUR(seashell,255,245,238);
		COLOUR(sienna,160,82,45);
		COLOUR(silver,192,192,192);
		COLOUR(skyblue,135,206,235);
		COLOUR(slateblue,106,90,205);
		COLOUR(slategray,112,128,144);
		COLOUR(slategrey,112,128,144);
		COLOUR(snow,255,250,250);
		COLOUR(springgreen,0,255,127);
		COLOUR(steelblue,70,130,180);
		COLOUR(tan,210,180,140);
		COLOUR(teal,0,128,128);
		COLOUR(thistle,216,191,216);
		COLOUR(tomato,253,99,71);
		COLOUR(turquoise,64,224,208);
		COLOUR(violet,238,130,238);
		COLOUR(wheat,245,222,179);
		COLOUR(white,255,255,255);
		COLOUR(whitesmoke,245,245,245);
		COLOUR(yellow,255,255,0);
		COLOUR(yellowgreen,154,205,50);

		is_initialized = true;
   	}
   	
   	// make lower case version of string
   	std::string text( text_ );
   	std::transform( text.begin(), text.end(), text.begin(), tolower );
   	
   	// look up the name
   	colourmap::const_iterator lp = colours.find( text.c_str() );
   	if ( lp != colours.end() ) return lp->second;
   	
   	// try RGB hex values
	if ( text.size()==7 && text[0]=='#' && isxdigit(text[1]) && isxdigit(text[2]) 
		&& isxdigit(text[3]) && isxdigit(text[4]) && isxdigit(text[5]) && isxdigit(text[6]) ) 
	{
		short r = (get_hex_value(  text[1] ) << 4 ) + get_hex_value( text[2] );
		short g = (get_hex_value(  text[3] ) << 4 ) + get_hex_value( text[4] );
		short b = (get_hex_value(  text[5] ) << 4 ) + get_hex_value( text[6] );
		return colour(r,g,b);
	}
   	// try RGB hex values
	if ( text.size()==4 && text[0]=='#' && isxdigit(text[1]) && isxdigit(text[2]) 
		&& isxdigit(text[3]) ) 
	{
		short r = (get_hex_value(  text[1] ) << 4 ) + get_hex_value( text[1] );
		short g = (get_hex_value(  text[2] ) << 4 ) + get_hex_value( text[2] );
		short b = (get_hex_value(  text[3] ) << 4 ) + get_hex_value( text[3] );
		return colour(r,g,b);
	}
	
	// no such name
	std::string msg;
	msg.append( "The colour specification '" );
	msg.append( text );
	msg.append( "' could not be converted to an RGB value." );
	throw std::runtime_error( msg );	
}
