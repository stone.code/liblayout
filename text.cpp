/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 20110 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include <stdexcept>

using namespace liblayout;

static char const* lookup_name( text::map_id_name const& names, layer_id layer )
{
	text::map_id_name::const_iterator lp = names.find( layer );
	if ( lp != names.end() ) return lp->second.c_str();
	return 0;
}

void text::write( layout const& view, layer_id layer, map_id_name const& id_to_name, std::ostream& out )
{
	char const* name = lookup_name( id_to_name, layer );

	out << "Summary for layer # " << layer;
	if ( name ) out << " (" << name << ")";
	out << ":\n";
}

void text::write( layout const& view, map_id_name const& id_to_name, std::ostream& out )
{
	rect bounds = view.bounds();
	out << "Bounds:\n";
	out << "\tX:\t" << bounds.x1 << '\t' << bounds.x2 << '\n';
	out << "\tY:\t" << bounds.y1 << '\t' << bounds.y2 << '\n';

	out << "Layers Present:\n";
	{
		std::vector<layer_id> layers;
		view.get_layers( layers );

		if ( layers.begin() == layers.end() ) {
			out << "\tNone.\n";
		}
		else for ( std::vector<layer_id>::const_iterator lp = layers.begin(); lp != layers.end(); ++lp ) {
			char const* name = lookup_name( id_to_name, *lp );
			out << "\t# " << *lp;
			if (name) out << "\t(" << name << ')';
			out << '\n';
		}
	}
}

