/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>
#include <ctype.h>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdexcept>
#include <stdlib.h>

using namespace liblayout;
using std::string;

#define INVALID_DXF "Invalid DXF file.  "

static bool is_integer( char const* ptr )
{
	while ( isspace(*ptr) ) ++ptr;
	if ( !isdigit(*ptr) ) return false;
	while ( isdigit(*ptr) ) ++ptr;
	while ( isspace(*ptr) ) ++ptr;
	return (*ptr=='\0');
}

static bool throw_invalid_path( std::istream& in, char const* entity, string const& handle, size_t line )
{
	std::stringstream message;
	message << INVALID_DXF << "Invalid entity of type " << entity << " found during DXF import (source line " 
		<< line << ").  Insufficient points to form a line.";
	if ( !handle.empty() )
		message << "  Entity handle is " << handle << '.';
	else
		message << "  Found at position " << in.tellg() << '.';
	message << '\0';
	throw std::runtime_error( message.str() );

}

static bool throw_invalid_polygon( std::istream& in, char const* entity, string const& handle, size_t line )
{
	std::stringstream message;
	message << INVALID_DXF << "Invalid entity of type " << entity << " found during DXF import (source line " 
		<< line << ").  Insufficient points to form a polygon.";
	if ( !handle.empty() )
		message << "  Entity handle is " << handle << '.';
	else
		message << "  Found at position " << in.tellg() << '.';
	message << '\0';
	throw std::runtime_error( message.str() );

}

static bool get_pair( std::istream& in, long& code, string& text )
{
	char buffer[256];
	size_t s;

	// Get the group code
	in.getline( buffer, 256 );
	if ( !is_integer(buffer) ) return true;
	code = atol(buffer);
	// Get the group text
	in.getline( buffer, 256 );
	text = buffer;
	s = text.size();
	while ( isspace( text[s-1] ) ) --s;
	text.erase( s );

	return false;
}

static string advance_next_group_0( std::istream& in )
{
	long code;
	string text;

	do {
		// Move to the next group zero code
		bool err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Could not find ENDSEC group code." );
	} while ( code != 0 );
	
	// Return the group text
	return text;
}

static void import_header( std::istream& in )
{
	long code;
	string text;

	// The header must begin with a section token
	bool err = get_pair( in, code, text );
	if ( err || code!=0 || text!="SECTION" )
		throw std::runtime_error( INVALID_DXF "Expected a SECTION group code." );
	err = get_pair( in, code, text );
	if ( err || code!=2 || text!="HEADER" )
		throw std::runtime_error( INVALID_DXF "Expected a HEADER group code." );

	// Loop through all items until we see end of section
	do {
		err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Could not find ENDSEC group code." );
	} while ( code!=0 && text!="ENDSEC" );
}

static void skip_section( std::istream& in )
{
	long code;
	string text;

	do {
		bool err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Could not find ENDSEC group code." );
	} while ( code!=0 || text!="ENDSEC" );
}

static bool lookup_layer( layer_id& lhs, dxf::map_name_id const& name_id, string const& layer )
{
        // Look-up the name to find the ID
	dxf::map_name_id::const_iterator lp = name_id.find( layer );
	if ( lp == name_id.end() ) return false;
	// Found a match
	lhs = lp->second;
	return true;
}

static layer_id lookup_layer( dxf::map_name_id& name_id, string const& layer,
	dxf::on_layer_handler on_layer, void* on_layer_data )
{
	// Look-up the name to find the ID
	dxf::map_name_id::const_iterator lp = name_id.find( layer );
	// If necessary, add the name to the map
	if ( lp == name_id.end() ) {
                // Add a new layer into the map
                layer_id id = dxf::get_next_layer_id( name_id );
                assert( id != 0 );
                name_id[ layer ] = id;
                lp = name_id.find( layer );
                // Inform caller of update
                on_layer( on_layer_data, lp->first.c_str(), lp->second );
	}

	// Add the shape
	assert( lp != name_id.end() );
	return lp->second;
}

static string import_trace( 
	string& layer, std::vector<point>& polygon, unsigned scale, std::istream& in )
{
	double x1 = 0, y1 = 0;
	double x2 = 0, y2 = 0;
	double x3 = 0, y3 = 0;
	double x4 = 0, y4 = 0;
	long code;
	string text;

	// Read in the attributes
	do {
		bool err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Entitie incomplete." );

		switch ( code ) {
		case 8:
			layer = text; break;
		case 10:
			x1 = atof(text.c_str()); break;
		case 11:
			x2 = atof(text.c_str()); break;
		case 12:
			x3 = atof(text.c_str()); break;
		case 13:
			x4 = atof(text.c_str()); break;
		case 20:
			y1 = atof(text.c_str()); break;
		case 21:
			y2 = atof(text.c_str()); break;
		case 22:
			y3 = atof(text.c_str()); break;
		case 23:
			y4 = atof(text.c_str()); break;
		default:
			/* do nothing */ break;
		}
	} while ( code!=0 );
	
	/* code == 0 */
	/* Maybe ENDSEC or start of another shape */
	assert( code==0 );

	// Make the shape
	polygon.clear();
	polygon.push_back( point( lround(x1*scale), lround(y1*scale) ) );
	polygon.push_back( point( lround(x2*scale), lround(y2*scale) ) );
	polygon.push_back( point( lround(x3*scale), lround(y3*scale) ) );
	polygon.push_back( point( lround(x4*scale), lround(y4*scale) ) );
	
	// Return the new group text
	return text;
}

static string import_solid( 
	string& layer, std::vector<point>& polygon, unsigned scale, std::istream& in )
{
	double x1 = 0, y1 = 0;
	double x2 = 0, y2 = 0;
	double x3 = 0, y3 = 0;
	double x4 = 0, y4 = 0;
	long code;
	string text;
	bool pt4_present = false;

	// Read in the attributes
	do {
		bool err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Entitie incomplete." );

		switch ( code ) {
		case 8:
			layer = text; break;
		case 10:
			x1 = atof(text.c_str()); break;
		case 11:
			x2 = atof(text.c_str()); break;
		case 12:
			x3 = atof(text.c_str()); break;
		case 13:
			pt4_present = true; x4 = atof(text.c_str()); break;
		case 20:
			y1 = atof(text.c_str()); break;
		case 21:
			y2 = atof(text.c_str()); break;
		case 22:
			y3 = atof(text.c_str()); break;
		case 23:
			pt4_present = true; y4 = atof(text.c_str()); break;
		default:
			/* do nothing */ break;
		}
	} while ( code!=0 );
	
	/* code == 0 */
	/* Maybe ENDSEC or start of another shape */
	assert( code==0 );

	// Make the shape
	polygon.clear();
	polygon.push_back( point( lround(x1*scale), lround(y1*scale) ) );
	polygon.push_back( point( lround(x2*scale), lround(y2*scale) ) );
	polygon.push_back( point( lround(x3*scale), lround(y3*scale) ) );
	if ( pt4_present )
		polygon.push_back( point( lround(x4*scale), lround(y4*scale) ) );
	
	// Return the new group text
	return text;
}

static string import_vertex( std::vector<point>& vertices, unsigned scale, std::istream& in )
{
	coord_t x, y;
	long code;
	string text;

	// Read in the attributes
	do {
		bool err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Entitie incomplete." );

		switch ( code ) {
		case 10:
			x = lround( atof(text.c_str()) * scale ); break;
		case 20:
			y = lround( atof(text.c_str()) * scale ); break;
		default:
			/* do nothing */ break;
		}
	} while ( code!=0 );
	
	// Add the point to the polygon
	vertices.push_back( point(x,y) );
	// And return
	return text;
}

static string import_polyline( 
	string& layer, std::vector<point>& polygon, coord_t& width, unsigned scale, std::istream& in )
{
	long code;
	string text, handle;
	int flag = 0;

	width = 0;
	
	do {
		bool err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Entity incomplete" );

		switch ( code ) {
		case 5:
			handle = text; break;
		case 8:
			layer = text; break;
		case 39:
			width = lround( atof( text.c_str() ) * scale ); break;
		case 70:
			flag = atoi( text.c_str() ); break;
		default:
			/* do nothing */ break;
		}
	} while ( code!=0 );
	
	// Start importing vertices
	polygon.clear();
	while ( text == "VERTEX" ) {
		text = import_vertex( polygon, scale, in );
	}
	
	/* Maybe ENDSEC or start of another shape */
	assert( code==0 );
	// If the pair is a SEQEND, skip
	if ( text=="SEQEND" ) {
		bool err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Could not find ENDSEC group code." );
	}
	
	// Check on the type of polyline
	if ( flag == 0 ) {
		// Polyline or path
		if ( width<1 ) width = 1;
		// Check that there are sufficient points
		if ( polygon.size() < 2 ) throw_invalid_path( in, "POLYLINE", handle, __LINE__ );
	}
	else if ( flag == 1 ) {
		// Closed polygon
		width = 0;
		// Check that there are sufficient points
		if ( polygon.size() < 3 ) throw_invalid_polygon( in, "POLYLINE", handle, __LINE__ );
	}
	else
		throw std::runtime_error( INVALID_DXF "Unsupported polyline entity" );
		
	
	// Return the new group text
	return text;
}

static string import_lwpolyline( 
	string& layer, std::vector<point>& polygon, coord_t& width, unsigned scale, std::istream& in )
{
	long code;
	string text, handle;
	int flag = 0;
	size_t xcount = 0;
	size_t ycount = 0;

	polygon.clear();
	width = 0;
	
	do {
		bool err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Entity incomplete" );

		switch ( code ) {
		case 5:
			handle = text; break;
		case 8:
			layer = text; break;
		case 10:
			if ( polygon.size()==xcount ) polygon.push_back( point(0,0) );
			polygon[xcount].x = lround( atof( text.c_str() ) * scale ); 
			++xcount;
			break;
		case 20:
			if ( polygon.size()==ycount ) polygon.push_back( point(0,0) );
			polygon[ycount].y = lround( atof( text.c_str() ) * scale ); 
			++ycount;
			break;
		case 39:
		case 43:
			width = lround( atof( text.c_str() ) * scale ); break;
		case 70:
			flag = atoi( text.c_str() ); break;
		default:
			/* do nothing */ break;
		}
	} while ( code!=0 );
	
	/* Maybe ENDSEC or start of another shape */
	assert( code==0 );
	
	// Check on the type of polyline
	if ( flag == 0 ) {
		// Polyline or path
		if ( width<1 ) width = 1;
		// Check that there are sufficient points
		if ( polygon.size() < 2 ) throw_invalid_path( in, "LWPOLYLINE", handle, __LINE__ );
	}
	else if ( flag == 1 ) {
		// Closed polygon
		width = 0;
		// Check that there are sufficient points
		if ( polygon.size() < 3 ) throw_invalid_polygon( in, "LWPOLYLINE", handle, __LINE__ );
	}
	else
		throw std::runtime_error( INVALID_DXF "Unsupported lwpolyline entity" );
	
	// Return the new group text
	return text;
}

static string import_circle( string& layer, coord_t& xa, coord_t& ya, coord_t& ra,
	unsigned scale, std::istream& in )
{
	double x, y, r;
	long code;
	string text;

	// Read in the attributes
	do {
		bool err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Entitie incomplete." );

		switch ( code ) {
		case 8:
			layer = text; break;
		case 10:
			x = atof( text.c_str() ); break;
		case 20:
			y = atof( text.c_str() ); break;
		case 40:
			r = atof( text.c_str() ); break;
		default:
			/* do nothing */ break;
		}
	} while ( code!=0 );

	/* Maybe ENDSEC or start of another shape */
	assert( code==0 );

	// Make the shape
	xa = lround( x * scale );
	ya = lround( y * scale );
	ra = lround( r * scale );

	// Return the new group text
	return text;
}

static string import_line( string& layer, std::vector<point>& pts, coord_t& width, unsigned scale, std::istream& in )
{
	long code;
	string text;
	point a(0,0), b(0,0);

	width = 0;

	do {
		bool err = get_pair( in, code, text );
		if ( err ) throw std::runtime_error( INVALID_DXF "Entitie incomplete." );

		switch ( code ) {
		case 8:
			layer = text; break;
		case 10:
			a.x = lround( atof( text.c_str() ) * scale); break;
		case 11:
			b.x = lround( atof( text.c_str() ) * scale); break;
		case 20:
			a.y = lround( atof( text.c_str() ) * scale); break;
		case 21:
			b.y = lround( atof( text.c_str() ) * scale); break;
		case 39:
			width = lround( atof( text.c_str() ) * scale); break;
		default:
			/* do nothing */ break;
		}
	} while ( code != 0 );

	// Make the shape
	pts.clear();
	pts.push_back( a );
	pts.push_back( b );
	if ( width==0 ) width = 1;

	// Return the new group text
	return text;
}

static void import_entities( layout& view,
	dxf::map_name_id const& name_id, unsigned scale, 
	dxf::on_comment_handler on_comment, void* on_comment_data,
	std::istream& in )
{
	long code;
	string text;

	bool err = get_pair( in, code, text );
	if ( err ) throw std::runtime_error( INVALID_DXF "Could not find ENDSEC group code." );
	// A Shape or an ENDSEC is expected
	if ( code!=0 ) throw std::runtime_error( INVALID_DXF "Could not find ENDSEC group code." );

	// Iterate through the shapes
	while ( text != "ENDSEC" ) {
		string layer;
		std::vector<point> polygon;
		coord_t x, y, width;
		layer_id id;

		assert( code == 0 );

		if ( text=="TRACE" ) {
			text = import_trace( layer, polygon, scale, in );
			if ( lookup_layer( id, name_id, layer ) )
				view.insert( id, polygon );
		}
		else if ( text=="SOLID" ) {
			text = import_solid( layer, polygon, scale, in );
			if ( lookup_layer( id, name_id, layer ) )
				view.insert( id, polygon );
		}
		else if ( text=="POLYLINE" ) {
			text = import_polyline( layer, polygon, width, scale, in );
			if ( lookup_layer( id, name_id, layer ) ) {
				if ( width==0 )
					view.insert( id, polygon );
				else
					view.insert( id, polygon, width, path_end_square );
			}
		}
		else if ( text=="LWPOLYLINE" ) {
			text = import_lwpolyline( layer, polygon, width, scale, in );
			if ( lookup_layer( id, name_id, layer ) ) {
				if ( width==0 )
					view.insert( id, polygon );
				else
					view.insert( id, polygon, width, path_end_square );
			}
		}
		else if ( text=="CIRCLE" ) {
			text = import_circle( layer, x, y, width, scale, in );
			if ( lookup_layer( id, name_id, layer ) )
				view.insert( id, width, point(x,y) );
		}
		else if ( text=="LINE" ) {
			text = import_line( layer, polygon, width, scale, in );
			if ( lookup_layer( id, name_id, layer ) ) {
				view.insert( id, polygon, width, path_end_square );
			}
		}
		else if ( text=="VIEWPORT" ) {
			text = advance_next_group_0( in );
		}
		else {
			// Print a warning message
			if ( on_comment ) {
				std::string msg( "WARN:  An unsupported entity (" );
				msg.append( text );
				msg.append( ") was encountered and ignored." );
				on_comment( on_comment_data, msg.c_str() );
			}
			// Advance past the entity
			text = advance_next_group_0( in );
		}
	}

	assert( code == 0 );
	assert( text == "ENDSEC" );
}

static void import_entities( layout& view, dxf::map_name_id& name_id, unsigned scale, 
	dxf::on_layer_handler on_layer, void* on_layer_data,
	dxf::on_comment_handler on_comment, void* on_comment_data,
	std::istream& in )
{
	long code;
	string text;

	bool err = get_pair( in, code, text );
	if ( err ) throw std::runtime_error( INVALID_DXF "Could not find ENDSEC group code." );
	// A Shape or an ENDSEC is expected
	if ( code!=0 ) throw std::runtime_error( INVALID_DXF "Could not find ENDSEC group code." );

	// Iterate through the shapes
	while ( text != "ENDSEC" ) {
		string layer;
		std::vector<point> polygon;
		coord_t x, y, width;
		layer_id id;

		assert( code == 0 );

		if ( text=="TRACE" ) {
			text = import_trace( layer, polygon, scale, in );
			id = lookup_layer( name_id, layer, on_layer, on_layer_data );
			view.insert( id, polygon );
		}
		else if ( text=="SOLID" ) {
			text = import_solid( layer, polygon, scale, in );
			id = lookup_layer( name_id, layer, on_layer, on_layer_data );
			view.insert( id, polygon );
		}
		else if ( text=="POLYLINE" ) {
			text = import_polyline( layer, polygon, width, scale, in );
			id = lookup_layer( name_id, layer, on_layer, on_layer_data );
			if ( width==0 )
				view.insert( id, polygon );
			else
				view.insert( id, polygon, width, path_end_square );
		}
		else if ( text=="LWPOLYLINE" ) {
			text = import_lwpolyline( layer, polygon, width, scale, in );
			id = lookup_layer( id, name_id, layer );
			if ( width==0 )
				view.insert( id, polygon );
			else
				view.insert( id, polygon, width, path_end_square );
		}
		else if ( text=="CIRCLE" ) {
			text = import_circle( layer, x, y, width, scale, in );
			id = lookup_layer( name_id, layer, on_layer, on_layer_data );
			view.insert( id, width, point(x,y) );
		}
		else if ( text=="LINE" ) {
			text = import_line( layer, polygon, width, scale, in );
			id = lookup_layer( name_id, layer, on_layer, on_layer_data );
			view.insert( id, polygon, width, path_end_square );
		}
		else if ( text=="VIEWPORT" ) {
			text = advance_next_group_0( in );
		}
		else {
			// Print a warning
			if ( on_comment ) {
				std::string msg( "WARN:  An unsupported entity (" );
				msg.append( text );
				msg.append( ") was encountered and ignored." );
				on_comment( on_comment_data, msg.c_str() );
			}
			// Advance past the entity
			text = advance_next_group_0( in );
		}
	}

	assert( code == 0 );
	assert( text == "ENDSEC" );
}

layout* dxf::import( std::list<layout>& layouts, map_name_id const& name_id, unsigned scale,
	on_comment_handler on_comment, void* on_comment_data,
	std::istream& in )
{
	layout* cell = 0;

	// Read in the header section
	import_header( in );

	// Process the sections
	do {
		long code;
		string text;

		// Process the start of the section
		// Note, we might also see a end-of-file marker
		bool err = get_pair( in, code, text );
		if ( err || code!=0 ) throw std::runtime_error( INVALID_DXF "Expected a SECTION or EOF group code." );
		
		if ( text=="SECTION" ) {
			// okay, do nothing
		}
		else if ( text=="EOF" ) {
			// end of the file
			break;
		}
		else {
			throw std::runtime_error( INVALID_DXF "Expected a SECTION or EOF group code." );
		}

		// What type of section is this?
		err = get_pair( in, code, text );
		if ( err || code!=2 ) throw std::runtime_error( INVALID_DXF "Expected a section name group code." );

		if ( text=="ENTITIES" ) {
			if ( !cell ) {
				layouts.push_front( layout() );
				cell = & layouts.front();
			}
			assert( cell );
			import_entities( *cell, name_id, scale, on_comment, on_comment_data, in );
		}
		else
			skip_section( in );
	} while ( 1 );

	return cell;
}

layout* dxf::import( std::list<layout>& layouts, map_name_id& name_id, unsigned scale,
	on_comment_handler on_comment, void* on_comment_data,
	on_layer_handler on_layer, void* on_layer_data,
	std::istream& in )
{
	layout* cell = 0;

	// Read in the header section
	import_header( in );

	// Process the sections
	do {
		long code;
		string text;

		// Process the start of the section
		// Note, we might also see a end-of-file marker
		bool err = get_pair( in, code, text );
		if ( err || code!=0 ) throw std::runtime_error( INVALID_DXF "Expected a SECTION or EOF group code." );
		
		if ( text=="SECTION" ) {
			// okay, do nothing
		}
		else if ( text=="EOF" ) {
			// end of the file
			break;
		}
		else {
			throw std::runtime_error( INVALID_DXF "Expected a SECTION or EOF group code." );
		}

		// What type of section is this?
		err = get_pair( in, code, text );
		if ( err || code!=2 ) throw std::runtime_error( INVALID_DXF "Expected a section name group code." );

		if ( text=="ENTITIES" ) {
			if ( !cell ) {
				layouts.push_front( layout() );
				cell = & layouts.front();
			}
			assert( cell );
			import_entities( *cell, name_id, scale, on_layer, on_layer_data, on_comment, on_comment_data, in );
		}
		else
			skip_section( in );
	} while ( 1 );

	return cell;
}

bool dxf::is_file_dxf( std::istream& in )
{
	long code;
	string text;

	// The file must start with a section token
	bool err = get_pair( in, code, text );
	if ( err ) return false;
	if ( code != 0 ) return false;
	if ( text != "SECTION" ) return false;
	// The next code must be a header token
	err = get_pair( in, code, text );
	if ( err ) return false;
	if ( code != 2 ) return false;
	if ( text != "HEADER" ) return false;
	// Simply read in tokens, checking that they are well-formed, until
	// the end of the file looking for an EOF token.
	do {
		if ( in.eof() ) return false;
		err = get_pair( in, code, text );
		if ( err ) return false;
	} while ( code!=0 || text!="EOF" );
	// If everything so far has worked, assume that the file is a properly
	// formatted DXF
	return true;
}

