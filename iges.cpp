/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2012 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include "layout_export.h"
#include <assert.h>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <time.h>

using namespace liblayout;

namespace liblayout {
namespace iges {
	struct section_info {
		char type;
		int row;
		int col;

		void eol( std::ostream& ) throw();
		void eol( std::ostream&, int de_pointer ) throw();
		void send( std::ostream&, char const* text ) throw();
		void send( std::ostream&, char const* text, int de_pointer ) throw();
	};
}
}

void iges::section_info::eol( std::ostream& out ) throw()
{
	// First, fill out line column 73
	assert( col<=73 );
	for( ; col<73; ++col ) {
		out << ' ' ;
	}
	out << type; // ++col
	out << std::setw(7) << (++row) << '\n';
	col = 1;
}

void iges::section_info::eol( std::ostream& out, int de_pointer ) throw()
{
	// First, fill out line column 66
	assert( col<=66 );
	for( ; col<66; ++col ) {
		out << ' ' ;
	}
	out << std::setw(7) << de_pointer;
	out << type; // ++col
	out << std::setw(7) << (++row) << '\n';
	col = 1;
}

void iges::section_info::send( std::ostream& out, char const* text ) throw()
{
	size_t len = strlen(text);
	if (col+len>73) {
		eol( out );
	}

	out << text;
	col += len;
}

void iges::section_info::send( std::ostream& out, char const* text, int de_pointer ) throw()
{
	size_t len = strlen(text);
	if (col+len>66) {
		eol( out, de_pointer );
	}

	out << text;
	col += len;
}

static void write_string( std::ostream& out, iges::section_info& info, char const* text, bool eor=false ) 
{
	char buffer[74];
	char t = eor ? ';' : ',';
	sprintf( buffer, "%uH%s%c", (unsigned)strlen(text), text, t );
	info.send( out, buffer );
	if (eor) info.eol(out);
}

static void write_int( std::ostream& out, iges::section_info& info, int text ) 
{
	char buffer[74];

	sprintf( buffer, "%d,", text );
	info.send( out, buffer );
}

static void write_real( std::ostream& out, iges::section_info& info, double text ) 
{
	char buffer[74];

	sprintf( buffer, "%g,", text );
	info.send( out, buffer );
}

static void write_coord( std::ostream& out, iges::section_info& info, coord_t text, int de_pointer=0, bool eor=false ) 
{
	char buffer[74];
	char t = eor ? ';' : ',';

	sprintf( buffer, "%ld%c", text, t );
	info.send( out, buffer, de_pointer );
	if (eor) info.eol(out,de_pointer);
}

static void write_now( std::ostream& out, iges::section_info& info ) 
{
	time_t rawtime;
	struct tm* ptm;

	rawtime = time(NULL);
	ptm = gmtime(&rawtime);
	
	char buffer[74];
	sprintf( buffer, "%04d%02d%02d.%02d%02d%02d", ptm->tm_year+1900, ptm->tm_mon+1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec );
	write_string( out, info, buffer );
}

static int write_start( std::ostream& out )
{
	iges::section_info info = { 'S', 0, 1 };

	info.send( out, PACKAGE_NAME );
	info.eol( out );
	info.send( out, PACKAGE_VERSION );
	info.eol( out );

	return 2;
}

static int write_global( std::ostream& out, char const* product, char const* filename )
{
	iges::section_info info = { 'G', 0, 1 };

	// Field 1:  Parameter delimeter character
	write_string( out, info, "," );
	// Field 2:  Record delimiter character
	write_string( out, info, ";" );
	// Field 3:  Production identification from sending system
	write_string( out, info, product );
	// Field 4:  File name
	write_string( out, info, filename );
	// Field 5:  Native system ID
	write_string( out, info, PACKAGE_NAME );
	// Field 6:  Preprocessor version
	write_string( out, info, PACKAGE_NAME " " PACKAGE_VERSION);
	// Field 7:  Number of binary bits for integer representation
	write_int( out, info, 32 );
	// Field 8:
	write_int( out, info, 38 );
	// Field 9:
	write_int( out, info, 6 );
	// Field 10:
	write_int( out, info, 38 );
	// Field 11:
	write_int( out, info, 15 );
	// Field 12:  Product identification for the receiving system
	write_string( out, info, product );
	// Field 13:  Model space scale
	write_real( out, info, 1e-9 );
	// Field 14:  Units flag
	write_int( out, info, 6 /* meters */ );
	// Field 15:  Units name
	write_string( out, info, "M" /* meters */ );
	// Field 16:  Maximum number of line weight graduations
	write_int( out, info, 1 );
	// Field 17:  Width of maximum line weight in units
	write_real( out, info, 0.0254/72 /* one point */ );
	// Field 18:  date and time of exchange file generation
	write_now( out, info );
	// Field 19:  Minimum user-intended resolution
	write_real( out, info, 1e-10 );
	// Field 20:  Approximate maximum coordinate value occuring in the model
	write_real( out, info, 0.1 );
	// Field 21:  Name of author
	write_string( out, info, "Unspecified" );
	// Field 22:  Author's organization
	write_string( out, info, "Unspecified" );
	// Field 23:  Flag value corresponding to the version of the specification
	write_int( out, info, 3 );
	// Field 24:  Flag value corresponding to the version of the drafting standard 
	write_int( out, info, 0 );
	// Field 25:  Date and time the model was created or last modified
	write_now( out, info );
	// Field 26:  Descriptor indicating application protocol, application subset, ...
	write_string( out, info, "NULL", true );

	return info.row;
}

static void write_dir_polygon( std::ostream& out, iges::section_info& info,
	std::ostream& param_out, iges::section_info& param_info, layer_id layer, polygon const& pgn )
{
	char buffer[74];
	int de_pointer = info.row+1;
	int param_pointer = param_info.row+1;

	/* parameter data */
	write_int( param_out, param_info, 106 );
	write_int( param_out, param_info, 1 );
	write_int( param_out, param_info, pgn.size()+1 );
	write_real( param_out, param_info, 0.0 );
	for ( size_t lp=0; lp<pgn.size(); ++lp ) {
		write_coord( param_out, param_info, pgn[lp].x, de_pointer );
		write_coord( param_out, param_info, pgn[lp].y, de_pointer );
	}
	write_coord( param_out, param_info, pgn[0].x, de_pointer );
	write_coord( param_out, param_info, pgn[0].y, de_pointer, true );

	/* directory entry*/
	sprintf( buffer, "%8d%8d%8d%8d%8d%8d%8d%8d%8d", 
		106 /* entity type */,
		param_pointer, 0, 1 /*line font*/, layer /*level*/, 0 /*view*/, 0 /*Xformation*/, 
		0 /*label*/, 1/*status*/ );
	info.send( out, buffer ); info.eol(out);
	sprintf( buffer, "%8d%8d%8d%8d%8d%8s%8s%8s%8d", 
		106 /* linear path entity */,
		1 /*line weight*/, 0 /*color*/, param_info.row - param_pointer + 1 /*param line count*/,
		63 /*form number*/, "", "", "", 0/*status*/ );
	info.send( out, buffer); info.eol(out);
}

static void write_directory_and_param( std::ostream& out, std::vector<shape> const& shapes, 
	int& row_count_1, int& row_count_2 )
{
	iges::section_info d = { 'D', 0, 1 };
	iges::section_info p = { 'P', 0, 1 };
	std::stringstream pdata;

	// by pass all the shapes not in layer
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		// create polygon
		polygon pgn;
		lp->get_polygon( pgn );
		write_dir_polygon( out, d, pdata, p, lp->layer(), pgn );
	}

	out << pdata.str();

	row_count_1 = d.row;
	row_count_2 = p.row;
}

void static write_terminate( std::ostream& out, int const* lines )
{
	char buffer[82];

	sprintf( buffer, "S%7dG%7dD%7dP%7d%40sT%7d",
		lines[0], lines[1], lines[2], lines[3], "", 1 );
	out << buffer;
}

void	iges::write( layout const& view, layer_id layer, std::ostream& out )
{
	// get all of the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes, layer );

	int lines[4];

	// Write the first section, Start
	lines[0] = write_start( out );
	// Write the second section, Global
	lines[1] = write_global( out, view.get_name(), "filename" );
	// Write the directory
	write_directory_and_param( out, shapes, lines[2], lines[3] );
	// Write the terminate
	write_terminate( out, lines );
}

void	iges::write( layout const& view, std::ostream& out )
{
	// get all of the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes );

	int lines[4];

	// Write the first section, Start
	lines[0] = write_start( out );
	// Write the second section, Global
	lines[1] = write_global( out, view.get_name(), "filename" );
	// Write the directory
	write_directory_and_param( out, shapes, lines[2], lines[3] );
	// Write the terminate
	write_terminate( out, lines );
}
