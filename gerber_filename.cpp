/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2007-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains wrappers for the Gerber export routines.  These
** functions overload equivalent functions, except that they take a 
** null terminated string, open a file, and then replace the filename
** with the iostream.
**
*/

#include "layout_export.h"
#include <fstream>
#include <stdexcept>

using namespace liblayout;

void gerber::write( layout const& view, layer_id layer, double scale, char const* filename )
{
	std::ofstream file;

	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	write( view, layer, scale, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the cif file." );
	}
	file.close();
}

void gerber::write( layout const& view, double scale, char const* filename )
{
	std::ofstream file;

	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	write( view, scale, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the cif file." );
	}
	file.close();
}
