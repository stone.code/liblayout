/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#ifndef __LAYOUT_LOGGING_H_
#define __LAYOUT_LOGGING_H_

#include <string>
#include <sstream>

/// This namespace handles all logging from the liblayout.
namespace logging {

	/// A simple typedef for constant null-terminated strings.
	typedef char const* cstr;
	
	/// Enumeration of the severity levels that can be attached to logged
	/// messages.
	///
	/// The levels are chosen to match the syslog protocal.  Please refer
	/// to http://rfc.net/rfc3164.html.
	enum severity
	{
		EMERGENCY = 0,	/// system is unusable
		ALERT = 1,		/// action must be taken immediately
		CRITICAL = 2,	/// critical conditions
		ERROR = 3,		/// error conditions
		WARNING = 4,	/// warning conditions
		NOTICE = 5,		/// normal but significant condition
		INFORMATION = 6,/// informational messages
		DEBUG = 7		/// debug-level messages
	};

	/// Abstract interface to a logging back-end.
	class logging_imp
	{
	public:
		virtual 		~logging_imp() {};
		/// Write a log message to the back-end.
		virtual	void	write( severity, char const* message ) = 0;
		/// Get a null-terminated string specifying the name of this
		/// back-end.
		virtual cstr 	name() const = 0;
	};

	/// Concrete implementation of a backend that writes log messages to
	/// the standard output stream (stdout).
	class stdout_logging : public logging_imp
	{
	public:
		void	write( severity, char const* message );
		cstr 	name() const;
	};

	/// Concrete implementation of a backend that writes log messages to
	/// the standard error stream (stderr).
	class stderr_logging : public logging_imp
	{
	public:
		void	write( severity, char const* message );
		cstr 	name() const;
	};

	/// Set the logging back-end to the passed implementation.
	///
	/// The caller is required to ensure that the lifetime of the logging
	/// back-end lasts as long as required.  That is, the back-end's 
	/// lifetime must be at least until the backend is replaced, or no 
	/// further calls to print will occur.
	void		set_logging( logging_imp* );
	/// Set the logging back-end to an implementation that will write all
	/// message to the standard output stream.
	void		set_logging_stdout();
	/// Set the logging back-end to an implementation that will write all
	/// message to the standard error stream.
	void		set_logging_stderr();
	/// Get a pointer to the current logging back-end.
	logging_imp*	get_logging();
	/// Get the name of the current logging back-end.
	cstr			get_logging_name();
	/// Write a message to the log
	void		print( severity, char const* message );
	/// Write a message to the log
	void		print( severity, std::string const& message );	

	/// Helper class for creating logging messages
	class lout : private std::stringstream
	{
	public:
		lout( severity s ) { s_ = s; };
		~lout() { *this << '\0'; print( s_, str() ); }
		
		std::ostream& operator<<(char t)
		{ std::ostream& o=*this; o << t; return o; }
		template <class T>
		std::ostream& operator<<(T const& t )
		{ std::ostream& o=*this; o << t; return o; }

	private:
		severity s_;
	};

} // namespace logging

#endif // __LAYOUT_LOGGING_H_
