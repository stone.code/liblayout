/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>

using namespace liblayout;

text_shape::text_shape( layer_id id, char const* text_, point pt )
: shape::imp(id), text(text_), xy(pt), width(0)
{
	assert( width>=0 );
}

rect text_shape::bounds() const
{
	assert( false );
	return rect(0,0,0,0);
}

int text_shape::contains_point( point pt ) const
{
	assert( false );
	return int();
}

shape::imp* text_shape::eval( transform const& t ) const
{
	return new text_shape( layer, text.c_str(), t * xy );
}

void text_shape::get_polygon( polygon& p ) const
{
	assert( false );
}
