/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2007 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#pragma warning (disable:4786)

#include "../layout.h"
#include "triangulation.h"

using namespace liblayout::triangulation;
using namespace std;

static void convert( vector<Polygon_2>& out, std::vector<liblayout::shape> const& in )
{
	std::vector<liblayout::shape>::const_iterator lp;
	for ( lp = in.begin(); lp != in.end(); ++lp )
	{
		liblayout::polygon p;
		lp->get_polygon( p );
		
		out.push_back( Polygon_2() );
		
		liblayout::polygon::const_iterator point;
		for ( point = p.begin(); point != p.end(); ++point )
			out.back().push_back( Kernel::Point_2( point->x, point->y ) );

		if ( out.back().is_clockwise_oriented() )
			out.back().reverse_orientation();
	}
}

static void insert( CDT& out, Polygon_with_holes_2::General_polygon_2 const& polygon )
{
	typedef Polygon_with_holes_2::General_polygon_2::Edge_const_iterator iterator;

	assert( polygon.size() > 2 );
	
	for ( iterator lp = polygon.edges_begin(); lp != polygon.edges_end(); ++lp )
	{
		CDT::Vertex_handle v1 = out.insert( lp->source() );
		CDT::Vertex_handle v2 = out.insert( lp->target() );
		out.insert_constraint( v1, v2 );
	}
}

static void fill_region( CDT::Face_handle face )
{
	assert( !face->info() );
	face->info() = true;
	for ( int i = 0; i<3; ++i )
		if ( !face->is_constrained(i) && !face->neighbor(i)->info() ) fill_region( face->neighbor(i) );
}

static void insert2( CDT& out, Polygon_with_holes_2::General_polygon_2 const& polygon )
{
	typedef Polygon_with_holes_2::General_polygon_2::Edge_const_iterator iterator;

	assert( polygon.size() > 2 );
	
	for ( iterator lp = polygon.edges_begin(); lp != polygon.edges_end(); ++lp )
	{
		CDT::Locate_type lt;
		int li;
		CDT::Face_handle face = out.locate( lp->target(), lt, li );
		assert( lt == CDT::VERTEX );
		CDT::Vertex_handle v1 = face->vertex(li);
		assert( v1->point() == lp->target() );
		face = out.locate( lp->source(), lt, li, face );
		assert( lt == CDT::VERTEX );
		CDT::Vertex_handle v2 = face->vertex(li);
		assert( v2->point() == lp->source() );

		CDT::Edge_circulator edge = out.incident_edges( v1 );
		do {
			int i = (edge->second + 1) % 3;
			int j = (edge->second + 2) % 3;
			assert( edge->first->vertex(j) == v1 );
			if ( edge->first->vertex(i) == v2 ) break;
			++edge;
		} while (true);
		// Set the flag
		assert( !out.is_infinite( edge->first ) );
		if ( !edge->first->info() ) fill_region( edge->first );
	}
}

static void check_collinear_segements( Polygon_2& polygon )
{
	assert( polygon.size() >= 3 );

	// is the last point in the polygon collinear?
	int size = polygon.size();
	while ( CGAL::collinear( polygon[size-2], polygon[size-1], polygon[0] ) )
	{
		polygon.erase( polygon.vertices_begin() + size -1 );
		--size;
		assert( size == polygon.size() );
		assert( size >= 3 ); 
	}
	
	// now we can look for collinear points within that don't wrap
	for ( int pos = polygon.size()-1; pos>1; --pos )
	{
		if ( CGAL::collinear( polygon[pos-2], polygon[pos-1], polygon[pos] ) )
		{
			polygon.erase( polygon.vertices_begin() + pos - 1 );
		}
	}
}

static void convert( CDT& out, Polygon_set_2 const& in )
{
	assert( in.is_valid() );
	assert( in.number_of_polygons_with_holes() > 0 );
	
	std::vector<Polygon_with_holes_2> polygons;
	polygons.reserve( in.number_of_polygons_with_holes() );
	in.polygons_with_holes( back_inserter( polygons ) );
	
	std::vector<Polygon_with_holes_2>::iterator lp = polygons.begin();
	for ( lp = polygons.begin(); lp != polygons.end(); ++lp )
	{
		// add the outer boundary
		check_collinear_segements( lp->outer_boundary() );
		insert( out, lp->outer_boundary() );
		
		// add inner boundaries
		Polygon_with_holes_2::Hole_const_iterator lph;
		for ( lph = lp->holes_begin(); lph != lp->holes_end(); ++lph )
			insert( out, *lph );

		// marks the faces
		insert2( out, lp->outer_boundary() );
	}
}

void liblayout::triangulation::get_triangles( vector<triangle>& out, vector<liblayout::shape> const& in )
{
	assert( in.size()>0 );
	
	std::vector<Polygon_2> polygons_a;
	polygons_a.reserve( in.size() );
	convert( polygons_a, in );
	
	Polygon_set_2 polygons_b;
	polygons_b.join( polygons_a.begin(), polygons_a.end() );
	
	CDT triangulation;
	convert( triangulation, polygons_b );
	
	// TODO:
	// it would be possible to try to simplify the triangulation
	// at this point.  We could remove redundant constraints on edges.
	// We could remove unnecessary vertices.
	

	// Transfer our marks triangles to the output
	CDT::Finite_faces_iterator face;
	for ( face = triangulation.finite_faces_begin(); face != triangulation.finite_faces_end(); ++face )
	{
		// skip faces that are not marked
		if ( ! face->info() ) continue;
		// write out a triangle
		CDT::Triangle tmp( triangulation.triangle( face ) );
		triangle tmp2 = { tmp[0].x(), tmp[0].y(), tmp[1].x(), tmp[1].y(), tmp[2].x(), tmp[2].y() };
		out.push_back( tmp2 );
		// some checks to ensure data integrity
		assert( face->is_constrained(0) || face->neighbor(0)->info() );
		assert( face->is_constrained(1) || face->neighbor(1)->info() );
		assert( face->is_constrained(2) || face->neighbor(2)->info() );
	}
}

void liblayout::triangulation::get_triangles( vector<triangle>& out, liblayout::layout const* view, liblayout::layer_id layer )
{
	std::vector<liblayout::shape> shapes;
	view->get_shapes( shapes, layer );
	get_triangles( out, shapes );
}
