#include "triangulation.h"
#include <stdlib.h>
#include <stdio.h>
#include <list>
#include <iostream>

using namespace liblayout;

#define INCHES_PER_METER (39.37)

static void write_header( std::ostream& out, char const* title, int pages )
{
	out << "%!PS-Adobe-2.0\n";
	out << "%%Creator:  " PACKAGE_NAME " " PACKAGE_VERSION "\n";
	if ( title && *title ) out << "%%Title:  " << title << '\n';
	if ( pages>0 ) out << "%%Pages:  " << pages << '\n';
	out << "/page-begin { gsave 0 setgray /Helvetica-Bold findfont " << psmask::font_size << " scalefont setfont } def\n";
	out << "/page-end { grestore showpage } def\n";
	out << "/page-header { exch " << psmask::font_size << ' ' << psmask::font_size << " moveto show\n";
	out << " dup stringwidth pop currentpagedevice /PageSize get 0 get exch sub " << psmask::font_size << " sub " << psmask::font_size*1.5 << " moveto show } def\n";
	out << "/set-darkfield { 0 0 moveto\n";
	out << " currentpagedevice /PageSize get 0 get 0 lineto\n";
	out << " currentpagedevice /PageSize get 0 get currentpagedevice /PageSize get 1 get lineto\n";
	out << " 0 currentpagedevice /PageSize get 1 get lineto\n";
	out << " closepath fill 1 setgray } def\n";
}

static void write_scalefunc( std::ostream& out, rect const& bounds, double scale )
{
	assert( bounds.valid() );

	double s = scale * 72 /*points per inch*/ * INCHES_PER_METER;

	out << "/page-scale { currentpagedevice /PageSize get dup\n";
	out << "1 get 2 div exch 0 get 2 div exch translate\n"; // center the page
	out << s << ' ' << s << " scale\n"; // set the scaling
	out << -bounds.center_x() << ' ' << -bounds.center_y() << " translate } def\n"; // offset to center of layout
}

static void write_footer( std::ostream& out )
{
	out << "%%EOF" << std::endl;
}

static void write_shape( std::ostream& file, triangulation::triangle const & view )
{
	file << view.x1 << ' ' << view.y1 << " moveto\n";
	file << view.x2 << ' ' << view.y2 << " lineto\n";
	file << view.x3 << ' ' << view.y3 << " lineto\n";
	file << "closepath gsave 0.5 setgray fill grestore stroke\n";
}

static void create_page( std::ostream& out, std::vector<triangulation::triangle> const& view, bool darkfield, char const* layer_name, short page )
{
	// print out page header
	out << "%%Page:  " << page << ' ' << page << std::endl;
	out << "page-begin\n";
	if( darkfield ) out << "set-darkfield\n";
	out << "(temp layout) ";
	out << "(temp layer) page-header\n";
	out << "page-scale\n";
	
	// write out shapes
	std::vector<triangulation::triangle>::const_iterator shape;
	for ( shape = view.begin(); shape != view.end(); ++shape ) {
		write_shape( out, *shape );
	}

	// finish the page
	out << "page-end" << std::endl;
}

static void process( char const* filename )
{
	std::list<layout>	layouts;
	layout*			top_layout = 0;
	cif::map_name_id		name_to_id;

	// import the layout
	top_layout = cif::import( layouts, name_to_id, true, filename );
	assert( top_layout );

	std::vector<triangulation::triangle> triangles;

	triangulation::get_triangles( triangles, top_layout, 1 );

	rect bounds = top_layout->bounds();
	assert( bounds.valid() );

	// create the output
	write_header( std::cout, "temp", 1 );
	write_scalefunc( std::cout, bounds, 1e-4 );
	// create the page	
	create_page( std::cout, triangles, false, "layer", 1 );
	// create the file header
	write_footer( std::cout );
}

int main( int argc, char* argv[] )
{
	if ( argc!=2 ) 
	{
		printf( "please provide filename for the layout.\n" );
		return EXIT_FAILURE;
	}
	assert( argc==2 );
	assert( argv[1] );
	assert( *argv[1] );
	process( argv[1] );
}
