#include "../layout.h"
#include <CGAL/Cartesian.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/Polygon_set_2.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Triangulation_euclidean_traits_2.h>
#include <vector>

namespace liblayout {
namespace triangulation {
	typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;

	typedef CGAL::Polygon_2<Kernel>                    Polygon_2;
	typedef CGAL::Polygon_with_holes_2<Kernel>         Polygon_with_holes_2;
	typedef CGAL::Polygon_set_2<Kernel>                Polygon_set_2;

	class Face_info {
	public:
		Face_info() : is_dark(false) {};
		Face_info( bool rhs ) : is_dark(rhs) {};

		operator bool&() { return is_dark; };
		operator bool() const { return is_dark; };

		bool is_dark;
	};

	typedef CGAL::Triangulation_vertex_base_2<Kernel>                Vb;
	typedef CGAL::Constrained_triangulation_face_base_2<Kernel>      Fb_base;
	typedef CGAL::Triangulation_euclidean_traits_2<Kernel>			 Traits;
	typedef CGAL::Triangulation_face_base_with_info_2<Face_info,Traits,Fb_base> Fb;
	typedef CGAL::Triangulation_data_structure_2<Vb,Fb>              TDS;
	typedef CGAL::Constrained_Delaunay_triangulation_2<Kernel, TDS> CDT;
	
	struct triangle
	{
		double x1, y1, x2, y2, x3, y3;
	};
	
	void get_triangles( std::vector<triangle>& out, layout const* view, layer_id layer );
	void get_triangles( std::vector<triangle>& out, std::vector<shape> const& shapes );
};
};
