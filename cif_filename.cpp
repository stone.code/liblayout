/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Export to CalTech Interchange Format (CIF).
**
*/

#include "layout.h"
#include <fstream>
#include <stdexcept>


using namespace liblayout;

layout*	cif::import( std::list<layout>& layouts, map_name_id const& layer_map, 
	on_comment_handler on_comment, void* on_comment_userdata, 
	char const* filename )
{
	std::ifstream file;
	
	file.open( filename, std::ios::in );
	if ( !file ) {
		throw std::runtime_error( std::string( "Could not open the following file for reading:  " ) + filename );
	}
	return import( layouts, layer_map, on_comment, on_comment_userdata, file );
}

layout*	cif::import( std::list<layout>& layouts, map_name_id& layer_map, 
	on_comment_handler on_comment, void* on_comment_userdata,
	on_layer_handler on_layer, void* on_layer_userdata, 
	char const* filename )
{
	std::ifstream file;

	file.open( filename, std::ios::in );
	if ( !file ) {
		throw std::runtime_error( std::string( "Could not open the following file for reading:  " ) + filename );
	}
	return import( layouts, layer_map, on_comment, on_comment_userdata, 
		on_layer, on_layer_userdata, file );
}

cif::strings	cif::import_layers( char const* filename )
{
	std::ifstream file;

	file.open( filename, std::ios::in );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for reading:  " ) + filename );
	}
	return import_layers( file );
}

void cif::write( layout const& view, map_id_name& layer_map, bool keep_unknown_layers, char const* filename )
{
	std::ofstream file;

	// create the file
	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	write( view, layer_map, keep_unknown_layers, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the CIF file." );
	}
}

bool cif::is_file_cif( char const* filename )
{
	std::ifstream file;

	file.open( filename, std::ios::in );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for reading:  " ) + filename );
	}
	return is_file_cif( file );
}
