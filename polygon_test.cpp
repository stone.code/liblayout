/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2015 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include "vendor/catch.hpp"

using liblayout::point;
using liblayout::polygon;

TEST_CASE( "polygon test 1" )
{
	polygon p;
	
	p.push_back( point(0,0) );
	REQUIRE( !p.valid() );
	p.push_back( point(0,1) );
	REQUIRE( !p.valid() );
	p.push_back( point(1,1) );
	REQUIRE( p.valid() );
}

TEST_CASE( "polygon test 2" )
{
	polygon p;
	
	p.push_back( point(0,0) );
	REQUIRE( !p.valid() );
	p.push_back( point(0,1) );
	REQUIRE( !p.valid() );
	p.push_back( point(0,2) );
	REQUIRE( !p.valid() );
	p.push_back( point(1,1) );
	REQUIRE( p.valid() );
}
