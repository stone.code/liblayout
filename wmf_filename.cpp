/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include <fstream>
#include <stdexcept>

using namespace liblayout;

void wmf::write( layout const& view, map_id_style const& layer_map, double mag, char const* filename )
{
	std::ofstream file;

	file.open( filename, std::ios::out | std::ios::binary );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	wmf::write( view, layer_map, mag, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the cif file." );
	}
	file.close();
}

void wmf::write( layout const& view, layer_id layer, map_id_style const& layer_map, double mag, char const* filename )
{
	std::ofstream file;

	file.open( filename, std::ios::out | std::ios::binary );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	wmf::write( view, layer, layer_map, mag, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the cif file." );
	}
	file.close();
}
