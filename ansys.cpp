/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains most of the export routines associated with
** exporting layout information to ANSYS Neutral Format.
**
*/

#pragma warning (disable:4786)

#include "layout_export.h"
#include "layout_shapes.h"
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <stdexcept>

using namespace liblayout;
using namespace std;

static long get_keypoint( std::map<point,long>& kps, point pt, std::ostream& out )
{
	std::map<point,long>::const_iterator lp = kps.find( pt );
	if ( lp == kps.end() ) 
	{
		long new_kp = kps.size()+1;
		kps[ pt ] = new_kp;
		out << "K," << new_kp << ',' << pt.x << ',' << pt.y << '\n';
		assert( kps[pt] == new_kp );
		return new_kp;
	}
	else
	{
		return lp->second;
	}
}

static void write_shape( std::ostream& out, std::map<point,long>& kps, shape const& view )
{
	// write out the shape
	rect_shape const* rect;
	polygon_shape const* poly;

	assert( view.get() );

	if ( rect = dynamic_cast<rect_shape const*>( view.get() ) )
	{
		out << "RECT," << rect->x1 << ',' << rect->x2 << ',' << rect->y1 << ',' << rect->y2 << '\n';
	}
	else if ( poly = dynamic_cast<polygon_shape const*>( view.get() ) )
	{
		// ensure that all kps are created
		polygon::const_iterator lp;
		for ( lp = poly->begin(); lp != poly->end(); ++lp )
			get_keypoint( kps, *lp, out );
		// create area
		if ( poly->size() <= 18 )
		{
			out << "A";
			for ( lp = poly->begin(); lp != poly->end(); ++lp )
				out << ',' << kps[*lp];
			out << '\n';
		}
		else
		{
			out << "FLST,2," << poly->size() << ",3\n";
			for ( lp = poly->begin(); lp != poly->end(); ++lp )
				out << "FITEM,2," << kps[*lp] << '\n';
			out << "A,P51X\n";
		}
	}
	else
	{
		// create polygon
		polygon p;
		view.get_polygon( p );
		// ensure that all kps are created
		polygon::const_iterator lp;
		for ( lp = p.begin(); lp != p.end(); ++lp )
			get_keypoint( kps, *lp, out );
		// create area
		if ( p.size() <= 18 )
		{
			out << "A";
			for ( lp = p.begin(); lp != p.end(); ++lp )
				out << ',' << kps[*lp];
			out << '\n';
		}
		else
		{
			out << "FLST,2," << p.size() << ",3\n";
			for ( lp = p.begin(); lp != p.end(); ++lp )
				out << "FITEM,2," << kps[*lp] << '\n';
			out << "A,P51X\n";
		}
	}
}

static void write_header( std::ostream& out, char const* title )
{
	assert( title && *title );

	out << "! ANSYS NEUTRAL FORMAT\n";
	out << "!\n";
	if ( title && *title ) out << "! Layout title " << title << '\n';
	out << "! Written by " PACKAGE_NAME " " PACKAGE_VERSION "\n";
	out << "!\n";
	out << "FINISH\n";
	out << "/PREP7\n";
}

static void write_footer( std::ostream& out )
{
	out << "FINISH\n";
	out << "!\n";
	out << "! Done.\n";
}

void ansys::write( layout const& view, layer_id layer, std::ostream& out )
{
	std::map<point,long>	kps;

	// print out header
	write_header( out, view.get_name() );

	// get all of the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes, layer );

	// by pass all the shapes not in layer
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		assert( lp->layer() == layer );
		write_shape( out, kps, *lp );
	}

	// print out the footer
	write_footer( out );
}

void ansys::write( layout const& view, std::ostream& out )
{
	std::map<point,long>	kps;

	// print out header
	write_header( out, view.get_name() );

	// get all of the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes );
	std::sort( shapes.begin(), shapes.end() );

	// setup first material
	layer_id layer = shapes.front().layer();
	out << "MAT," << layer << '\n';

	// by pass all the shapes not in layer
	std::vector<shape>::const_iterator lp = shapes.begin();
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		// make sure the shape is in the proper layer
		if ( lp->layer() != layer )
		{
			layer = lp->layer();
			out << "MAT," << layer << '\n';
		}

		write_shape( out, kps, *lp );
	}

	out << "FINISH\n";
	out << "!\n";
	out << "! Done.\n";
}

