/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#ifndef __LAYOUT_SHAPES_H_
#define __LAYOUT_SHAPES_H_

#include "layout.h"
#include <string>

namespace liblayout
{

	class rect_shape : public shape::imp, public rect
	{
	public:
		rect_shape( layer_id id, rect const& rhs );

		rect		bounds() const;
		int		contains_point( point pt ) const;
		shape::imp*	eval( transform const& t ) const;
		void		get_polygon( polygon& ) const;
	};
	
	class circle_shape : public shape::imp
	{
	public:
		circle_shape( layer_id id, coord_t radius, point center );
		
		rect		bounds() const;
		int		contains_point( point pt ) const;
		shape::imp*	eval( transform const& t ) const;
		void		get_polygon( polygon& ) const;

	public:
		coord_t		radius;
		point		center;
	};

	class path_shape : public shape::imp, public std::vector<point>
	{
	public:
		path_shape( layer_id id, std::vector<point> const& rhs, coord_t width, path_end_t path_end );

		rect		bounds() const;
		int		contains_point( point pt ) const;
		shape::imp*	eval( transform const& t ) const;
		void		get_polygon( polygon& ) const;
		bool		is_dot() const;
		
	public:
		coord_t		width;
		path_end_t	path_end;
	};

	class polygon_shape : public shape::imp, public polygon
	{
	public:
		polygon_shape( layer_id id, std::vector<point> const& rhs );

		rect		bounds() const;
		int		contains_point( point pt ) const;
		shape::imp*	eval( transform const& t ) const;
		void		get_polygon( polygon& ) const;
	};

	class text_shape : public shape::imp
	{
	public:
		text_shape( layer_id id, char const* text, point pt );

		rect		bounds() const;
		int		contains_point( point pt ) const;
		shape::imp*	eval( transform const& t ) const;
		void		get_polygon( polygon& ) const;

	public:
		std::string	text;
		long		width;
		point		xy;
	};

	class layout_composite : public composite::imp
	{
	public:
		layout_composite( layout* l, transform const& t );
		layout_composite( layout* l, transform const& t, int rows, int cols, point delta_r, point delta_c );

		rect			bounds() const;
		int			contains_point( point pt ) const;
		composite::imp*		eval( transform const& t ) const;
		void			get_layers( std::set<layer_id>& ) const;
		void			get_shapes( std::vector<shape>& ) const;
		void			get_shapes( std::vector<shape>&, layer_id ) const;
		bool			has_bounds() const;

		int			get_cols() const { return cols; };
		point			get_delta_r() const { return delta_r; };
		point			get_delta_c() const { return delta_c; };
		transform&		get_transform() { return t_; };
		transform const&	get_transform() const { return t_; };
		layout*			get_layout() { return layout_; };
		layout const*		get_layout() const { return layout_; };
		int			get_rows() const { return rows; };
		void			set_cols( int );
		void			set_rows( int );
		void			set_spacing( point, point );
		bool			valid() const;

	private:
		layout*			layout_;
		transform		t_; // transform of layout data
		int			rows;
		int			cols;
		// do not apply transform to the following
		// when layout out rows and columns
		point			delta_r; // direction to lay out rows
		point			delta_c; // direction to lay out columns
	};

	class layout_name_composite : public composite::imp
	{
	public:
		layout_name_composite( char const* name, transform const& t );
		layout_name_composite( char const* name, transform const& t, int rows, int cols, point delta_r, point delta_c );
		layout_name_composite( std::string const& name, transform const& t, int rows, int cols, point delta_r, point delta_c );

		rect			bounds() const;
		int			contains_point( point pt ) const;
		composite::imp*		eval( transform const& t ) const;
		void			get_layers( std::set<layer_id>& ) const;
		void			get_shapes( std::vector<shape>& ) const;
		void			get_shapes( std::vector<shape>&, layer_id ) const;
		bool			has_bounds() const;

		composite::imp*		dereference( std::list<layout>& ) const;
		int			get_cols() const { return cols; };
		point			get_delta_r() const { return delta_r; };
		point			get_delta_c() const { return delta_c; };
		layout*			get_layout( std::list<layout>& ) const;
		transform&		get_transform() { return t_; };
		transform const&	get_transform() const { return t_; };
		char const*		get_name() const { return name_.c_str(); };
		int			get_rows() const { return rows; };
		void			set_cols( int );
		void			set_name( char const* name ) { name_=name; };
		void			set_rows( int );
		void			set_spacing( point, point );
		bool			valid() const;

	private:
		std::string		name_;
		transform		t_; // transform of layout data
		int			rows;
		int			cols;
		// do not apply transform to the following
		// when layout out rows and columns
		point			delta_r; // direction to lay out rows
		point			delta_c; // direction to lay out columns
	};

} // liblayout

#endif // __LAYOUT_SHAPES_H_
