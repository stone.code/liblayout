/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>

using namespace liblayout;

bool composite::is_layout() const
{
	return !! dynamic_cast<layout_composite*>( ptr );
}

void composite::imp::sub_ref()
{
	assert( ref_count>0 );
	--ref_count;
	if ( ref_count==0 ) delete this;
}
