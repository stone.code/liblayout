/*
** This code is distributed under a modified BSD license.  Please refer
** to the accompanying LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
*/

#include <assert.h>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <stdlib.h>
#include "layout.h"

//////////////////////////////////////////////////////
// Forward function declarations
static void print_usage( std::ostream& );
static void print_version( std::ostream& );
static void process( char const* filename );

//////////////////////////////////////////////////////
// Program entry
int main( int argc, char* argv[] )
{
	// check that we have at least one parameter, this is in 
	// addition to the program name
	if ( argc<2 ) {
		print_usage( std::cerr );
		return EXIT_FAILURE;
	}
	
	// if we have exactly two arguments, we might be requesting
	// either help or version information 
	// for help, then
	if ( argc==2 && argv[1][0]=='-' )
	{
		// there are one command-line argument
		// the argument is a command switch
		
                // first, ignore first dash if there are two
                if ( argv[1][1]=='-' ) ++argv[1];
                assert( argv[1][0]=='-' );
                
		// is it a request for help?
		if ( tolower( argv[1][1] ) == 'h' )
		{
			print_usage( std::cout );
			return EXIT_SUCCESS;
		}
		
		// is it a request for version information?
		if ( tolower( argv[1][1] ) == 'v' )
		{
			print_version( std::cout );
			return EXIT_SUCCESS;
		}
	}
	
	// skip the program name
	// process all other parameters as if they were a filename
	while ( * ++argv ) process( *argv );

	// processing is complete
	return EXIT_SUCCESS;
}

static void print_usage( std::ostream& out )
{
	out <<	"Usage for layout-identify:\n"
		"\tlayout-identify filename [filename ...]\n"
		"\tlayout-identify -h\t\tRequest help.  Print this message.\n"
		"\tlayout-identify -v\t\tRequest version information.\n";
}

static void print_version( std::ostream& out )
{
	out <<	
	out <<	PACKAGE_NAME ":  " PACKAGE_VERSION "\n"
		"Layout Library:  " << liblayout_version_zstring() << '\n';
}

static void process( char const* filename )
{
	try {
		// Try to open the file and read
		// This test is non-specific, and makes sure the file
		// is indeed available for reading.
		{
			std::ifstream file( filename );
			char buffer[1];
			file.read( buffer, 1 );
			if ( file.fail() )
				throw std::runtime_error( "Cannot read from file." );
		}

		if ( liblayout::gdsii::is_file_gdsii( filename ) ) {
			std::cout << "GDSII\n";
			return;
		}
		if ( liblayout::cif::is_file_cif( filename ) ) {
			std::cout << "CIF\n";
			return;
		}
		if ( liblayout::dxf::is_file_dxf( filename ) ) {
			std::cout << "DXF\n";
			return;
		}
	
		printf( "UNKNOWN\n" );
	}
	catch ( std::exception& e ) {
		std::cout << "CANTREAD\t" << e.what() << '\n';
	}
	catch ( ... ) {
		std::cout << "CANTREAD\tUnknown reason\n";
	}
}

