/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include <assert.h>
#include <string.h>

std::ostream& liblayout::operator<<( std::ostream& out, liblayout::manhattan_transform mt )
{
	switch ( mt ) {
		case R0:
			out << "R0";
			break;
		case R90:
			out << "R90";
			break;
		case R180:
			out << "R180";
			break;
		case R270:
			out << "R270";
			break;
		case MX:
			out << "MX";
			break;
		case MXR90:
			out << "MXR90";
			break;
		case MY:
			out << "MY";
			break;
		case MYR90:
			out << "MYR90";
			break;
		default:
			assert(false);
			out << "R?";
			break;
	};
	
	return out;
}

bool liblayout::is_manhattan_transform( char const* text )
{
	if ( strcmp(text,"R0")==0 ) return true;
	if ( strcmp(text,"R90")==0 ) return true;
	if ( strcmp(text,"R180")==0 ) return true;
	if ( strcmp(text,"R270")==0 ) return true;
	if ( strcmp(text,"MX")==0 ) return true;
	if ( strcmp(text,"MXR90")==0 ) return true;
	if ( strcmp(text,"MY")==0 ) return true;
	if ( strcmp(text,"MYR90")==0 ) return true;
	return false;
}

liblayout::manhattan_transform liblayout::get_manhattan_transform( char const* text )
{
	if ( strcmp(text,"R0")==0 ) return R0;
	if ( strcmp(text,"R90")==0 ) return R90;
	if ( strcmp(text,"R180")==0 ) return R180;
	if ( strcmp(text,"R270")==0 ) return R270;
	if ( strcmp(text,"MX")==0 ) return MX;
	if ( strcmp(text,"MXR90")==0 ) return MXR90;
	if ( strcmp(text,"MY")==0 ) return MY;
	if ( strcmp(text,"MYR90")==0 ) return MYR90;
	assert( false );
	return R0;
}
