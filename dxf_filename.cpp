/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include <fstream>
#include <stdexcept>

using namespace liblayout;

layout* dxf::import( std::list<layout>& layouts, map_name_id const& name_id, unsigned scale,
	on_comment_handler on_comment, void* on_comment_userdata,
	char const* filename )
{
	std::ifstream file;

	// create the file
	file.open( filename, std::ios::in );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for reading:  " ) + filename );
	}
	return dxf::import( layouts, name_id, scale, on_comment, on_comment_userdata, file );
}

layout* dxf::import( std::list<layout>& layouts, map_name_id& name_id, unsigned scale,
	on_comment_handler on_comment, void* on_comment_userdata,
	on_layer_handler on_layer, void* on_layer_userdata,
	char const* filename )
{
	std::ifstream file;

	// create the file
	file.open( filename, std::ios::in );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for reading:  " ) + filename );
	}
	return dxf::import( layouts, name_id, scale,
		on_comment, on_comment_userdata, 
		on_layer, on_layer_userdata,
		file );
}

void	dxf::write( layout const& view, map_id_name const& id_to_name, double scale, char const* filename )
{
	std::ofstream file;

	// create the file
	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	dxf::write( view, id_to_name, scale, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the DXF file." );
	}
}

void	dxf::write( layout const& view, layer_id layer, map_id_name const& id_to_name, double scale, char const* filename )
{
	std::ofstream file;

	// create the file
	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	dxf::write( view, layer, id_to_name, scale, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the DXF file." );
	}
}

bool	dxf::is_file_dxf( char const* filename )
{
	std::ifstream file;

	file.open( filename, std::ios::in );
	if ( !file ) 
	{
		throw std::runtime_error( std::string( "Could not open the following file for reading:  " ) + filename );
	}
	return is_file_dxf( file );
}

