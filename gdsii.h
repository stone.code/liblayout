/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This header contains the record type definitions for the GDSII file
** format.
**
*/

#ifndef __LAYOUT_GDSII_H_
#define __LAYOUT_GDSII_H_

enum gdsii_record_type
{
	HEADER = 0x0002,
	BGNLIB = 0x0102,
	LIBNAME = 0x0206,
	UNITS = 0x0305,
	ENDLIB = 0x0400,
	BGNSTR = 0x0502,
	STRNAME = 0x0606,
	ENDSTR = 0x0700,
	BOUNDARY = 0x0800,
	PATH = 0x0900,
	SREF = 0x0a00,
	AREF = 0x0b00,
	TEXT = 0x0c00,
	LAYER = 0x0d02,
	DATATYPE = 0x0e02,
	WIDTH = 0x0f03,
	XY = 0x1003,
	ENDEL = 0x1100,
	SNAME = 0x1206,
	COLROW = 0x1302,
	TEXTTYPE = 0x1602,
	PRESENTATION = 0x1701,
	STRING = 0x1906,
	STRANS = 0x1A01,
	MAG = 0x1B05,
	ANGLE = 0x1C05,
	REFLIBS = 0x1F06,
	FONTS = 0x2006,
	GENERATIONS = 0x2202,
	ATTRTABLE = 0x2306,
	PATHTYPE = 0x2102,
	ELFLAGS = 0x2601,
	PLEX = 0x2f03,
	BGNEXTN = 0x3003,
	ENDEXTN = 0x3103,
	FORMAT = 0x3602,
	MASK = 0x3706,
	ENDMASKS = 0x3800
};

#endif // __LAYOUT_GDSII_H_
