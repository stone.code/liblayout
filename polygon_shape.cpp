/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>

using namespace liblayout;

polygon_shape::polygon_shape( layer_id id, std::vector<point> const& rhs )
: shape::imp(id), polygon(rhs)
{
	normalize();
}

rect polygon_shape::bounds() const
{
	return polygon::bounds();
}

int polygon_shape::contains_point( point pt ) const
{
	assert( false );
	return int();
}

shape::imp* polygon_shape::eval( transform const& t ) const
{
	polygon p( *this );
	
	t.apply( p );
	return new polygon_shape( layer, p );
}

void polygon_shape::get_polygon( polygon& p ) const
{
	p.clear();
	std::copy( begin(), end(), std::back_inserter( p ) );
}

