/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>
#include <iostream>

using namespace liblayout;

static void write_HEADER( std::ostream& out )
{
	out << "  0\n";
	out << "SECTION\n";
	out << "  2\n";
	out << "HEADER\n";
	out << "  0\n";
	out << "ENDSEC\n";
}

static void write_start_ENTITIES( std::ostream& out )
{
	out << "  0\n";
	out << "SECTION\n";
	out << "  2\n";
	out << "ENTITIES\n";
}

static void write_end_ENTITIES( std::ostream& out )
{
	out << "  0\n";
	out << "ENDSEC\n";
}

static void write_EOF( std::ostream& out )
{
	out << "  0\n";
	out << "EOF\n";
}

static void write_point( std::ostream& out, coord_t x, coord_t y, double scale, bool final_point )
{
	out << "  10\n";
	out << (x*scale) << '\n';
	out << "  20\n";
	out << (y*scale) << '\n';
	out << "  30\n0\n";
	if ( final_point )
		out << "  0\nSEQEND\n";
	else
		out << "  0\nVERTEX\n";
}

static void write_polygon( std::ostream& out, polygon const& view, double scale, char const* layer )
{
	out << "  0\nPOLYLINE\n"; // start of a polyline
	out << "  70\n1\n"; // flag indicating closed polyline (polygon)
	out << "  8\n" << layer << '\n';

	// write out the polygon points
	for( polygon::const_iterator lp=view.begin(); lp!=view.end(); ++lp ) 
		write_point( out, lp->x, lp->y, scale, false );
	if ( view.begin()!=view.end() ) 
		write_point( out, view.begin()->x, view.begin()->y, scale, true );
}

static void write_shape( std::ostream& out, shape const& view, double scale, char const* layer )
{
	assert( layer );
	
	circle_shape const* circle = 0;
	rect_shape const* box = 0;
	polygon_shape const* poly = 0;

	if ( circle = dynamic_cast<circle_shape const*>( view.get() ) ) {
		out << "  0\nCIRCLE\n";	// start of circle
		out << "  8\n" << layer << '\n';
		out << "  10\n" << (circle->center.x*scale) << '\n';
		out << "  20\n" << (circle->center.y*scale) << '\n';
		out << "  40\n" << (circle->radius*scale) << '\n';
	}
	else if ( box = dynamic_cast<rect_shape const*>( view.get() ) ) {
		out << "  0\nPOLYLINE\n"; // start of a polyline
		out << "  70\n1\n"; // flag indicating closed polyline (polygon)
		out << "  8\n" << layer << '\n';
		write_point( out, box->x1, box->y1, scale, false );
		write_point( out, box->x2, box->y1, scale, false );
		write_point( out, box->x2, box->y2, scale, false );
		write_point( out, box->x1, box->y2, scale, false );
		write_point( out, box->x1, box->y1, scale, true );
	}
	else if ( poly = dynamic_cast<polygon_shape const*>( view.get() ) ) {
		write_polygon( out, *poly, scale, layer );
	}
	else {
		// convert to a polygon
		polygon p;
		view.get_polygon( p );
		write_polygon( out, p, scale, layer );
	}
}

static char const* look_up_layer_name( dxf::map_id_name const& id_to_name, layer_id layer )
{
	dxf::map_id_name::const_iterator lp = id_to_name.find( layer );
	if ( lp != id_to_name.end() ) {
		return lp->second.c_str();
	}
	else {
		return "UNKNOWN";
	}
}

void dxf::write( layout const& view, map_id_name const& id_to_name, double scale, std::ostream& out )
{
	assert( scale > 0 );

	// get the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes );

	// write the file header
	write_HEADER( out );

	// draw the shapes
	write_start_ENTITIES( out );
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		char const* layer_name = look_up_layer_name( id_to_name, lp->layer() );
		write_shape( out, *lp, scale, layer_name );
	}
	write_end_ENTITIES( out );

	// complete the file
	write_EOF( out );
}

void dxf::write( layout const& view, layer_id layer, map_id_name const& id_to_name, double scale, std::ostream& out )
{
	assert( scale > 0 );

	// get the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes, layer );
	// look-up the layer name
	char const* layer_name = look_up_layer_name( id_to_name, layer );

	// write the file header
	write_HEADER( out );

	// draw the shapes
	write_start_ENTITIES( out );
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		write_shape( out, *lp, scale, layer_name );
	}
	write_end_ENTITIES( out );

	// complete the file
	write_EOF( out );
}

