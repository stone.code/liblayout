/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include <assert.h>
#include <stdexcept>

using namespace liblayout;

layout::layout()
{
}

layout::layout( layout const & rhs )
: name(rhs.name), shapes(rhs.shapes), composites(rhs.composites)
{
}

rect layout::bounds() const
{
	rect r(0,0,0,0);
	
	// precondition, the layout is not empty
	assert( !empty() );

	// handle shapes
	if ( !shapes.empty() ) {
		std::list<shape>::const_iterator slp = shapes.begin();
		
		// initialize bounds with the first shape
		r = slp->bounds();
		assert( r.valid() );
		
		// update bounds with info from rest of the shapes
		for ( ++slp; slp!=shapes.end(); ++slp ) {
			r += slp->bounds();
		}
	}
	
	// handle composites
	bool r_initialized = !shapes.empty();

	std::list<composite>::const_iterator clp;
	for ( clp = composites.begin(); clp != composites.end(); ++clp )
	{
		// can't use this composite, if it has no bounds
		if ( !clp->has_bounds() ) continue;
		
		// has the bounds been initialized?
		if ( r_initialized ) {
			rect r2 = clp->bounds();
			assert( r2.valid() );
			r += r2;
		}
		else {
			r = clp->bounds();
			r_initialized = true;
		}
	}

	// make sure we have got something to return
	assert( r_initialized );
	// return bounds
	assert( r.valid() );
	return r;
}

void layout::clear()
{
	name = ""; //name_.clear();
	shapes.clear();
	composites.clear();
}

int layout::contains_point( point pt ) const
{
	throw std::logic_error( "Unimplemented function" );
}

void layout::insert( shape s )
{
	shapes.push_back( s );
}

void layout::insert( composite c )
{
	composites.push_back( c );
}

layout::composite_iterator layout::erase( composite_iterator it )
{
	return composites.erase( it );
}

layout::shape_iterator layout::erase( shape_iterator it )
{
	return shapes.erase( it );
}

void layout::flatten()
{
	// get all of the shapes from the composite object
	std::vector<shape> s;

	// make sure the composites have been written out
	layout::composite_iterator clp;
	for ( clp = composites.begin(); clp != composites.end(); ++clp )
	{
		clp->get_shapes( s );
	}

	// move shapes to shape list
	std::vector<shape>::iterator lp;
	for ( lp = s.begin(); lp != s.end(); ++lp )
	{
		shapes.push_back( *lp );
	}

	// remove composites
	composites.clear();
}

void layout::get_layers( std::set<layer_id>& lhs ) const
{
	std::list<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		lhs.insert( lp->layer() );
	}

	std::list<composite>::const_iterator clp;
	for ( clp = composites.begin(); clp != composites.end(); ++clp )
	{
		clp->get_layers( lhs );
	}
}

void layout::get_layers( std::vector<layer_id>& lhs ) const
{
	std::set<layer_id>	s;
	get_layers(s);

	std::set<layer_id>::const_iterator lp;
	for ( lp = s.begin(); lp != s.end(); ++lp )
	{
		lhs.push_back( *lp );
	}
}

void layout::get_shapes( std::vector<shape>& lhs ) const
{
	// copy all of our regular shapes
	std::copy( shapes.begin(), shapes.end(), std::back_inserter( lhs ) );

	// make sure the composites have been written out
	std::list<composite>::const_iterator clp;
	for ( clp = composites.begin(); clp != composites.end(); ++clp )
	{
		// get all of the shapes from the composite object
		std::vector<shape> s;
		clp->get_shapes( s );
		// add shapes to our list
		for ( size_t lp = 0; lp < s.size(); ++lp )
		{
			lhs.push_back( s[lp] );
		}
	}
}

void layout::get_shapes( std::vector<shape>& lhs, layer_id layer ) const
{
	// copy all of our regular shapes
	std::list<shape>::const_iterator slp;
	for ( slp = shapes.begin(); slp != shapes.end(); ++slp )
	{
		if ( slp->layer() == layer )
			lhs.push_back( *slp );
	}

	// make sure the composites have been written out
	std::list<composite>::const_iterator clp;
	for ( clp = composites.begin(); clp != composites.end(); ++clp )
	{
		// get all of the shapes from the composite object
		std::vector<shape> s;
		clp->get_shapes( s, layer );
		// add shapes to our list
		for ( size_t lp = 0; lp < s.size(); ++lp )
		{
			lhs.push_back( s[lp] );
		}
	}
}

bool layout::has_bounds() const
{
	// do we have some shapes
	if ( !shapes.empty() ) return true;
	
	// check composites
	std::list<composite>::const_iterator clp;
	for ( clp = composites.begin(); clp != composites.end(); ++clp )
	{
		if ( clp->has_bounds() ) return true;
	}
	
	// found nothing with bounds
	return false;		
}

void layout::shapes_sort()
{
	shapes.sort();
}
