/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include "layout_shapes.h"
#include <algorithm>
#include <assert.h>
#include <ctype.h>
#include <exception>
#include <fstream>
#include <stack>
#include <map>

using namespace liblayout;
using namespace std;

static void write_byte_hex( std::ostream& out, short value )
{
	char digit[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	out << ((value >> 8) & 0xFF);
	out << (value & 0xFF);
}

static void write_css( layout const& view, svg::map_id_style const& layer_map, std::ostream& out )
{
	// get all the necessary layers
	std::set<layer_id>	layers;
	view.get_layers( layers );

	out << "\t<style type=\"text/css\"><![CDATA[" << std::endl;

	// iterate over all the layers
	std::set<layer_id>::const_iterator lp;
	for ( lp = layers.begin(); lp != layers.end(); ++lp )
	{
		// Look up style information for this layer
		svg::map_id_style::const_iterator style_ = layer_map.find( *lp );
		if ( style_ == layer_map.end() ) {
			// Information not present.  Use default layer information
			style_ = layer_map.find( 0 );
		}
		assert( style_ != layer_map.end() );
		style const& pen = style_->second;
	
		out << "\tg.layer" << *lp << " {" << std::endl;
		if( pen.has_fill() ) {
			short red = pen.fill.red();
			short green = pen.fill.green();
			short blue = pen.fill.blue();
			short alpha = pen.fill.alpha();
			assert( alpha<0xFF );
			
			// fill colour
			out << "\t\tfill: ";
			write_byte_hex( out, red );
			write_byte_hex( out, green );
			write_byte_hex( out, blue );
			out << ";\n";
			
			// fill transparency
			if ( alpha>0 ) 
				out << "\t\tfill-opacity: " << (1.0-alpha/255.0) << ";\n";
		}
		if ( pen.has_stroke() ) {
			short red = pen.stroke.red();
			short green = pen.stroke.green();
			short blue = pen.stroke.blue();
			short alpha = pen.stroke.alpha();
			assert( alpha<0xFF );
			
			// stroke colour
			out << "\t\tstroke: ";
			write_byte_hex( out, red );
			write_byte_hex( out, green );
			write_byte_hex( out, blue );
			out << ";\n";
			
			// stroke transparency
			if ( alpha>0 ) 
				out << "\t\tstroke-opacity: " << (1.0-alpha/255.0) << ";\n";
				
			// stroke width
			if ( pen.stroke_width>=0 )
				out << "\t\tstroke-width: " << pen.stroke_width << ";\n";
		}
		out << "\t}" << std::endl;
	}

	out << "]]></style>" << std::endl;
}

static void write_shape( shape const& view, rect const& b, std::ostream& out )
{
	// write out shape
	const rect_shape* box;
	const polygon_shape* poly;

	assert( view.get() );

	if ( box = dynamic_cast<rect_shape const*>( view.get() ) )
	{
		// remember we are flipping the y-direction
		out << "    <rect x=\"" << (box->x1 - b.x1) << "\" y=\"" << (b.y2-box->y2) << "\" width=\"" << box->width() << "\" height=\"" << box->height() << "\" />" << std::endl;
	}
	else if ( poly = dynamic_cast<polygon_shape const*>( view.get() ) )
	{
		out << "    <polygon points=\"";
		polygon_shape::const_iterator lp;
		for ( lp = poly->begin(); lp != poly->end(); ++lp )
		{
			out << ' ' << (lp->x - b.x1) << ',' << (b.y2 - lp->y);
		}
		out << "\" />" << std::endl;
	}
	else
	{
		// convert to a polygon
		polygon p;
		view.get_polygon( p );
		out << "    <polygon points=\"";
		polygon_shape::const_iterator lp;
		for ( lp = p.begin(); lp != p.end(); ++lp )
		{
			out << ' ' << (lp->x - b.x1) << ',' << (b.y2 - lp->y);
		}
		out << "\" />" << std::endl;
	}
}

namespace liblayout
{
namespace svg
{
	class pred
	{
	public:
		pred( map_id_style& op ) : map(op) {};

		bool	operator()( shape const& lhs, shape const& rhs ) const 
		{ 
			if (map[lhs.layer()].order < map[rhs.layer()].order ) return true;
			if (map[lhs.layer()].order > map[rhs.layer()].order ) return false; 
			return lhs.layer() < rhs.layer();
		};

	private:
		svg::map_id_style& map;
	};
}
}

void svg::write( layout const& view, svg::map_id_style const& layer_map, double mag, std::ostream& out )
{
	layer_id			layer;
	std::vector<shape>	shapes;

	assert( mag >= 0 );

	// initialize the variables
	rect bounds = view.bounds();
	assert( bounds.valid() );
	view.get_shapes( shapes );

	// print out header
	out << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
	out << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 20010904//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">" << std::endl;
	out << "<svg width=\"";
	if (mag>0)
		out << bounds.width()*mag*100 << "cm";
	else
		out << "100%";
	out << "\" height=\"";
	if (mag>0)
		out << bounds.height()*mag*100 << "cm";
	else
		out << "100%";
	out << "\" viewBox=\"0 0 " << bounds.width() << ' ' << bounds.height() << "\">" << std::endl;
	out << "  <defs>" << std::endl;
	write_css( view, layer_map, out );
	out << "  </defs>" << std::endl;

	// write out the shapes
	std::sort( shapes.begin(), shapes.end() );
	layer = shapes.front().layer();
	out << "  <g class=\"layer" << layer << "\" >" << std::endl;
	
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		if ( lp->layer() != layer )
		{
			layer = lp->layer();
			out << "  </g>" << std::endl;
			out << "  <g class=\"layer" << layer << "\" >" << std::endl;
		}

		write_shape( *lp, bounds, out );
	}

	out << "  </g>" << std::endl;
	out << "</svg>";
}

void svg::write( layout const& view, layer_id layer, svg::map_id_style const& layer_map, double scale, std::ostream& out )
{
	std::vector<shape>	shapes;

	assert( scale >= 0 );

	// initialize the variables
	rect bounds = view.bounds();
	assert( bounds.valid() );
	view.get_shapes( shapes );

	// print out header
	out << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
	out << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 20010904//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">" << std::endl;
	out << "<svg width=\"";
	if (scale>0)
		out << bounds.width()*scale*100 << "cm";
	else
		out << "100%";
	out << "\" height=\"";
	if (scale>0)
		out << bounds.height()*scale*100 << "cm";
	else
		out << "100%";
	out << "\" viewBox=\"0 0 " << bounds.width() << ' ' << bounds.height() << "\">" << std::endl;
	out << "  <defs>" << std::endl;
	write_css( view, layer_map, out );
	out << "  </defs>" << std::endl;

	// write out the shapes
	out << "  <g class=\"layer" << layer << "\" >" << std::endl;
	
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		if ( lp->layer() == layer )
			write_shape( *lp, bounds, out );
	}

	out << "  </g>" << std::endl;
	out << "</svg>";
}

void svg::write( shape const& view, double scale, std::ostream& out )
{
	assert( scale >= 0 );

	// initialize the variables
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// print out header
	out << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
	out << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 20010904//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">" << std::endl;
	out << "<svg width=\"";
	if (scale>0)
		out << bounds.width()*scale*100 << "cm";
	else
		out << "100%";
	out << "\" height=\"";
	if (scale>0)
		out << bounds.height()*scale*100 << "cm";
	else
		out << "100%";
	out << "\" viewBox=\"0 0 " << bounds.width() << ' ' << bounds.height() << "\">" << std::endl;

	// write out the shapes
	out << "  <g fill=\"#c0c0c0\" stroke=\"black\" >" << std::endl;
	write_shape( view, bounds, out );
	out << "  </g>" << std::endl;
	out << "</svg>";
}

void svg::write( polygon const& view, double mag, std::ostream& out )
{
	assert( mag >= 0 );

	// initialize the variables
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// print out header
	out << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
	out << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 20010904//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">" << std::endl;
	out << "<svg width=\"";
	if (mag>0)
		out << bounds.width()*mag*100 << "cm";
	else
		out << "100%";
	out << "\" height=\"";
	if (mag>0)
		out << bounds.height()*mag*100 << "cm";
	else
		out << "100%";
	out << "\" viewBox=\"0 0 " << bounds.width() << ' ' << bounds.height() << "\">" << std::endl;

	// write out the shapes
	out << "  <g fill=\"#c0c0c0\" stroke=\"black\" >" << std::endl;
	out << "    <polygon points=\"";
	polygon_shape::const_iterator lp;
	for ( lp = view.begin(); lp != view.end(); ++lp )
	{
		out << ' ' << (lp->x - bounds.x1) << ',' << (bounds.y2 - lp->y);
	}
	out << "\" />" << std::endl;
	out << "  </g>" << std::endl;
	out << "</svg>";
}


