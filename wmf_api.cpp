/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "wmf.h"
#include <assert.h>
#include <string.h>

#ifdef HAVE_STDINT_H
#include <stdint.h>
#elif HAVE_INTTYPES_H
#include <inttypes.h>
#else
// this should really be replaced with something more portable
typedef unsigned long uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char uint8_t;
#endif

using namespace liblayout;


//typedef DWORD COLORREF;

#define ENMT_HEADER                1
#define ENMT_EOF                  14
#define ENMT_POLYLINETO            6
#define ENMT_MOVETO               27
#define ENMT_POLYBEZIERTO          5
#define ENMT_BEGINPATH            59
#define ENMT_ENDPATH              60
#define	EMR_RECTANGLE			  43 
#define	EMR_POLYGON				   3 
#define ENMT_FILLPATH             62
#define ENMT_CREATEPEN            38
#define ENMT_CREATEBRUSHINDIRECT  39
#define ENMT_SELECTOBJECT         37
#define ENMT_SETWORLDTRANSFORM    35
#define ENMT_SETPOLYFILLMODE      19
#define ENMT_STROKEPATH           64
#define ENMT_LINETO               54
#define ENMT_POLYBEZIERTO16       88

#define WDEVPIXEL 1280
#define HDEVPIXEL 1024
#define WDEVMLMTR 320
#define HDEVMLMTR 240

wmf::writer::writer()
{
	records = 0;
	handles = 0;
	width = 0;
	height = 0;
}

/* endianess independent IO functions */

static void write32(std::ostream& out, uint32_t data)
{
	assert( sizeof(data) == 4 );

	out.put( data & 0xFF );
	out.put( (data >> 8) & 0x0FF );
	out.put( (data >> 16) & 0xFF );
	out.put( (data >> 24) & 0xFF );
}

static void write16(std::ostream& out, uint16_t data)
{
	assert( sizeof(data) == 2 );

	out.put( data & 0xFF );
	out.put( (data >> 8) & 0x0FF );
}

static void write32(std::vector<unsigned char>& out, uint32_t data)
{
	assert( sizeof(data) == 4 );

	out.push_back( data & 0xFF );
	out.push_back( (data >> 8) & 0x0FF );
	out.push_back( (data >> 16) & 0xFF );
	out.push_back( (data >> 24) & 0xFF );
}

static void write16(std::vector<unsigned char>& out, uint16_t data)
{
	assert( sizeof(data) == 2 );

	out.push_back( data & 0xFF );
	out.push_back( (data >> 8) & 0x0FF );
}

static int WriteBeginPath(std::vector<unsigned char>& out)
{
	int recsize = sizeof(uint32_t) * 2;

	write32( out, ENMT_BEGINPATH );
	write32( out, recsize);

	return recsize;
}

static int WriteEndPath(std::vector<unsigned char>& out)
{
	int recsize = sizeof(uint32_t) * 2;

	write32(out, ENMT_ENDPATH);
	write32(out, recsize);

	return recsize;
}

static int WriteFillPath(std::vector<unsigned char>& out)
{
	int recsize = sizeof(uint32_t) * 6;

	write32(out, ENMT_FILLPATH);
	write32(out, recsize);
	write32(out, 0x0);
	write32(out, 0x0);
	write32(out, 0xFFFFFFFF);
	write32(out, 0xFFFFFFFF);

	return recsize;
}

static int WriteStrokePath(std::vector<unsigned char>& out)
{
	int recsize = sizeof(uint32_t) * 6;

	write32(out, ENMT_STROKEPATH);
	write32(out, recsize);
	write32(out, 0x0);
	write32(out, 0x0);
	write32(out, 0xFFFFFFFF);
	write32(out, 0xFFFFFFFF);

	return recsize;
}

static int WriteMoveTo(std::vector<unsigned char>& out, long x, long y )
{
	int recsize = sizeof(uint32_t) * 4;

	write32(out, ENMT_MOVETO);
	write32(out, recsize);
	write32(out, x );
	write32(out, y );

	return recsize;
}

static int WriteLineTo(std::vector<unsigned char>& out, long x, long y )
{
	int recsize = sizeof(uint32_t) * 4;

	write32(out, ENMT_LINETO);
	write32(out, recsize);
	write32(out, x );
	write32(out, y );

	return recsize;
}

void	wmf::writer::create_rect( coord_t x1, coord_t y1, coord_t x2, coord_t y2 )
{
	if ( x1 > width ) width = x1;
	if ( x2 > width ) width = x2;
	if ( y1 > height ) height = y1;
	if ( y2 > height ) height = y2;

	//WriteBeginPath( data );
	//WriteMoveTo( data, x1, y1 );
	//WriteLineTo( data, x1, y2 );
	//WriteLineTo( data, x2, y2 );
	//WriteLineTo( data, x2, y1 );
	//WriteLineTo( data, x1, y1 );
	//WriteEndPath( data );
	//WriteFillPath( data );
	//WriteStrokePath( data );
	// records += 9;

	int recsize = sizeof(uint32_t) * 6;

	write32(data, EMR_RECTANGLE);
	write32(data, recsize);
	write32(data, x1 );
	write32(data, y1 );
	write32(data, x2 );
	write32(data, y2 );
	records++;
}

void	wmf::writer::create_polygon( std::vector<point> const& p )
{
	int recsize = sizeof(uint32_t) * (2 + 4 + 1 + p.size()*2 );

	// get the bounds for the polygon
	coord_t left = p.front().x;
	coord_t right = p.front().x;
	coord_t top = p.front().y;
	coord_t bottom = p.front().y;
	std::vector<point>::const_iterator lp;
	for ( lp = p.begin(); lp != p.end(); ++lp )
	{
		if ( lp->x < left ) left = lp->x;
		if ( lp->x > right ) right = lp->x;
		if ( lp->y < top ) top = lp->y;
		if ( lp->y > bottom ) bottom = lp->y;
	}

	// write out the record header
	write32( data, EMR_POLYGON);
	write32( data, recsize);
	write32( data, left );
	write32( data, top );
	write32( data, right );
	write32( data, bottom );
	write32( data, p.size() );

	// write out the points
	for ( lp = p.begin(); lp != p.end(); ++lp )
	{
		if ( lp->x > width ) width = lp->x;
		if ( lp->y > height ) height = lp->y;
		write32( data, lp->x );
		write32( data, lp->y );
	}

	++records;
}

wmf::handle_t	wmf::writer::create_pen( color_t color )
{
	assert( sizeof( uint32_t ) == 4 );
	int recsize = sizeof(uint32_t) * 7;
  
	write32(data, ENMT_CREATEPEN);
	write32(data, recsize);
	write32(data, ++handles);
	write32(data, 0x0);  /* solid pen style */
	write32(data, 0x0);  /* 1 pixel ...  */
	write32(data, 0x0);  /* ... pen size */
	write32(data, color);
	++records;

	return handles;
}

wmf::handle_t wmf::writer::create_brush( color_t color )
{
	assert( sizeof( uint32_t ) == 4 );
	int recsize = sizeof(uint32_t) * 6;
  
	write32(data, ENMT_CREATEBRUSHINDIRECT);
	write32(data, recsize);
	write32(data, ++handles );
	write32(data, 0x0);  /* solid brush style */
	write32(data, color ); 
	write32(data, 0x0);  /* ignored when solid */
	++records;

	return handles;
}

wmf::handle_t wmf::writer::create_null_brush( )
{
	assert( sizeof( uint32_t ) == 4 );
	int recsize = sizeof(uint32_t) * 6;
  
	write32(data, ENMT_CREATEBRUSHINDIRECT);
	write32(data, recsize);
	write32(data, ++handles );
	write32(data, 1);  /* solid brush style */
	write32(data, 0 ); 
	write32(data, 0x0);  /* ignored when solid */
	++records;

	return handles;
}

void	wmf::writer::select_pen( handle_t index )
{
	int recsize = sizeof(uint32_t) * 3;
  
	write32(data, ENMT_SELECTOBJECT);
	write32(data, recsize);
	write32(data, index );
	++records;
}

void	wmf::writer::select_brush( handle_t index )
{
	assert( sizeof( uint32_t ) == 4 );
	int recsize = sizeof(uint32_t) * 3;
  
	write32(data, ENMT_SELECTOBJECT);
	write32(data, recsize);
	write32(data, index );
	++records;
}

static int WriteFooter( std::ostream* fdes )
{
	assert( sizeof( uint32_t ) == 4 );
	int recsize = sizeof(uint32_t) * 5;
  
	if(fdes != NULL)
	{
	    write32(*fdes, ENMT_EOF);
	    write32(*fdes, recsize);
	    write32(*fdes, 0);
	    write32(*fdes, recsize - sizeof(uint32_t));
	    write32(*fdes, recsize);
	}
  
	return recsize;
}

static int WriteHeader(std::ostream* fdes, char const* name, uint32_t width, uint32_t height, int fsize, int nrec, int nhand)
{
	int i, recsize;
	size_t desclen;
	const char * editor = "liblayout";

	desclen = (strlen(editor) + strlen(name) + 3);
	assert( sizeof( uint32_t ) == 4 );
	recsize = sizeof(uint32_t) * 25 + (desclen * 2) + ((desclen * 2) % 4);

	if( fdes != NULL)
	{
	    write32(*fdes, ENMT_HEADER);
	    write32(*fdes, recsize);
		/* pixel bounds */
		write32(*fdes, 0);			/* Left inclusive bounds */
		write32(*fdes, 0);			/* Right inclusive bounds */
		write32(*fdes, width+1);	/* Top inclusive bounds */
		write32(*fdes, height+1);	/* Bottom inclusive bounds */
		/* millimeter bounds */
		write32(*fdes, 0);
		write32(*fdes, 0);
		write32(*fdes, width * WDEVMLMTR * 100 / WDEVPIXEL);
		write32(*fdes, height * HDEVMLMTR * 100 / HDEVPIXEL);
		/* signature " EMF" */
		write32(*fdes, 0x464D4520);
		/* current version */
		write32(*fdes, 0x00010000);
		/* file size */
		write32(*fdes, fsize);
		/* number of records */
		write32(*fdes, nrec);
		/* number of handles */
		write16(*fdes, nhand);
		/* reserved */
		write16(*fdes, 0);
		/* size of description */
		write32(*fdes, desclen);
		/* description offset */
		write32(*fdes, 100);
		/* palette entries */
		write32(*fdes, 0);
		/* device width & height in pixel & millimeters */
		write32(*fdes, WDEVPIXEL);
		write32(*fdes, HDEVPIXEL);
		write32(*fdes, WDEVMLMTR);
		write32(*fdes, HDEVMLMTR);
		/* pixel format & opengl (not used) */
		write32(*fdes, 0);
		write32(*fdes, 0);
		write32(*fdes, 0);
		/* description string in Unicode */
		for(i=0;editor[i]; i++)
		{
			write16(*fdes, editor[i] & 0x07F );
		}
		write16(*fdes, 0);
		for(i=0;name[i]; i++)
		{
			write16(*fdes, name[i] & 0x07F );
		}
		write32(*fdes, 0);
		if((desclen * 2) % 4)
			write16(*fdes, 0);
	}

	return recsize;
}


void wmf::writer::write( std::ostream& out ) const
{
	size_t file_size = data.size();

	// update the file size
	file_size += WriteHeader(0, "layout", width, height, 0, 0, 0 );
	file_size += WriteFooter(0);

	// write the file
	WriteHeader( &out, "layout", width, height, file_size, records+2, handles );
	std::vector<unsigned char>::const_iterator lp;
	for ( lp = data.begin(); lp != data.end(); ++lp )
		out.put( *lp );
	WriteFooter( &out );
}

