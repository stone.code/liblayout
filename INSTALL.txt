The application can be built and installed
using the standard commands, as follows:

    ./configure
    make
    make install

Thank-you for using liblayout.

