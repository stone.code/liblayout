/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include "layout_shapes.h"
#include "wmf.h"
#include <algorithm>
#include <assert.h>
#include <ctype.h>
#include <iostream>
#include <stdexcept>

using namespace liblayout;

static void export_shape( shape const& view, rect const& b, wmf::writer& file)
{
	// write out shape
	const rect_shape* box;

	if ( box = dynamic_cast<rect_shape const*>( view.get() ) )
	{
		assert( box->valid() );
		file.create_rect( box->x1 - b.x1, b.y2 - box->y2, box->x2 - b.x1, b.y2 - box->y1 );
	}
	else
	{
		// convert to a polygon
		polygon p;
		view.get_polygon( p );
		polygon::iterator lp;
		for ( lp = p.begin(); lp != p.end(); ++lp )
		{
			lp->x = lp->x - b.x1;
			lp->y = b.y2 - lp->y;
		}
		file.create_polygon( p );
	}
}

static void set_drawing( wmf::writer& file, wmf::map_id_style const& layer_map, layer_id layer )
{
	// first, can we find the layer in the css
	wmf::map_id_style::const_iterator lp = layer_map.find(layer);
	if ( lp == layer_map.end() )
	{
		wmf::handle_t pen = file.create_pen( 0 );
		wmf::handle_t brush = file.create_null_brush();
		file.select_pen( pen );
		file.select_brush( brush );
	}
	else
	{
		// create the pen
		if ( lp->second.has_stroke() )
			file.select_pen( file.create_pen( lp->second.stroke.get() ) );
		else
			file.select_pen( file.create_pen( 0 ) );

		if ( lp->second.has_fill() )
			file.select_brush( file.create_brush( lp->second.fill.get() ) );
		else
			file.select_brush( file.create_null_brush() );
	}
}

void wmf::write( layout const& view, map_id_style const& layer_map, double scale, std::ostream& out )
{
	assert( scale >= 0 );
	writer		file;

	// get size
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// get the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes );

	// write out the shapes
	std::sort( shapes.begin(), shapes.end() );

	layer_id layer = shapes.front().layer();
	set_drawing( file, layer_map, layer );

	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		if ( lp->layer() != layer )
		{
			layer = lp->layer();
			set_drawing( file, layer_map, layer );
		}

		export_shape( *lp, bounds, file );
	}

	file.write( out );
}

void wmf::write( layout const& view, layer_id layer, map_id_style const& layer_map, double scale, std::ostream& out )
{
	assert( scale >= 0 );
	writer		file;

	// get size
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// get the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes );

	// write out the shapes
	set_drawing( file, layer_map, layer );

	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		if ( lp->layer() == layer )
			export_shape( *lp, bounds, file );
	}

	file.write( out );
}
