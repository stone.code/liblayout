/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "logging.h"
#include <assert.h>
#include <iostream>

static logging::stdout_logging default_implementation;
static logging::logging_imp* implementation = &default_implementation;

void logging::set_logging( logging_imp* imp )
{
	if ( !imp ) imp = &default_implementation;
	implementation = imp;
	assert( implementation );
}

void logging::set_logging_stdout()
{
	implementation = &default_implementation;
}

void logging::set_logging_stderr()
{
	static stderr_logging imp;
	implementation = &imp;
}

logging::logging_imp* logging::get_logging()
{
	assert( implementation );
	return implementation;
}

logging::cstr logging::get_logging_name()
{
	assert( implementation );
	return implementation->name();
}

void logging::print( logging::severity s, char const* message )
{
	assert( implementation );
	implementation->write( s, message );
}

void logging::print( logging::severity s, std::string const& message )
{
	assert( implementation );
	implementation->write( s, message.c_str() );
}

/************************************************************************
** stdout_logging
*/

void logging::stdout_logging::write( severity s, char const* message )
{
	assert( message );
	assert( *message );

	switch (s)
	{
	case INFORMATION:
		std::cout << "INFO:  ";
		break;
	case WARNING:
		std::cout << "WARN:  ";
		break;
	case ERROR:
		std::cout << "ERROR:  ";
		break;
	}

	std::cout << message << std::endl;
}

logging::cstr logging::stdout_logging::name() const
{
	return "stdout";
}

/************************************************************************
** stderr_logging
*/

void logging::stderr_logging::write( severity s, char const* message )
{
	assert( message );
	assert( *message );

	switch (s)
	{
	case INFORMATION:
		std::cerr << "INFO:  ";
		break;
	case WARNING:
		std::cerr << "WARN:  ";
		break;
	case ERROR:
		std::cerr << "ERROR:  ";
		break;
	}

	std::cerr << message << std::endl;
}

logging::cstr logging::stderr_logging::name() const
{
	return "stderr";
}
