/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2010 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include <fstream>
#include <stdexcept>

using namespace liblayout;

void text::write( layout const& view, layer_id layer, map_id_name const& id_to_name, char const* filename )
{
	std::ofstream file;

	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	text::write( view, layer, id_to_name, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the text file." );
	}
	file.close();
}

void text::write( layout const& view, map_id_name const& id_to_name, char const* filename )
{
	std::ofstream file;

	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	text::write( view, id_to_name, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the text file." );
	}
	file.close();
}
