/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2007-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include <assert.h>
#include <iostream>

using liblayout::style;

std::ostream& liblayout::operator<<(std::ostream& out, liblayout::style const& info )
{
	assert( info.stroke_width >= 0 );

	if ( info.has_fill() ) {
		out << "Fill (" << info.fill.red() << ',' << 
		info.fill.green() << ',' <<
		info.fill.blue();
		if ( info.fill.alpha() ) out << ",alpha=" << info.fill.alpha();
		if ( info.fill_pattern!=style::fill_solid) out << ",pattern=" << info.fill_pattern;
		out << "), ";
	}
	else {
		out << "no fill,";
	}
	if ( info.has_stroke() ) {
		out << "and Stroke (" << info.stroke.red() << ',' << 
		info.stroke.green() << ',' <<
		info.stroke.blue();
		out << ",width=" << info.stroke_width;
		if ( info.stroke.alpha() ) out << ",alpha=" << info.stroke.alpha();
		if ( info.stroke_pattern!=style::stroke_solid) out << ",pattern=" << info.stroke_pattern;
		out << ")";
	}
	else {
		out << "and no stroke.";
	}
	
	return out;
}
