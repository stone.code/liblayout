/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2007 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include "layout_shapes.h"
#include "gdsii.h"
#include <assert.h>
#include <math.h>
#include <sstream>
#include <stdexcept>
#include <vector>

using namespace liblayout;

/************************************************************************
  Support routines
************************************************************************/

static void throw_unknown_command( std::istream& in, long type, long line )
{
	std::stringstream message;
	message << "Unknown command (" << type << ") found at position " << in.tellg() << " (source line " << __LINE__ << ") during GDSII import." << '\0';
	throw std::runtime_error( message.str() );
}

static void throw_read_error( std::istream& in )
{
	std::stringstream message;
	message << "Read failure at position " << in.tellg() << " during GDSII import." << '\0';
	throw std::runtime_error( message.str() );
}

static void throw_polygon_error( std::istream& in )
{
	std::stringstream message;
	message << "Polygon with fewer than three distinct points found at position " << in.tellg() << " during GDSII import." << '\0';
	throw std::runtime_error( message.str() );
}

static void throw_path_error( std::istream& in )
{
	std::stringstream message;
	message << "Path with fewer than two distinct points found at position " << in.tellg() << " during GDSII import." << '\0';
	throw std::runtime_error( message.str() );
}

static void throw_endel_error( std::istream& in )
{
	std::stringstream message;
	message << "End of element record expected at position " << in.tellg() << " during GDSII import." << '\0';
	throw std::runtime_error( message.str() );
}

static void throw_unexpected( std::istream& in, char const* record )
{
	std::stringstream message;
	message << "Expected of type " << record << " at position " << in.tellg() << " during GDSII import." << '\0';
	throw std::runtime_error( message.str() );
}

static void throw_bad_polygon( std::istream& in )
{
	std::stringstream message;
	message << "Corrupted bondary record found at position " << in.tellg() << " during GDSII import.  Are there too many points in your polygon?" << '\0';
	throw std::runtime_error( message.str() ); 
}

static void log_version( short version, gdsii::on_comment_handler on_comment, void* on_comment_userdata )
{
	assert( on_comment );
	
	// Get a string representing the version information
	char const* str = 0;
	switch ( version ) {
		case 0:
			str = "<v3.0"; break;
		case 3:
			str = "v3.0"; break;
		case 4:
			str = "v4.0"; break;
		case 5:
			str = "v5.0"; break;
		case 600:
			str = "v6.0"; break;
	}
	
	// Send the comment
	if ( str ) {
		std::stringstream message;
		message << "GDSII file is marked version " << version << '.';
		(*on_comment)( on_comment_userdata, message.str().c_str() );
	}
	else {
		(*on_comment)( on_comment_userdata, "GDSII file is marked with an unknown version." );
	}
}

static void convert_layout_name_composites( layout* l, std::list<layout>& layouts )
{
	// check out all the composites
	layout::composite_iterator clp;
	for ( clp = l->composites_begin(); clp != l->composites_end(); ++clp )
	{
		layout_name_composite const* oldptr;

		if ( oldptr = dynamic_cast<layout_name_composite const*>( clp->get() ) )
		{
			*clp = oldptr->dereference( layouts );
			assert( clp->get() );
			assert( dynamic_cast<layout_composite const*>( clp->get() ) );
		}
	}
}

static layout* get_top_layout( std::vector<layout*> const& layouts,
	gdsii::on_comment_handler on_comment, void* on_comment_userdata )
{
	// to avoid problems with MSVC6
	// hoise lp out of the loops
	std::vector<layout*>::const_iterator lp;

	// initialize a set with all of the layouts
	std::set<layout const*> top_level;
	for ( lp = layouts.begin(); lp != layouts.end(); ++lp )
	{
		top_level.insert( *lp );
	}
	
	// remove layouts that are are sub-layouts
	for ( lp = layouts.begin(); lp != layouts.end(); ++lp )
	{
		layout::composite_iterator c;
		for ( c = (*lp)->composites_begin(); c != (*lp)->composites_end(); ++c )
		{
			layout_composite const* ref = dynamic_cast<layout_composite const*>( c->get() );
			if ( ref ) top_level.erase( ref->get_layout() );
		}
	}
	
	// check count, and provide a warning if more than one top-level
	// layout was found.
	assert( top_level.size() > 0 );
	if ( on_comment && top_level.size() > 1 ) {
		(*on_comment)( on_comment_userdata, 
			"Multiple top-level layouts were found in the GDSII file." );
	}
	
	// and return the first
	return const_cast<layout*>( *top_level.begin() );
}

/************************************************************************
  These routines are used to read/write integral types
************************************************************************/

static short read_short( std::istream& in )
{
	assert( sizeof(short) >= 2 );

	unsigned char buffer[2];
	buffer[0] = in.get();
	buffer[1] = in.get();
	if ( !in ) throw_read_error( in );

	unsigned int display_integer = buffer[0];
	display_integer <<= 8;
	display_integer += buffer[1];

	// check for sign flag
	if (display_integer & 0x8000)	
	{
		display_integer &= 0x7fff;
		display_integer ^= 0x7fff;
		display_integer += 1;
		display_integer *= -1;
	}

	return display_integer;
}

static short read_type( std::istream& in )
{
	short t = read_short( in );
	if ( t < 0 ) {
		std::stringstream message;
		message << "Invalid record type (" << t << ") found at position " << in.tellg() << " during GDSII import." << '\0';
		throw std::runtime_error( message.str() );
	}
	return t;
}

static long read_long( std::istream& in )
{
	assert( sizeof(long) >= 4 );

	unsigned char buffer[4];
	buffer[0] = in.get();
	buffer[1] = in.get();
	buffer[2] = in.get();
	buffer[3] = in.get();
	if ( !in ) throw_read_error( in );

	unsigned long display_integer = buffer[0];
	display_integer <<= 8;
	display_integer += buffer[1];
	display_integer <<= 8;
	display_integer += buffer[2];
	display_integer <<= 8;
	display_integer += buffer[3];

	// check for sign flag
	if (display_integer & 0x80000000)	
	{
		display_integer &= 0x7fffffff;
		display_integer ^= 0x7fffffff;
		display_integer += 1;
		display_integer *= -1;
	}

	return display_integer;
}

static double read_double( std::istream& in )
{
	assert( sizeof(double)>=8 );

	unsigned char buffer[8];
	buffer[0] = in.get();
	buffer[1] = in.get();
	buffer[2] = in.get();
	buffer[3] = in.get();
	buffer[4] = in.get();
	buffer[5] = in.get();
	buffer[6] = in.get();
	buffer[7] = in.get();
	if ( !in ) throw_read_error( in );

	int real_sign = buffer[0] & 0x80;
	int real_exponent = (buffer[0] & 0x7f) - 64;
	double real_mantissa = 0;
	for (int lp = 1; lp < 8; ++lp) {
		real_mantissa *= 0x100;
 		real_mantissa += buffer[lp];
 	}

	real_mantissa /= pow( (double)2, 7*8 );
	real_mantissa *= pow( (double)16, real_exponent );
	if (real_sign) {
		real_mantissa *= -1;
	}

	return real_mantissa;
}

/************************************************************************
  These routines are used to read GDSII records
************************************************************************/

static void	skip_record( std::istream& in, short length )
{
	for ( int lp=4; lp<length; ++lp ) in.get();
	if ( !in ) throw_read_error( in );
}

static void read_record( std::istream& in, std::vector<unsigned char>& buffer )
{
	assert( sizeof(short) == 2 );
	short length;
	in.read( reinterpret_cast<char*>( &length ), sizeof(length) );
	buffer.resize( length );
}

static void import_libname_record( std::istream& in, short length, std::string& name )
{
	// clear the name
	name = "";
	// append the characters
	for ( int lp=4; lp<length; ++lp )
		name.append( 1, static_cast<std::string::value_type>( in.get() ) );
}

static void import_sname_record( std::istream& in, short length, std::string& name )
{
	// clear the name
	name = "";
	// append the characters
	for ( int lp=4; lp<length; ++lp )
		name.append( 1, static_cast<std::string::value_type>( in.get() ) );
}

static void import_units_record( std::istream& in, short length, double& scaling_a, double& scaling_b )
{
	if ( length != 20 ) {
		std::stringstream message;
		message << "Units record has invalid length (" << length << ") found at position " << in.tellg() << " during GDSII import." << '\0';
		throw std::runtime_error( message.str() );
	}
	assert( sizeof(double) == 8 );
	scaling_a = read_double( in );
	scaling_b = read_double( in );
}

static void import_strname_record( std::istream& in, short length, std::string& name )
{
	// clear the name
	name = "";
	// append the characters
	for ( int lp=4; lp<length; ++lp )
		name.append( 1, static_cast<std::string::value_type>( in.get() ) );
}

static void import_boundary_record( std::istream& in, short length, layout& view )
{
	layer_id layer;
	short datatype = 0;

	// skip rest of record
	skip_record( in, length );

	length = read_short( in );
	short type = read_type( in );

	while ( type != XY )
	{
		switch ( type )
		{
		case ELFLAGS:
			skip_record( in, length );
			break;

		case PLEX:
			skip_record( in, length );
			break;

		case LAYER:
			assert( length == 6 );
			layer = read_short( in );
			break;

		case DATATYPE:
			assert( length == 6 );
			datatype = read_short( in );
			break;

		default:
			throw_unknown_command( in, type, __LINE__ );
			break;
		}

		length = read_short( in );
		type = read_type( in );
	};

	// interpret the XY
	std::vector<point> points;
	if ( length <= 0 ) throw_bad_polygon( in );
	assert( (length%2) == 0 );
	assert( ((length - 4) % 8) == 0 );
	int count = (length - 4) / 8;
	assert( count>0 );
	
	while ( count-- )
	{
		long x = read_long( in );
		long y = read_long( in );
		points.push_back( point( x, y ) );
		
		// some GDSII files have points doubled up
		if ( points.size()>1 && points[ points.size()-2] == points[ points.size()-1 ] )
		{
			points.pop_back();
		}
	}

	// check that last point isn't same as first point
	if ( points.front() == points.back() )
	{
		// remove last point
		points.pop_back();
	}
	
	// add the polygon to the view if there are enough points
	if ( points.size() < 3 ) throw_polygon_error( in );

	// create the polygon
	assert( points.size() >= 3 );
	view.insert( layer, points );

	// read in the end of element
	length = read_short( in );
	type = read_short( in );
	assert( in.good() );
	if ( type != ENDEL ) throw_endel_error( in );
	skip_record( in, length );
}

static void interpret_path( std::istream& in, short length, layout& view )
{
	layer_id layer;
	long width = 0;
	path_end_t pathtype = path_end_flush;
	short datatype = 0;

	// skip rest of record
	skip_record( in, length );

	length = read_short( in );
	short type = read_short( in );

	while ( type != XY )
	{
		switch ( type )
		{
		case ELFLAGS:
			skip_record( in, length );
			break;

		case PLEX:
			skip_record( in, length );
			break;

		case LAYER:
			assert( length == 6 );
			layer = read_short( in );
			break;

		case DATATYPE:
			assert( length == 6 );
			datatype = read_short( in );
			break;

		case PATHTYPE:
			assert( length == 6 );
			switch ( read_short(in) )
			{
			case 0: pathtype = path_end_flush; break;
			case 1: pathtype = path_end_round; break;
			case 2: pathtype = path_end_square; break;
			default:
				// logging::print(logging::WARNING, "Unknown path end type encountered." );
				pathtype = path_end_flush;
				break;
			};
			break;

		case WIDTH:
			// Read in the path width
			assert( length == 8 );
			width = read_long( in );
			// There is no sense in a negative width
			if ( width<0 ) width = -width;
			break;

		case BGNEXTN:
			skip_record( in, length );
			break;

		case ENDEXTN:
			skip_record( in, length );
			break;

		default:
			throw_unknown_command( in, type, __LINE__ ); 
			break;
		}

		length = read_short( in );
		type = read_type( in );
	};

	// interpret the XY
	std::vector<point> points;
	assert( (length%2) == 0 );
	assert( ((length - 4) % 8) == 0 );
	int count = (length - 4) / 8;
	while ( count-- )
	{
		long x = read_long( in );
		long y = read_long( in );
		points.push_back( point( x, y ) );
		
		// some GDSII files have points doubled up
		if ( points.size()>1 && points[ points.size()-2] == points[ points.size()-1 ] )
		{
			points.pop_back();
		}
	}

	// add the polygon to the view if there are enough points
	if ( points.size() < 2 ) 
		throw_path_error( in );

	// create the path
	assert( points.size() >= 2 );
	if ( width == 0 ) width = 1;
	view.insert( layer, points, width, pathtype );

	// read in the end of element
	length = read_short( in );
	type = read_short( in );
	if ( type != ENDEL ) throw_endel_error( in );
	skip_record( in, length );
}

static void calc_transform( transform& t, short strans, double magnification, double angle, point offset )
{
	// reset the transform
	t = transform(R0);
	// apply the reflect flag
	if ( strans & 0x8000 ) t.mirror_x();
	t.scale( magnification );
	t.rotate( angle );
	t.translate( offset );
}

static void import_sref_records( std::istream& in, short length, layout& view )
{
	// skip rest of record
	skip_record( in, length );

	length = read_short( in );
	short type = read_type( in );

	while ( type != SNAME )
	{
		switch ( type )
		{
		case ELFLAGS:
		case PLEX:
			skip_record( in, length );
			break;
		default:
			throw_unknown_command( in, type, __LINE__ );
			break;
		}

		length = read_short( in );
		type = read_type( in );
	};

	// interpret SNAME
	std::string sname;
	import_sname_record( in, length, sname );

	// interpret STRANS
	length = read_short( in );
	type = read_type( in );
	short strans = 0;
	if ( type == STRANS )
	{
		assert( length == 6 );
		strans = read_short( in );
		length = read_short( in );
		type = read_type( in );
	}

	// interpret magnification
	double magnification = 1.0;
	if ( type == MAG )
	{
		assert( length == 12 );
		magnification = read_double( in );
		length = read_short( in );
		type = read_type( in );
	}

	// interpret rotation
	double angle = 0;
	if ( type == ANGLE )
	{
		assert( length == 12 );
		angle = read_double( in );
		angle *= 3.141592654 / 180;
		length = read_short( in );
		type = read_type( in );
	}

	// interpret XY
	if ( type != XY ) throw_unexpected( in, "XY" );
	long offset_x, offset_y;
	assert( (length%2) == 0 );
	assert( ((length - 4) % 8) == 0 );
	int count = (length - 4) / 8;
	assert( count == 1 );
	while ( count-- )
	{
		offset_x = read_long( in );
		offset_y = read_long( in );
	}

	// create the path
	transform t;
	calc_transform( t, strans, magnification, angle, point(offset_x,offset_y) );
	view.insert( sname.c_str(), t );

	// read in the end of element
	length = read_short( in );
	type = read_type( in );
	assert( !! in );
	if ( type != ENDEL ) throw_endel_error( in );
	skip_record( in, length );
}

static void import_aref_records( std::istream& in, short length, layout& view )
{
	// skip rest of record
	skip_record( in, length );

	length = read_short( in );
	short type = read_type( in );

	while ( type != SNAME )
	{
		switch ( type )
		{
		case ELFLAGS:
		case PLEX:
			skip_record( in, length );
			break;
		default:
			throw_unknown_command( in, type, __LINE__ );
			break;
		}

		length = read_short( in );
		type = read_type( in );
	};

	// interpret SNAME
	std::string sname;
	import_sname_record( in, length, sname );

	// interpret STRANS
	length = read_short( in );
	type = read_short( in );
	short strans = 0;
	if ( type == STRANS )
	{
		assert( length == 6 );
		strans = read_short( in );
		length = read_short( in );
		type = read_type( in );
	}

	// interpret magnification
	double magnification = 1.0;
	if ( type == MAG )
	{
		assert( length == 12 );
		magnification = read_double( in );
		length = read_short( in );
		type = read_type( in );
	}

	// interpret rotation
	double angle = 0;
	if ( type == ANGLE )
	{
		assert( length == 12 );
		angle = read_double( in );
		angle *= 3.141592654 / 180;
		length = read_short( in );
		type = read_type( in );
	}
	assert( in.good() );
	
	// get the col row
	if ( type != COLROW ) throw_unexpected( in, "COLROW" );
	if ( length != 8 ) throw_unexpected( in, "COLROW" );
	short columns = read_short( in );
	short rows = read_short( in );
	assert( columns >= 0 );
	assert( rows >= 0 );
	if ( rows==0 ) rows = 1;
	if ( columns==0 ) columns = 1;

	// interpret XY
	length = read_short( in );
	type = read_short( in );
	if ( type != XY ) throw_unexpected( in, "XY" );
	point points[3] = { {0,0}, {0,0}, {0,0} /*dummy values*/ };
	assert( (length%2) == 0 );
	assert( ((length - 4) % 8) == 0 );
	int count = (length - 4) / 8;
	assert( count == 3 );
	while ( count-- )
	{
		points[count].x = read_long( in );
		points[count].y = read_long( in );
	}
	// assert( point[0].x == point[2].x );
	// assert( point[1].y == point[2].y );
	
	// create the instance
	transform t;
	calc_transform( t, strans, magnification, angle, points[2] );
	point delta_r = point( (points[0].x - points[2].x) / rows, (points[0].y - points[2].y) / rows );
	point delta_c = point( (points[1].x - points[2].x) / columns, (points[1].y - points[2].y) / columns );
	assert( rows==1 || delta_r.x!=0 || delta_r.y!=0 );
	assert( columns==1 || delta_c.x!=0 || delta_c.y!=0 );
	view.insert( sname.c_str(), t, rows, columns, delta_r, delta_c );

	// read in the end of element
	length = read_short( in );
	type = read_short( in );
	assert( in.good() );
	if ( type != ENDEL ) throw_endel_error( in );
	skip_record( in, length );
}

static void import_text_record( std::istream& in, short length, layout& view )
{
	layer_id layer;
	long width;
	short datatype = 0;

	// skip rest of record
	skip_record( in, length );

	length = read_short( in );
	short type = read_type( in );

	while ( type != XY )
	{
		switch ( type )
		{
		case ELFLAGS:
			skip_record( in, length );
			break;

		case PLEX:
			skip_record( in, length );
			break;

		case LAYER:
			assert( length == 6 );
			layer = read_short( in );
			break;

		case MAG:
			assert( length == 12 );
			{
				double mag = read_double( in );
			}
			break;

		case ANGLE:
			assert( length == 12 );
			{
				double ang = read_double( in );
			}
			break;


		case TEXTTYPE:
			//assert( length == 6 );
			//datatype = read_short( in );
			skip_record( in, length );
			break;

		case PRESENTATION:
			skip_record( in, length );
			break;

		case PATHTYPE:
			skip_record( in, length );
			break;

		case STRANS:
			skip_record( in, length );
			break;

		case WIDTH:
			assert( length == 8 );
			width = read_long( in );
			break;

		default:
			throw_unknown_command( in, type, __LINE__ );
			break;
		}

		length = read_short( in );
		type = read_type( in );
	};

	// interpret the XY
	coord_t pos_x, pos_y;
	assert( (length%2) == 0 );
	assert( ((length - 4) % 8) == 0 );
	int count = (length - 4) / 8;
	assert( count == 1 );
	while ( count-- )
	{
		pos_x = read_long( in );
		pos_y = read_long( in );
	}

	// interpret the STRING
	length = read_short( in );
	type = read_type( in );
	if ( type != STRING ) throw_unexpected( in, "STRING" );
	skip_record( in, length );

	// read in the end of element
	length = read_short( in );
	type = read_short( in );
	assert( in.good() );
	if ( type != ENDEL ) throw_endel_error( in );
	skip_record( in, length );
}

static void import_bgnstr_records( std::istream& in, short length, layout& view )
{
	// skip rest of record
	skip_record( in, length );

	length = read_short( in );
	short type = read_type( in );

	while ( type != ENDSTR )
	{
		switch ( type )
		{
		case STRNAME:
			{
				std::string name;
				import_strname_record( in, length, name );
				view.set_name( name );
			};
			break;

		case BOUNDARY:
			import_boundary_record( in, length, view );
			break;

		case PATH:
			interpret_path( in, length, view );
			break;

		case TEXT:
			import_text_record( in, length, view );
			break;

		case SREF:
			import_sref_records( in, length, view );
			break;

		case AREF:
			import_aref_records( in, length, view );
			break;

		default:
			throw_unknown_command( in, type, __LINE__ );
			break;
		};

		length = read_short( in );
		type = read_type( in );
		assert( in.good() );
	}
	
	// We should be finishing with an ENDSTR record
	assert( type == ENDSTR );
	assert( length == 4 ); 
}

/************************************************************************
  These routines are the top level routines
************************************************************************/

layout*	gdsii::import( std::list<layout>& layouts, double* scale, 
	on_comment_handler on_comment, void* on_comment_userdata,
	std::istream& in )
{
	short length;
	short type;
	short version;
	std::string	libname;
	std::vector<layout*>	post_processing;

	// read in header
	length = read_short( in );
	assert( ! (length % 2) );
	type = read_short( in );
	
	if ( type != HEADER ) throw_unexpected( in, "HEADER" );
	if ( length != 6 ) throw_unexpected( in, "HEADER" );
	version = read_short( in );
	if ( on_comment ) log_version( version, on_comment, on_comment_userdata );
	
	// read in beginining of library
	length = read_short( in );
	type = read_short( in );
	if ( type != BGNLIB ) throw_unexpected( in, "BGNLIB" );
	skip_record( in, length );

	// read in lib name
	length = read_short( in );
	type = read_short( in );
	if ( type != LIBNAME ) throw_unexpected( in, "LIBNAME" );
	import_libname_record( in, length, libname );

	// move through the  rest of the header
	length = read_short( in );
	type = read_short( in );
	while ( type != UNITS && in.good() )
	{

		skip_record( in, length );
		length = read_short( in );
		type = read_short( in );
	} 
	double scaling_a, scaling_b;
	import_units_record( in, length, scaling_a, scaling_b ); 
	if ( scale ) *scale = scaling_b;

	length = read_short( in );
	type = read_short( in );
	while ( type != ENDLIB )
	{
		switch ( type )
		{
		case BGNSTR:
			// create the new layout
			layouts.push_back( layout() );
			import_bgnstr_records( in, length, layouts.back() );
			// save the cell address for post-processing
			post_processing.push_back( & layouts.back() );
			break;

		default:
			throw_unknown_command( in, type, __LINE__ );
			break;
		}

		length = read_short( in );
		type = read_short( in );
		assert( in.good() );
	};

	// convert name composites
	std::vector<layout*>::iterator lp;
	for ( lp = post_processing.begin(); lp != post_processing.end(); ++lp )
	{
		convert_layout_name_composites( *lp, layouts );
	}

	// return cell
	// check that we have imported at least one cell
	return ( post_processing.size() > 0 ) 
	? get_top_layout( post_processing, on_comment, on_comment_userdata )
	: 0 /* NULL */;
}

bool gdsii::is_file_gdsii( std::istream& in )
{
	assert( sizeof(short) == 2 );

	short length = read_short( in );
	if ( length != 6 ) return false;
	short type = read_short( in );
	if ( type != 2 ) return false;
	// library currently only reads one version of gdsii
	short ver = read_short( in );
	//if ( ver != 1 ) return false;
	length = read_short( in );
	//if ( length != 16 ) return false;
	type = read_short( in );
	if ( type != 0x102 ) return false;
	
	// assume the file is good
	return true;
}
