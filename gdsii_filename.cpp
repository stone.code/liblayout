/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include <fstream>
#include <stdexcept>


using namespace liblayout;

layout* gdsii::import( std::list<layout>& layouts, double* scale, 
	on_comment_handler on_comment, void* on_comment_userdata,
	char const* filename )
{
	std::ifstream file;

	file.open( filename, std::ios::in | std::ios::binary );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for reading:  " ) + filename );
	}
	return import( layouts, scale, on_comment, on_comment_userdata, file );
}

void gdsii::write( layout const& view, double scale, char const* filename )
{
	std::ofstream file;

	file.open( filename, std::ios::out | std::ios::binary );
	if ( !file ) {
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	write( view, scale, file );
	if ( !file ) {
		throw std::runtime_error( "Error writing out the cif file." );
	}
}

bool gdsii::is_file_gdsii( char const* filename )
{
	std::ifstream file;

	file.open( filename, std::ios::in | std::ios::binary );
	if ( !file ) {
		throw std::runtime_error( std::string( "Could not open the following file for reading:  " ) + filename );
	}
	return is_file_gdsii( file );
}
