/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include <assert.h>
#include <stdexcept>

using namespace liblayout;

rect polygon::bounds() const
{
	if ( empty() ) throw std::runtime_error( "Polygon is empty." );

	const_iterator lp = begin();
	rect r( lp->x, lp->y, lp->x, lp->y );

	for ( ++lp; lp != end(); ++lp ) {
		r += *lp;
	}

	return r;
}

void polygon::normalize()
{
	// nothing to do if the polygon is empty
	if ( begin() == end() ) return;
	
	// first check against front and back
	// this test needs to be done, and it is cheapest to 
	// pop_back to remove duplicates
	while( size()>1 && front() == back() ) pop_back();
	assert( begin() != end() );
	
	// remember, the first point is always unique
	const_iterator lp = begin() + 1;
	iterator out = begin()+1;
	for ( ; lp != end(); ++lp ) {
		if ( *lp != *(lp-1) ) {
			*out = *lp;
			++out;
		}
	}
	
	// we don't need anything in the array [out,end)
	erase( out, end() );
}

bool polygon::valid() const
{
	// first, we need at least three points
	if ( size()<3 ) return false;
	
	// check for distinct x and distinct y values
	const_iterator lp = begin();
	rect r( lp->x, lp->y, lp->x, lp->y );
	for ( ++lp; lp != end(); ++lp ) {
		r += *lp;
		if ( r.valid() ) return true;
	}
	
	return false;
}
