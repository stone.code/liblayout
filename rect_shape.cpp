/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>

using namespace liblayout;

rect_shape::rect_shape( layer_id id, rect const& rhs )
: shape::imp(id), rect(rhs)
{
	assert( valid() );
}

rect rect_shape::bounds() const
{
	return *this;
}

int rect_shape::contains_point( point pt ) const
{
	assert( false );
	return int();
}

shape::imp* rect_shape::eval( transform const& t ) const
{
	// can only do manhattan transforms
	if ( t.is_manhattan() )
	{
		rect r( t*point(x1,y1), t*point(x2,y2) );
		r.normalize();
		return new rect_shape( layer, r );
	}

	// but a polygon can handle all linear transforms
	polygon p;
	get_polygon(p);
	t.apply( p );
	return new polygon_shape( layer, p );
}

void rect_shape::get_polygon( polygon& p ) const
{
	p.empty();
	p.push_back( point( x1, y1 ) );
	p.push_back( point( x1, y2 ) );
	p.push_back( point( x2, y2 ) );
	p.push_back( point( x2, y1 ) );
}
