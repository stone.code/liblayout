/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include <assert.h>
#include <iostream>
#include <math.h>
#include <time.h>

using namespace liblayout;

#define POINTS_PER_INCH (72)
#define INCHES_PER_METER (39.37007874)

class universaltime
{};

std::ostream& operator<<( std::ostream& out, universaltime _dummy )
{
	time_t     now;
    struct tm  ts;
    char       buf[80];

    // Get the current time
    now = time(0);

    // Format and print the time, "ddd yyyy-mm-dd hh:mm:ss zzz"
    ts = *gmtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S UTC", &ts);
	out << buf;

    return out;
}

static void write_header( std::ostream& out, char const* title, rect const& bounds, double scale ) //* goes before first page
{
	// document header
	out << "%!PS-Adobe-2.0\n";
	out << "%%BoundingBox:  0 0 " << ceil(bounds.width()*scale+2) << ' ' << ceil(bounds.height()*scale+2) << '\n'; 
	out << "%%Creator: " PACKAGE_NAME " " PACKAGE_VERSION "\n";
	out << "%%CreationDate:  " << universaltime() << '\n';
	if ( title && *title ) out << "%%Title:  " << title << '\n';
	out << "%%Pages: 1\n";
	out << "%%EndComments\n";

	// start of the only page
	out << "%%Page:  1 1\n";
	// add a pagesize directive (add 1pt wide margin)
	out << "<< /PageSize [" << (bounds.width()*scale + 2) << ' ' 
		<< (bounds.height()*scale + 2) << "] >> setpagedevice\n";
	out << "gsave\n";

	// translate so that image is in upper right quadrant
	// offset for the 1pt margin
	assert( bounds.x1 <= bounds.x2 );
	assert( bounds.y1 <= bounds.y2 );
	out << (-bounds.x1*scale+1) << ' ' << (-bounds.y1*scale+1) << " translate\n";
}

static void write_style_fill( std::ostream& out, style const& pen )
{
	assert( pen.has_fill() );
	float r = pen.fill.red() / 255.0;
	float g = pen.fill.green() / 255.0;
	float b = pen.fill.blue() / 255.0;
	out << r << ' ' << g << ' ' << b << " setrgbcolor";
}

static void write_style_stroke( std::ostream& out, style const& pen )
{
	// set paint
	assert( pen.has_stroke() );
	float r = pen.stroke.red() / 255.0;
	float g = pen.stroke.green() / 255.0;
	float b = pen.stroke.blue() / 255.0;
	out << r << ' ' << g << ' ' << b << " setrgbcolor ";

	// set width
	assert( pen.stroke_width > 0 );
	out << pen.stroke_width << " setlinewidth ";
	
	// set pattern
	switch ( pen.stroke_pattern ) {
		case style::stroke_dotted:
			out << " [1 4] 0 setdash "; break;
		case style::stroke_dashed:
			out << " [8 8] 0 setdash "; break;
		case style::stroke_longdashed:
			out << " [24 8] 0 setdash "; break;
		case style::stroke_dashdotted:
			out << " [8 8 1 8] 0 setdash "; break;
		case style::stroke_longdashdotted:
			out << " [24 8 1 8] 0 setdash "; break;
		//case style::stroke_solid:
		default:
			out << " [] 0 setdash "; break;
	}
	
	// set join
	switch( pen.stroke_join ) {
		case style::join_miter: out << " 0 setlinejoin "; break;
		case style::join_round: out << " 1 setlinejoin "; break;
		case style::join_bevel: out << " 2 setlinejoin "; break;
		default: assert(false); break;
	}
	
	// and finish the line
	out << '\n';
}

static void write_style( std::ostream& out, style const& pen )
{
	// set function for stroking and filling
	if ( pen.has_fill() && pen.has_stroke() ) {
		write_style_stroke( out, pen );
		out << "\n/stroke-and-fill { gsave ";
		write_style_fill( out, pen );
		out << " fill grestore stroke } def\n";
	}
	else if ( pen.has_fill() ) {
		write_style_fill( out, pen );
		out << "\n/stroke-and-fill { fill } def\n";
	}
	else if ( pen.has_stroke() ) {
		write_style_stroke( out, pen );
		out << "/stroke-and-fill { stroke } def\n";
	}
	else {
		out << "/stroke-and-fill {} def\n";
	}
}

static void write_footer( std::ostream& out ) // goes after last page
{
	// end of the only page
	out << "grestore\n";
	out << "showpage\n";

	// end of the document
	out << "%%EOF" << std::endl;
}

static void write_shape( std::ostream& file, double scale, shape const& view )
{
	// convert to a polygon
	polygon p;
	view.get_polygon( p );

	polygon::const_iterator lp = p.begin();
	file << lp->x*scale << ' ' << lp->y*scale << " moveto %% polygon " << p.size() << '\n';
	for ( ++lp; lp != p.end(); ++lp ) {
		file << lp->x*scale << ' ' << lp->y*scale << " lineto\n";
	}
	file << "closepath stroke-and-fill\n";
}

float get_ps_scale( double scale, rect const& bounds )
{
	assert( scale >= 0 );

	// if scale is zero, autoscale
	if ( scale == 0.0 ) {
		double scale1 = 4.0 * POINTS_PER_INCH / bounds.width();
		double scale2 = 4.0 * POINTS_PER_INCH / bounds.height();
		return (scale1<scale2) ? scale1 : scale2;
	}

	// convert from scaling in meter to postscript unts
	return scale * POINTS_PER_INCH * INCHES_PER_METER;
}

void ps::write( layout const& view, double scale, ps::map_id_style const& id_to_css, std::ostream& out )
{
	// get size
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// convert scaling to postscript value
	scale = get_ps_scale( scale, bounds );

	// get the layer in the layout
	std::vector<layer_id> layers;
	view.get_layers( layers );
	std::vector<layer_id>::const_iterator layer;
	// sort the layers by order of preference
	style::sort( layers, id_to_css );

	// write the header to the file
	write_header( out, view.get_name(), bounds, scale );

	for ( layer = layers.begin(); layer != layers.end(); ++layer ) {
		// get the shapes for this layer
		std::vector<shape>	shapes;
		view.get_shapes( shapes, *layer );
		
		// write out the style information
		map_id_style::const_iterator pen = id_to_css.find( *layer );
		if ( pen != id_to_css.end() )
			write_style( out, pen->second );
		else
			write_style( out, style() );
	
		// print the shapes
		std::vector<shape>::const_iterator lp;
		for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
			write_shape( out, scale, *lp );

	}
	write_footer( out );
}

void ps::write( layout const& view, layer_id layer, double scale, ps::map_id_style const& id_to_css, std::ostream& out )
{
	// get size
	rect bounds = view.bounds();
	assert( bounds.valid() );

	// convert scale to postscript value
	scale = get_ps_scale( scale, bounds );

	// get the shapes
	std::vector<shape>	shapes;
	view.get_shapes( shapes, layer );

	// write the header to the file
	write_header( out, view.get_name(), bounds, scale );

	// write out the style information
	map_id_style::const_iterator pen = id_to_css.find( layer );
	if ( pen  != id_to_css.end() )
		write_style( out, pen ->second );
	else
		write_style( out, style() );

	// print the shapes	
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
		write_shape( out, scale, *lp );
		
	// and we are done
	write_footer( out );
}
