/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2015 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include "vendor/catch.hpp"

TEST_CASE( "style" )
{
	liblayout::style info;
	
	// check fill style
	info.set_fill( "none" );
	REQUIRE( !info.has_fill() );
	info.set_fill( "red" );
	REQUIRE( info.has_fill() );
	info.set_fill_pattern( "none" );
	REQUIRE( !info.has_fill() );
	
	// check stroke style
	info.set_stroke( "none" );
	REQUIRE( !info.has_stroke() );
	info.set_stroke( "red" );
	REQUIRE( info.has_stroke() );
	info.set_stroke_pattern( "none" );
	REQUIRE( !info.has_stroke() );
	
}
