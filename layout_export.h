/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#ifndef __LAYOUT_EXPORT_H_
#define __LAYOUT_EXPORT_H_

#include "layout.h"

/************************************************************************
** namespace liblayout
************************************************************************/

namespace liblayout
{
	/// This structure includes all of the information to map geometry
	/// from a layout to an output image.
	///
	/// All of the fields may be zero.  The non-zero fields can be
	/// be computed from any information that is present when combined 
	/// with the bounds for the layout.
	struct scale_info
	{
		/// The desired scaling between the layout and the output image.
		double	scale;
		/// The desired width of the output image in meters.
		double	width;
		/// The desired height of the output image in meters.
		double	height;
		
		/// Returns true if all of the fields have been completed.
		bool	is_complete() const { return scale!=0 && height!=0 && width!=0 ; };
		/// Returns true if the either the width or height is not large
		/// enough to contain the layout.
		bool	will_crop( rect const& bounds ) const;
		/// Returns true if the either the width or height is not large
		/// enough to contain the layout.
		bool	will_crop( layout const& view ) const
		{ return will_crop( view.bounds() ); };
		
		/// Create a new scale_info structure where all of the fields have been completed.
		scale_info	complete_fields( rect const& bounds, double default_width=0.127, double default_height=0.127 ) const;
		/// Create a new scale_info structure where all of the fields have been completed.
		scale_info	complete_fields( layout const& view, double default_width=0.127, double default_height=0.127 ) const
		{ return complete_fields( view.bounds(), default_width, default_height ); };
	};

	struct colour
	{
	public:
		// this should be a 32-bit size
		typedef unsigned long value_t;

		constexpr colour( value_t rhs ) : val(rhs) {};
		constexpr colour( colour const& rhs ) : val(rhs.val) {};
		constexpr colour( short r, short g, short b ) 
		: val(((long)b << 16) + ((long)g << 8) + r) {};
		constexpr colour( short r, short g, short b, short a )
		: val(((long)a << 24) + ((long)b << 16) + ((long)g << 8) + r) {};

		colour&		operator=( value_t rhs ) { val=rhs; return *this; };
		colour&		operator=( colour const& rhs) { val=rhs.val; return *this; };
		
		value_t		get() const { return val; };
		short		red() const { return val & 0xFF; };
		short		green() const { return (val>>8) & 0xFF; };
		short		blue() const { return (val>>16) & 0xFF; };
		short		alpha() const { return (val>>24) & 0xFF; };
		
		static colour	from_text(char const* text);

	private:
		value_t val;
	};
	
	/// This structure contains all of the style information used to paint
	/// geometry for a layer.
	struct style
	{
		enum join_t { join_miter, join_round, join_bevel };
		enum fill_t { fill_empty=0, fill_solid, fill_bdiagonal, fill_fdiagonal,
			fill_hatched, fill_vertical, fill_horizontal, fill_cross };
		enum stroke_t { stroke_empty=0, stroke_solid, stroke_dotted, stroke_dashed, 
			stroke_longdashed, stroke_dashdotted, stroke_longdashdotted };
	
		short		order; // order that layers are drawn
		colour		fill; // rgba
		fill_t		fill_pattern;
		colour		stroke; // rgba
		stroke_t	stroke_pattern;
		float		stroke_width; // in points
		join_t		stroke_join;
		float		stroke_miterlimit; // in points
		
		style();
		
		void		set_fill( char const* );
		void		set_fill_pattern( char const* );
		void		set_stroke( char const* );
		void		set_stroke_pattern( char const* );
		void		set_stroke_join( char const* );
		void		set_stroke_width( char const* );
		
		float		fill_opacity() const { return (0xFF - fill.alpha())/255.0f; };
		float		stroke_opacity() const { return (0xFF - fill.alpha())/255.0f; };
		
		bool		has_fill() const { return (fill_pattern!=fill_empty) && fill.alpha()!=0xFF; };
		bool		has_stroke() const { return (stroke_pattern!=stroke_empty) && stroke.alpha()!=0xFF; };
		
		bool		is_valid() const;
		
		static stroke_t	get_stroke( char const* fill );
		static fill_t	get_fill( char const* fill );
		static void	sort( std::vector<layer_id>&, std::map<layer_id,style> const& );
	};
	std::ostream& operator<<( std::ostream&, style const& );

	/// This namespace supports exporting ANSYS Neutral Files (ANF).
	///
	/// No scaling of the output geometry is currently supported.  Geometry 
	/// can be easily scaled inside ANSYS to the desired final size.
	///
	/// Typical use is to export a single layer to create 2D geometry for finite element simulation.
	/// However, multiple layers can be exported.  In this case, the geometry is marked with a
	/// material number matching the layer_id.
	namespace ansys
	{
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, layer_id layer, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, layer_id layer, char const* filename ); 
		/// Write out the geometry from all layers of a layout to an output stream.
		void	write( layout const& view, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void	write( layout const& view, char const* filename ); 
	}

	/// This namespace supports exporting to Gerber files.
	///
	/// The version of Gerber targeted is RS-274X.  This extension to the
	/// Gerber file format supports polygons, which are necessary to handle all
	/// layout data.
	///
	/// The output driver does not currently make use of dark field flags, and so all drawing
	/// layers are exported as essentially light-field masks.  However, this can be fixed
	/// by changing the image polarity in the text file by hand.  For example, the line IPPOS can
	/// be changed to IPNEG.  Note that the polarity cannot be set on a layer by layer basis.
	///
	/// Also, no support is given for reflecting the output image.  Not all Gerber viewers
	/// handle this correctly.  However, this effect can be easily achieved in liblayout
	/// by applying a transform to the geometry.
	///
	/// In all functions, the scale parameter is used to convert layout database units
	/// to meters.
	namespace gerber
	{
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, layer_id layer, double scale, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, layer_id layer, double scale, char const* filename );
		/// Write out the geometry from all layers of a layout to an output stream.
		void	write( layout const& view, double scale, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void	write( layout const& view, double scale, char const* filename );
	}

	/// This namespace supports exporting to IGES files.
	///
	/// No scaling of the output geometry is currently supported.  Most
	/// tools capable of importing IGES files are geometry processors, and
	/// should be able to handle any scaling, if necessary.
	namespace iges
	{
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, layer_id layer, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, layer_id layer, char const* filename );
		/// Write out the geometry from all layers of a layout to an output stream.
		void	write( layout const& view, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void	write( layout const& view, char const* filename );
	}

	/// This namespace supports writing out SVG images from layout data.
	///
	/// All of the functions in this namespace allow a scaling parameter of zero.
	/// In this case, the SVG image will have a width listed as 100%, instead of
	/// some fixed size.
	///
	/// For positive scale parameters, the scale parameter is used to convert
	/// layout coordinates to meters.  The output routines will calculate the
	/// size of the resulting image.  The image attributes will thus specify
	/// a fixed size.
	namespace svg
	{
		/// Map a layer id to style information.
		///
		/// When attempting to write geometry from a layer not found in the
		/// map, the driver can instead use the style information for a layer
		/// id of zero.  If style information for layer zero is not present,
		/// then the driver should return an error.
		typedef std::map<layer_id,style>	map_id_style;

		/// Write out the geometry from all layers of a layout to an output stream.
		void	write( layout const& view, map_id_style const& layer_map, double scale, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void	write( layout const& view, map_id_style const& layer_map, double scale, char const* filename );
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, layer_id, map_id_style const& layer_map, double scale, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, layer_id, map_id_style const& layer_map, double scale, char const* filename );
		/// Write out a single shape to an output stream.
		void	write( shape const& s, double scale, std::ostream& out );
		/// Write out a single shape to a file.
		void	write( shape const& s, double scale, char const* filename );
		/// Write out a polygon to an output stream.
		void	write( polygon const& p, double scale, std::ostream& out );
		/// Write out a polygon to a file.
		void	write( polygon const& p, double scale, char const* filename );
	}

	/// This namespace supports writing out text data from layouts.
	///
	/// The resulting text file contains summary information about the 
	/// layout file (bounds, layers present, etc.).
	namespace text
	{
		typedef cif::map_id_name map_id_name;

		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, layer_id layer, map_id_name const& id_to_name, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, layer_id layer, map_id_name const& id_to_name, char const* filename ); 
		/// Write out the geometry from all layers of a layout to an output stream.
		void	write( layout const& view, map_id_name const& id_to_name, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void	write( layout const& view, map_id_name const& id_to_name, char const* filename ); 
	}

	/// This namespace supports writing out VRML97 data from layouts.
	///
	/// If multiple layers are exported, then the layers are stacked
	/// vertically.  Each layer's height is determined by their
	/// location order, as specified by their styling (css) 
	/// information.
	///
	/// The scale parameter is used to transform the layout 
	/// coordinates to the coordinate space of the VRML file.
	/// Auto-scaling is not supported, so the scale parameter cannot
	/// be zero.
	namespace vrml
	{
		using svg::map_id_style;

		/// Write out the geometry from all layers of a layout to an output stream.
		void	write( layout const& view, map_id_style& layer_map, double scale, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void	write( layout const& view, map_id_style& layer_map, double scale, char const* filename );
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, layer_id layer, map_id_style& layer_map, double scale, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, layer_id layer, map_id_style& layer_map, double scale, char const* filename );
	}

	/// This namespace supports writing Windows Metafiles (WMF) from layout data.
	///
	/// For positive scale parameters, the scale parameter is used to convert
	/// layout coordinates to meters.  The output routines will calculate the
	/// size of the resulting image.  The image attributes will thus specify
	/// a fixed size.
	namespace wmf
	{
		using svg::map_id_style;

		/// Write out the geometry from all layers of a layout to an output stream.
		void	write( layout const& view, map_id_style const& layer_map, double scale, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void	write( layout const& view, map_id_style const& layer_map, double scale, char const* filename );
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, layer_id layer, map_id_style const& layer_map, double scale, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, layer_id layer, map_id_style const& layer_map, double scale, char const* filename );
	}

	/// This namespace supports writing Postscript from layout data.
	///
	/// For positive scale parameters, the scale parameter is used to convert
	/// layout coordinates to meters.  The output routines will calculate the
	/// size of the resulting image.  The image attributes will thus specify
	/// a fixed size.
	///
	/// If the scale parameter is zero, the layout data will be scaled to
	/// some unspecified, but macroscopically viewable, size.
	namespace ps
	{
		typedef svg::map_id_style map_id_style;

		/// Write out the geometry from all layers of a layout to an output stream.
		void	write( layout const& view, double scale, map_id_style const& id_to_css, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void	write( layout const& view, double scale, map_id_style const& id_to_css, char const* filename );
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, layer_id layer, double scale, map_id_style const& id_to_css, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, layer_id layer, double scale, map_id_style const& id_to_css, char const* filename );
	}

	/// This namespace supports writing Postscript from layout data.
	///
	/// Each layer in the layouts will be drawn on separate pages.  Each page
	/// thus represents one mask.
	///
	/// For positive scale parameters, the scale parameter is used to convert
	/// layout coordinates to meters.  The output routines will calculate the
	/// size of the resulting image.  The image attributes will thus specify
	/// a fixed size.
	///
	/// Autoscaling is not supported.
	namespace psmask
	{
		typedef std::map<layer_id,bool> map_id_darkfield;
		typedef cif::map_id_name map_id_name;
		
		extern short font_size; // in points
		
		// a scale value of 0 is invalid

		void	write( layout const& view, map_id_darkfield const& map_id_df, map_id_name const& map_id_name, double scale, std::ostream& out );
		void	write( layout const& view, map_id_darkfield const& map_id_df, map_id_name const& map_id_name, double scale, char const* filename );
		void	write( layout const& view, layer_id layer, map_id_darkfield const& map_id_df, map_id_name const& map_id_name, double scale, std::ostream& out );
		void	write( layout const& view, layer_id layer, map_id_darkfield const& map_id_df, map_id_name const& map_id_name, double scale, char const* filename );
	}

	/// This namespace supports writing Asymptote files from layout data.
	///
	/// Asymptote is a vector graphics programming language used to create technical drawings.
	/// The advantage of creating an Asymptote file, rather than creating the image directly,
	/// is that user's can annotate the drawings in Asymptote, make modifications, etc.
	///
	/// If the scaling parameter is non-zero, than the final image size will be computed and added to the output file.
	namespace asy
	{
		typedef svg::map_id_style map_id_style;

		/// Write out the geometry from all layers of a layout to an output stream.
		void	write( layout const& view, map_id_style& layer_map, double scale, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void	write( layout const& view, map_id_style& layer_map, double scale, char const* filename );
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, layer_id layer, map_id_style& layer_map, double scale, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, layer_id layer, map_id_style& layer_map, double scale, char const* filename );
	}

	/// This namespace supports writing Autocad DXF files.
	///
	/// This export module does not support scaling.  If the geometry
	/// needs to be scaled, this should be done after importing your
	/// geometry into your CAD software.
	namespace dxf
	{
		typedef cif::map_id_name map_id_name;
		
		/// Write out the geometry from all layers of a layout to an output stream.
		void	write( layout const& view, map_id_name const& id_to_name, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void	write( layout const& view, map_id_name const& id_to_name, char const* filename );
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, layer_id layer, map_id_name const& id_to_name, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, layer_id layer, map_id_name const& id_to_name, char const* filename );
	}

} // namespace liblayout

#endif // __LAYOUT_LAYOUT_H_

