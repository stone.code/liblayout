/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>
#include <stdexcept>

using namespace liblayout;

rect& rect::operator+=( rect const& r )
{
	// check preconditions
	assert( x1 <= x2 );
	assert( y1 <= y2 );
	assert( r.x1 <= r.x2 );
	assert( r.y1 <= r.y2 );
	
	// update our coordinates
	if ( r.x1 < x1 ) x1 = r.x1;
	if ( r.y1 < y1 ) y1 = r.y1;
	if ( r.x2 > x2 ) x2 = r.x2;
	if ( r.y2 > y2 ) y2 = r.y2;
	
	// check post-conditions
	assert( x1 <= x2 );
	assert( y1 <= y2 );
	// and we are done
	return *this;
}

void rect::normalize()
{
	// normalize the representation of the rectangle
	if ( x2 < x1 ) std::swap( x1, x2 );
	if ( y2 < y1 ) std::swap( y1, y2 );

	// check post-conditions	
	assert( x1 <= x2 );
	assert( y1 <= y2 );
}


