/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2015 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains the methods associated with the scale_info struct.
**
*/

#include "layout_export.h"
#include "vendor/catch.hpp"

using liblayout::colour;

TEST_CASE( "colour" )
{
	REQUIRE( colour::from_text( "white" ).get() == 0xFFFFFF );
	REQUIRE( colour::from_text( "#FFFFFF" ).get() == 0xFFFFFF );
	REQUIRE( colour::from_text( "#FFF" ).get() == 0xFFFFFF );
	REQUIRE( colour::from_text( "black" ).get() == 0 );
	REQUIRE( colour::from_text( "#000000" ).get() == 0 );
	REQUIRE( colour::from_text( "#000" ).get() == 0 );
}
