/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_export.h"
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <math.h>
#include <stdexcept>
#include <string>
#include <string.h>

using namespace liblayout;

style::stroke_t style::get_stroke( char const* text )
{
        if( strcmp(text,"none")==0 || strcmp(text,"empty")==0 )
                return stroke_empty;
        if ( strcmp(text,"solid")==0 )
                return stroke_solid;
        if ( strcmp(text,"dotted")==0 )
                return stroke_dotted;
        if ( strcmp(text,"dashed")==0 )
                return stroke_dashed;
        if ( strcmp(text,"longdashed")==0 )
                return stroke_longdashed;
        if ( strcmp(text,"dashdotted")==0 )
                return stroke_dashdotted;
        if ( strcmp(text,"longdashdotted")==0 )
                return stroke_longdashdotted;
	
	// no such name
	std::string msg;
	msg.append( "The stroke pattern specification '" );
	msg.append( text );
	msg.append( "' was not recognized." );
	throw std::runtime_error( msg );
	return stroke_solid;
}

style::fill_t style::get_fill( char const* text )
{
	if ( strcmp(text,"none")==0 || strcmp(text,"empty")==0 )
		return fill_empty;
	if ( strcmp(text,"solid")==0 || strcmp(text,"fill")==0 )
		return fill_solid;
	if ( strcmp(text,"bdiagonal")==0 )
		return fill_bdiagonal;
	if ( strcmp(text,"fdiagonal")==0 || strcmp(text,"diagonal")==0 )
		return fill_fdiagonal;
	if ( strcmp(text,"hatched")==0 )
		return fill_hatched;
	if ( strcmp(text,"horizontal")==0 )
		return fill_horizontal;
	if ( strcmp(text,"vertical")==0 )
		return fill_vertical;
	if ( strcmp(text,"cross")==0 )
		return fill_cross;

	// no such name
	std::string msg;
	msg.append( "The fill pattern specificaion '" );
	msg.append( text );
	msg.append( "' was not recognized." );
	throw std::runtime_error( msg );
	return fill_solid;
}

/***********************************************************************/

style::style()
: fill(0), stroke(0)
{
	fill_pattern = fill_empty;
	stroke_pattern = stroke_solid;
	stroke_width = 0.75; // in points
	stroke_join = join_round;
	stroke_miterlimit = 0;
}

bool style::is_valid() const
{
	if ( stroke_width < 0 ) return false;
	return true;
}

void style::set_fill( char const* text )
{
	// short-cut, user can specify "none" for the colour
	if ( strcmp(text,"none")==0 ) {
		fill_pattern = fill_empty;
		return;
	}

	// look up the color
	fill = colour::from_text( text );
	// make sure that pattern is not empty
	if( fill_pattern == fill_empty ) fill_pattern = fill_solid;
}

void style::set_fill_pattern( char const* text )
{
	if( strcmp(text,"none")==0 || strcmp(text,"empty")==0 )
		fill_pattern = fill_empty;
	else if ( strcmp(text,"solid")==0 )
		fill_pattern = fill_solid;
	else {
		// no such name
		std::string msg;
		msg.append( "The fill pattern specification '" );
		msg.append( text );
		msg.append( "' was not recognized." );
		throw std::runtime_error( msg );	
	}
}

void style::set_stroke( char const* text )
{
	// short-cut, user can specify "none" for the colour
	if ( strcmp(text,"none")==0 ) {
		stroke_pattern = stroke_empty;
		return;
	}

	// look up the color
	stroke = colour::from_text( text );
	// make sure that pattern is not empty
	if( stroke_pattern == stroke_empty ) stroke_pattern = stroke_solid;
}

void style::set_stroke_pattern( char const* text )
{
	if( strcmp(text,"none")==0 || strcmp(text,"empty")==0 )
		stroke_pattern = stroke_empty;
	else if ( strcmp(text,"solid")==0 )
		stroke_pattern = stroke_solid;
	else if ( strcmp(text,"dotted")==0 )
		stroke_pattern = stroke_dotted;
	else if ( strcmp(text,"dashed")==0 )
		stroke_pattern = stroke_dashed;
	else if ( strcmp(text,"longdashed")==0 )
		stroke_pattern = stroke_longdashed;
	else if ( strcmp(text,"dashdotted")==0 )
		stroke_pattern = stroke_dashdotted;
	else if ( strcmp(text,"longdashdotted")==0 )
		stroke_pattern = stroke_longdashdotted;
	else {
		// no such name
		std::string msg;
		msg.append( "The fill pattern specification '" );
		msg.append( text );
		msg.append( "' was not recognized." );
		throw std::runtime_error( msg );	
	}
}

void style::set_stroke_join( char const* text )
{
	if ( strcmp(text,"miter")==0 )
		stroke_join = join_miter;
	else if ( strcmp(text,"round")==0 )
		stroke_join = join_round;
	else if ( strcmp( text,"bevel")==0 )
		stroke_join = join_bevel;
	else {
		// no such name
		std::string msg;
		msg.append( "The fill pattern specification '" );
		msg.append( text );
		msg.append( "' was not recognized." );
		throw std::runtime_error( msg );	
	}
}

void style::set_stroke_width( char const* text )
{
	stroke_width = fabs( atof( text ) );
}

/***********************************************************************/

class pred_order {
public:
	pred_order( std::map<layer_id,style> const& rhs ) : info(rhs) {};
	
	bool operator()( layer_id a, layer_id b )
	{ std::map<layer_id,style>::const_iterator lp1 = info.find( a );
	  std::map<layer_id,style>::const_iterator lp2 = info.find( b );
	  if ( lp1!=info.end() && lp2!=info.end() )
	  	return lp1->second.order < lp2->second.order;
	  if ( lp1!=info.end() )
	  	return false;
	  if ( lp2!=info.end() )
	  	return true;
	  return false;
	}
private:
	std::map<layer_id,style> const& info;
};

void style::sort( std::vector<layer_id>& layers, std::map<layer_id,style> const& id_to_style )
{
	std::sort( layers.begin(), layers.end(), pred_order(id_to_style) );
}
