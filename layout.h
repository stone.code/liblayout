/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#ifndef __LAYOUT_LAYOUT_H_
#define __LAYOUT_LAYOUT_H_

#include <iostream>
#include <list>
#include <map>
#include <set>
#include <string>
#include <vector>

/************************************************************************
** Function to access version of the library
************************************************************************/
extern "C"
char const* liblayout_version_zstring();

/************************************************************************
** namespace liblayout
************************************************************************/

namespace liblayout
{
	/// This type is used to uniquely identify a layer within a
	/// layout.  The value 0 should be avoided, as this is in some
	/// cases used to indicate no layer, null layer, or default
	/// layer.
	typedef int	layer_id;
	/// This type is used to handle coordinates within a layout.
	/// This is equivalent to a DB unit in normal EDA terms.
	typedef	long	coord_t;

	/// Enumerates the possible end-caps used for paths.
	enum path_end_t	
	{
		path_end_flush = 1, 
		path_end_square, 
		path_end_round
	};

	class point
	{
	public:
		coord_t	x;
		coord_t	y;

		point() {};
		constexpr point( coord_t x_, coord_t y_ ) : x(x_), y(y_) {};
		constexpr point( point const& rhs ) : x(rhs.x), y(rhs.y) {};

		bool	operator==(point const& rhs) const { return x==rhs.x && y==rhs.y; };
		bool	operator!=(point const& rhs) const { return !operator==(rhs); };
		bool	operator<(point const& rhs) const { if (x<rhs.x) return true; if (x>rhs.x) return false; return y<rhs.y; };

		point&	operator=(point const& rhs) { x=rhs.x; y=rhs.y; return *this; };
	};

	class rect
	{
	public:
		coord_t	x1, y1, x2, y2;

		rect() {};
		constexpr rect( coord_t x1_, coord_t y1_, coord_t x2_, coord_t y2_ )
		: x1(x1_), y1(y1_), x2(x2_), y2(y2_) {}
		constexpr rect( point pt1, point pt2 )
		: x1(pt1.x), y1(pt1.y), x2(pt2.x), y2(pt2.y) {}
		constexpr rect( rect const& rhs )
		: x1(rhs.x1), y1(rhs.y1), x2(rhs.x2), y2(rhs.y2) {}

		bool	operator==(rect const& rhs) const { return x1==rhs.x1 && x2==rhs.x2 && y1==rhs.y1 && y2==rhs.y2; };
		bool	operator!=(rect const& rhs) const { return !operator==(rhs); };
		rect&	operator+=(rect const& r);
		rect&	operator+=(point p) 
			{ if(p.x>x2) x2=p.x; if(p.x<x1) x1=p.x; if(p.y>y2) y2=p.y; if(p.y<y1) y1=p.y; return *this; };

		point	center() const { return point( (x1+x2)/2, (y1+y2)/2 ); };
		coord_t	center_x() const { return (x1+x2)/2; };
		coord_t	center_y() const { return (y1+y2)/2; };
		coord_t height() const { return y2-y1; };
		void	normalize();
		bool	valid() const { return x1<x2 && y1<y2; };
		coord_t	width() const { return x2-x1; };
	};

	class polygon : public std::vector<point>
	{
	public:
		polygon() {};
		polygon( std::vector<point> const& rhs ) : std::vector<point>(rhs) {};

		rect		bounds() const;
		void		normalize();
		bool		valid() const;
	};

	/// Enumerates all of the possible combinations of rotation and
	/// reflection allowed in manhattan geometries.  This is also the
	/// complete list of geometry transforms (excluding translation)
	/// allowed in manhattan geometries. 
	enum manhattan_transform
	{
		R0=1, R90, R180, R270, MX, MXR90, MY, MYR90
	};
	std::ostream& operator<<( std::ostream& out, manhattan_transform );
	bool is_manhattan_transform( char const* text );
	manhattan_transform get_manhattan_transform( char const* text );

	class transform
	{
	public:
		typedef manhattan_transform mt;

		transform() {};
		transform( manhattan_transform mt );
		transform( transform const& );
		transform( float a11, float a12, float a13, float a21, float a22, float a23, float a31, float a32, float a33 );

		float&		operator()( int i, int j ) { return matrix[i][j]; };
		float		operator()( int i, int j ) const { return matrix[i][j]; };
		transform&	operator=( transform const& rhs );
		transform	operator*( transform const& ) const;
		transform&	operator*=( transform const& rhs );
		bool		operator==( transform const& rhs ) const;
		bool		operator!=( transform const& rhs ) const { return !operator==(rhs); };

		// apply the transform to various geometric primitives
		point		operator*( point pt ) const;
		rect		operator*( rect const& ) const;
		polygon		operator*( polygon const& ) const;
		void		apply( polygon& ) const;
		
		// queries
		bool		is_manhattan() const;
		float		get_manhattan_scaling() const;
		mt		get_manhattan_transform() const;
		bool		valid() const { return (matrix[2][0]==0 && matrix[2][1]==0 && matrix[2][2]==1); };
		
		// operations
		void		mirror_x();	// x-coords unchanged
		void		mirror_y();	// y-coords unchanged
		void		reflect_x() { mirror_y(); };
		void		reflect_y() { mirror_x(); };
		void		rotate( float c, float s );
		void		rotate( float angle );
		void		scale( float s );
		void		swap( transform& rhs );
		void		translate( point const pt );
		void		translate( float dx, float dy );

	private:
		float	matrix[3][3];
	};

	class shape
	{
	public:
		class imp
		{
		public:
					imp( layer_id id ) : layer(id), ref_count(0) {};
			virtual		~imp() {};
			virtual	rect	bounds() const = 0;
			virtual	int	contains_point( point pt ) const = 0;
			virtual imp*	eval( transform const& t ) const = 0;
			virtual void	get_polygon( polygon& p) const = 0;
			
			void		add_ref() { ++ref_count; };
			void		sub_ref();
			
			layer_id const	layer;
			
		private:
			unsigned	ref_count;
		};

		shape( imp* rhs ) : ptr(rhs) { ptr->add_ref(); };
		shape( shape const& rhs ) : ptr(rhs.ptr) { ptr->add_ref(); };
		~shape() { ptr->sub_ref(); };

		shape&		operator=( imp* rhs ) { ptr->sub_ref(); ptr=rhs; ptr->add_ref(); return *this; };
		shape&		operator=( shape const& rhs ) { ptr->sub_ref(); ptr=rhs.ptr; ptr->add_ref(); return *this; };
		bool		operator<( shape const& rhs ) const { if ( ptr->layer<rhs.ptr->layer ) return false; if ( ptr->layer>rhs.ptr->layer ) return false; return ptr<rhs.ptr; };
		bool		operator==( shape const& rhs ) const { return ptr==rhs.ptr; };

		rect		bounds() const { return ptr->bounds(); };
		bool		contains_point( point pt ) const { return ptr->contains_point(pt); };
		polygon		get_polygon() const;
		void		get_polygon( polygon& p ) const { ptr->get_polygon(p); };
		layer_id	layer() const { return ptr->layer; };
		imp const*	get() const { return ptr; };
		void		swap( shape& rhs ) { std::swap(ptr,rhs.ptr); };
		shape		transform( class transform const& t ) const { return ptr->eval(t); };

	private:
		imp*		ptr;
	};

	class composite
	{
	public:
		class imp
		{
		public:
			imp() : ref_count(0) {};
			virtual		~imp() {};
			virtual	rect	bounds() const = 0;
			virtual	int	contains_point( point pt ) const = 0;
			virtual imp*	eval( transform const& t ) const = 0;
			virtual	void	get_layers( std::set<layer_id>& ) const = 0;
			virtual	void	get_shapes( std::vector<shape>& ) const = 0;
			virtual	void	get_shapes( std::vector<shape>&, layer_id ) const = 0;
			virtual	bool	has_bounds() const = 0;
			
			void		add_ref() { ++ref_count; };
			void		sub_ref();
			
		private:
			unsigned	ref_count;
		};

		composite( imp* rhs ) : ptr(rhs) { ptr->add_ref(); };
		composite( composite const& rhs ) : ptr(rhs.ptr) { ptr->add_ref(); };
		~composite() { ptr->sub_ref(); };

		composite&	operator=( imp* rhs ) { ptr->sub_ref(); ptr=rhs; ptr->add_ref(); return *this; };
		composite&	operator=( composite const& rhs ) { ptr->sub_ref(); ptr=rhs.ptr; ptr->add_ref(); return *this; };
		bool		operator<( composite const& rhs ) const { return ptr<rhs.ptr; };
		bool		operator==( composite const& rhs ) const { return ptr==rhs.ptr; };

		rect		bounds() const { return ptr->bounds(); };
		int		contains_point( point pt ) const { return ptr->contains_point(pt); };
		void		get_layers( std::set<layer_id>& lhs ) const { ptr->get_layers(lhs); };
		void		get_shapes( std::vector<shape>& shapes ) const { ptr->get_shapes( shapes ); };
		void		get_shapes( std::vector<shape>& shapes, layer_id id ) const { ptr->get_shapes( shapes, id ); };
		bool		is_layout() const;
		bool		has_bounds() const { return ptr->has_bounds(); };
		imp const*	get() const { return ptr; };
		void		swap( composite& rhs ) { std::swap( ptr, rhs.ptr ); };
		composite	transform( class transform const& t ) const { return ptr->eval(t); };

	private:
		imp*		ptr;
	};

	class layout
	{
	public:
		typedef std::list<shape>::iterator shape_iterator;
		typedef std::list<shape>::const_iterator const_shape_iterator;
		typedef std::list<composite>::iterator composite_iterator;
		typedef std::list<composite>::const_iterator const_composite_iterator;

		layout();
		layout( layout const& rhs );

		rect		bounds() const;
		void		clear();
		composite_iterator composites_begin() { return composites.begin(); };
		const_composite_iterator composites_begin() const { return composites.begin(); };
		composite&	composites_front() { return composites.front(); };
		composite const& composites_front() const { return composites.front(); };
		size_t		composites_size() const { return composites.size(); };
		composite_iterator composites_end() { return composites.end(); };
		const_composite_iterator composites_end() const { return composites.end(); };
		void		insert( shape );
		void		insert( layer_id, rect const& );
		void		insert( layer_id, coord_t radius, point center );
		void		insert( layer_id, std::vector<point> const& pts, coord_t width, path_end_t );
		void		insert( layer_id, std::vector<point> const& pts );
		void		insert( layer_id, char const* text, point pt );
		int		contains_point( point pt ) const;
		void		insert( composite );
		void		insert( layout* layout, transform const& t );
		void		insert( layout* layout, transform const& t, int rows, int cols, point delta_r, point delta_c );
		void		insert( char const* name, transform const& t );
		void		insert( char const* name, transform const& t, int rows, int cols, point delta_r, point delta_c );
		bool		empty() const { return shapes.empty() && composites.empty(); };
		composite_iterator	erase( composite_iterator );
		shape_iterator	erase( shape_iterator );
		void		get_layers( std::set<layer_id>& ) const;
		void		get_layers( std::vector<layer_id>& ) const;
		char const*	get_name() const { return name.c_str(); };
		void		get_shapes( std::vector<shape>& ) const;
		void		get_shapes( std::vector<shape>&, layer_id ) const;
		bool		has_bounds() const;
		void		flatten();
		void		set_name( char const* rhs ) { name = rhs; };
		void		set_name( std::string const& rhs ) { name = rhs; };
		shape_iterator shapes_begin() { return shapes.begin(); };
		const_shape_iterator shapes_begin() const { return shapes.begin(); };
		shape&		shape_front() { return shapes.front(); };
		shape const& shapes_front() const { return shapes.front(); };
		size_t		shapes_size() const { return shapes.size(); };
		shape_iterator shapes_end() { return shapes.end(); };
		const_shape_iterator shapes_end() const { return shapes.end(); };
		void		shapes_sort();
		void		simplify();

	private:
		std::string		name;
		std::list<shape>	shapes;
		std::list<composite>	composites;	
	};

	/// This namespace supports importing and exporting Caltech Interchange Format (CIF) files.
	namespace cif
	{
		/// Map CIF layer names to a layer id.  Note that multiple layer names can
		/// map to the same layer id, if the names are equivalent.
		typedef std::map<std::string,layer_id>	map_name_id;
		/// Map a layer id to a CIF layer name.  Note that no two layer ids should
		/// map to the same name.
		typedef std::map<layer_id,std::string>	map_id_name;
		typedef std::vector<std::string>	strings;
		/// Base type for on_comment handlers when importing a CIF file.
		typedef void (*on_comment_handler)(void* user, char const* text);
		/// Base type for on_layer handlers when importing a CIF file.
		typedef void (*on_layer_handler)(void* user, char const* layer_name, layer_id id );
		
		/// Utility function used to find the next unused layer id for a 
		/// particular map.  This routine will return the maximum id in
		/// use plus one.
		layer_id get_next_layer_id( map_name_id const& );
		/// When layers are added to the map of names->id, it is often 
		/// convenient to update the reverse map as well.
		void update_id_names( map_id_name&, map_name_id const& );
		
		template <class T>
		void on_comment_thunk(void* user, char const* text )
		{ 
			(*reinterpret_cast<T*>(user))(text);
		}
		template <class T>
		void on_layer_thunk(void* user, char const* text, layer_id id )
		{ 
			(*reinterpret_cast<T*>(user))(text, id);
		}
		
		/// Import the layout geometry from a CIF file on the input stream.
		layout*	import( std::list<layout>&, map_name_id const& layer_map, 
			on_comment_handler on_comment, void* on_comment_userdata, 
			std::istream& in );
		/// Import the layout geometry from a CIF file on the input stream.
		template <class T>
		layout*	import( std::list<layout>& layouts, map_name_id const& layer_map, T& on_comment,
			std::istream& in )
		{
			return import( layouts, layer_map, on_comment_thunk<T>, &on_comment, in );
		}
		/// Import the layout geometry from a CIF file.
		layout*	import( std::list<layout>&, map_name_id const& layer_map, 
			on_comment_handler on_comment, void* on_comment_userdata, 
			char const* filename );
		/// Import the layout geometry from a CIF file on the input stream.
		template <class T>
		layout*	import( std::list<layout>& layouts, map_name_id const& layer_map, T& on_comment,
			char const* filename )
		{
			return import( layouts, layer_map, on_comment_thunk<T>, &on_comment, filename );
		}
		/// Import the layout geometry from a CIF file on the input stream.
		layout*	import( std::list<layout>&, map_name_id& layer_map, 
			on_comment_handler on_comment, void* on_comment_userdata,
			on_layer_handler on_layer, void* on_layer_userdata, 
			std::istream& in );
		/// Import the layout geometry from a CIF file on the input stream.
		template <class T, class S>
		layout*	import( std::list<layout>& layers, map_name_id& layer_map, 
			S& on_comment, T& on_layer, std::istream& in )
		{
			return import( layers, layer_map, on_comment_thunk<T>, &on_comment,
				on_layer_thunk<S>, &on_layer, in );
		}
		/// Import the layout geometry from a CIF file.
		layout*	import( std::list<layout>&, map_name_id& layer_map, 
			on_comment_handler on_comment, void* on_comment_userdata,
			on_layer_handler on_layer, void* on_layer_userdata, 
			char const* filename );
		/// Import the layout geometry from a CIF file on the input stream.
		template <class T, class S>
		layout*	import( std::list<layout>& layers, map_name_id& layer_map, 
			S& on_comment, T& on_layer, char const* filename )
		{
			return import( layers, layer_map, on_comment_thunk<T>, &on_comment,
				on_layer_thunk<S>, &on_layer, filename );
		}
		
		
		/// Import the layer names used by the CIF file on the input stream.
		strings	import_layers( std::istream& in );
		/// Import the layer names used by the CIF file.
		strings	import_layers( char const* filename );
		
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, map_id_name& layer_map, bool keep_unknown_layers, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, map_id_name& layer_map, bool keep_unknown_layers, char const* filename );

		/// Does the input stream contain CIF data?
		bool	is_file_cif( std::istream& in );
		/// Does the file contain valid CIF data?
		bool	is_file_cif( char const* filename );
	}
	
	/// This namespace support importing and exporting GDSII files.
	///
	/// GDSII file formats note the size of the DB unit length.  While all
	/// coordinates are stored as integers, GDSII files also store a scaling
	/// parameter.  The scaling parameter represents the size of DB units
	/// in meters.
	namespace gdsii
	{
		using cif::on_comment_handler;
		using cif::on_comment_thunk;
	
		/// Import the layout geometry from a GDII file on the input stream.
		layout*	import( std::list<layout>&, double* scale, 
			on_comment_handler on_comment, void* on_comment_userdata,
			std::istream& in );
		/// Import the layout geometry from a GDII file on the input stream.
		template <class T>
		layout*	import( std::list<layout>& layouts, double* scale, T& on_comment,
			std::istream& in )
		{
			return import( layouts, scale, on_comment_thunk<T>, &on_comment, in );
		}
		/// Import the layout geometry from a GDSII file.
		layout*	import( std::list<layout>&, double* scale, 
			on_comment_handler on_comment, void* on_comment_userdata,
			char const* filename );
		/// Import the layout geometry from a GDII file.
		template <class T>
		layout*	import( std::list<layout>& layouts, double* scale, T& on_comment,
			char const* filename )
		{
			return import( layouts, scale, on_comment_thunk<T>, &on_comment, filename );
		}
		
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void	write( layout const& view, double scale, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void	write( layout const& view, double scale, char const* filename );
		
		/// Does the input stream contain GDSII data?
		bool	is_file_gdsii( std::istream& in );
		/// Does the file contain GDSII data?
		bool	is_file_gdsii( char const* filename );
	}

        /// This namespace supports import and exporting Autocad DXF files.
        ///
        /// This export module does not support scaling.  If the geometry
        /// needs to be scaled, this should be done after importing your
        /// geometry into your CAD software.
        namespace dxf
        {
		typedef cif::map_id_name map_id_name;
		typedef cif::map_name_id map_name_id;
		using cif::on_comment_handler;
		using cif::on_comment_thunk;
		using cif::on_layer_handler;
		using cif::on_layer_thunk;
		using cif::get_next_layer_id;

		/// Import the layout geometry from a DXF file on the input stream.
		layout* import( std::list<layout>&, map_name_id const& name_id, unsigned scale,
			on_comment_handler on_comment, void* on_comment_userdata,
			std::istream& in );
		/// Import the layout geometry from a DXF file on the input stream.
		template <class T>
		layout* import( std::list<layout>& layouts, map_name_id const& name_id, unsigned scale,
		T& on_comment, std::istream& in )
		{
			return import( layouts, name_id, scale, on_comment_thunk<T>, &on_comment, in );
		}
		/// Import the layout geometry from a DXF file.
		layout* import( std::list<layout>&, map_name_id const& name_id, unsigned scale,
			on_comment_handler on_comment, void* on_comment_userdata,
			char const* filename );
		/// Import the layout geometry from a DXF file.
		template <class T>
		layout* import( std::list<layout>& layouts, map_name_id const& name_id, unsigned scale,
		T& on_comment, char const* filename )
		{
			return import( layouts, name_id, scale, on_comment_thunk<T>, &on_comment, filename );
		}
		/// Import the layout geometry from a DXF file on the input stream.
		layout* import( std::list<layout>&, map_name_id& name_id, unsigned scale,
			on_comment_handler on_comment, void* on_comment_userdata,
			on_layer_handler on_layer, void* on_layer_userdata,
			std::istream& in );
		/// Import the layout geometry from a DXF file on the input stream.
		template <class T, class S>
		layout* import( std::list<layout>& layouts, map_name_id& name_id, unsigned scale,
		T& on_comment, S& on_layer, std::istream& in )
		{
			return import( layouts, name_id, scale, 
				on_comment_thunk<T>, &on_comment, 
				on_layer_thunk<S>, &on_layer, in );
		}
		/// Import the layout geometry from a DXF file.
		layout* import( std::list<layout>&, map_name_id& name_id, unsigned scale,
			on_comment_handler on_comment, void* on_comment_userdata,
			on_layer_handler on_layer, void* on_layer_userdata,
			char const* filename );
		/// Import the layout geometry from a DXF file.
		template <class T, class S>
		layout* import( std::list<layout>& layouts, map_name_id& name_id,
			unsigned scale,
			T& on_comment, S& on_layer, char const* filename )
		{
			return import( layouts, name_id, scale,
				on_comment_thunk<T>, &on_comment, 
				on_layer_thunk<S>, &on_layer, filename );
		}

		/// Write out the geometry from all layers of a layout to an output stream.
		void    write( layout const& view, map_id_name const& id_to_name, double scale, std::ostream& out );
		/// Write out the geometry from all layers of a layout to a file.
		void    write( layout const& view, map_id_name const& id_to_name, double scale, char const* filename );
		/// Write out the geometry from a particular layer of a layout to an output stream.
		void    write( layout const& view, layer_id layer, map_id_name const& id_to_name, double scale, std::ostream& out );
		/// Write out the geometry from a particular layer of a layout to a file.
		void    write( layout const& view, layer_id layer, map_id_name const& id_to_name, double scale, char const* filename );

		/// Does the input stream contain DXF data?
		bool    is_file_dxf( std::istream& in );
		/// Does the file contain DXF data?
		bool    is_file_dxf( char const* filename );

	}

} // namespace liblayout

#endif // __LAYOUT_LAYOUT_H_
