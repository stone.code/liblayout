/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdexcept>
#include <stdlib.h>

using namespace liblayout;

path_shape::path_shape( layer_id id, std::vector<point> const& rhs, coord_t w, path_end_t pe )
: shape::imp(id), std::vector<point>(rhs), width(w), path_end(pe)
{
	assert( size() >= 2 );
	assert( width > 0 );
}

rect path_shape::bounds() const
{
	// get the polygon
	polygon p;
	get_polygon( p );
	return p.bounds();
}

int path_shape::contains_point( point pt ) const
{
	assert( false );
	return int();
}

shape::imp* path_shape::eval( transform const& t ) const
{
	std::vector<point> pts;
	pts.reserve( size() );
		
	for ( const_iterator lp = begin(); lp != end(); ++lp )
		pts.push_back( t * (*lp) );
		
	double scale = sqrt( t(0,0)*t(0,0) + t(0,1)*t(0,1) );
	return new path_shape( layer, pts, lround(scale*width), path_end );
}

static point my_intersection( point seg1a, point seg1b, point seg2a, point seg2b )
{
	// solve for the equation of the two lines
	// each in the form of a*x + b*y + c = 0
	double	a1, b1, c1, a2, b2, c2;

	double denom = ((double)seg1a.x)*seg1b.y - ((double)seg1a.y)*seg1b.x;
	if ( fabs(denom) < DBL_EPSILON )
	{
		// either a1 or b1 is zero
		if ( abs( seg1a.x-seg1b.x ) < abs( seg1a.y-seg1b.y ) )
		{
			a1 = 1;
			b1 = 0;
			c1 = seg1a.x;
		}
		else
		{
			a1 = 0;
			b1 = 1;
			c1 = seg1a.y;
		}
	}
	else
	{
		a1 = ( seg1a.y - seg1b.y );
		b1 = ( seg1b.x - seg1a.x );
		c1 = denom;
	}

	denom = ((double)seg2a.x)*seg2b.y - ((double)seg2a.y)*seg2b.x;
	if ( fabs(denom) < DBL_EPSILON )
	{
		// either a1 or b1 is zero
		if ( abs( seg2a.x-seg2b.x ) < abs( seg2a.y-seg2b.y ) )
		{
			a2 = 1;
			b2 = 0;
			c2 = seg2a.x;
		}
		else
		{
			a2 = 0;
			b2 = 1;
			c2 = seg2a.y;
		}
	}
	else
	{
		a2 = ( seg2a.y - seg2b.y );
		b2 = ( seg2b.x - seg2a.x );
		c2 = denom;
	}

	// check if the segments are parallel
	if ( b1*a2 - a1*b2 == 0 ) {
		if ( seg1b == seg2a ) 
			return seg1b;
		// TODO:  Make decision on proper return in this case.
		assert( false );
		return point(0,0);
	}

	// find point of intersection
	assert( b1*a2 - a1*b2 != 0 );
	denom = 1 / ( b1*a2-a1*b2 );
	long x = lround( ( b2*c1 - c2*b1 ) * denom );
	long y = lround( ( a1*c2 - c1*a2 ) * denom );
	return point( x, y );
}

void path_shape::get_polygon( polygon& p ) const
{
	// this is 1 / ( 1 + sqrt(2) )
	// make octagon sides of same length
	const double oct_factor = 0.41421356237309509;

	// there must be at least one point in the path
	assert( !empty() );
	
	// is this path well-formed
	// i.e., do we have enough points to form a line segment?
	if ( is_dot() ) {
		// we can't form a single line segment
		
		// create a rectangular dot for our point
		point a = front();
		rect r( a.x-width, a.y-width, a.x+width, a.y+width );
		
		// create the corresponding polygon
		p.empty();
		p.push_back( point( r.x1, r.y1 ) );
		p.push_back( point( r.x1, r.y2 ) );
		p.push_back( point( r.x2, r.y2 ) );
		p.push_back( point( r.x2, r.y1 ) );
		
		// and we are done
		return;	
	}

	std::vector<point> points1;
	std::vector<point> points2;

	const_iterator lp = begin();
	point pt0 = *lp++;
	point pt1(0,0 /* dummy values */);
	do {
		assert( lp != end() );
		pt1 = *lp++;
	} while ( pt0 == pt1 );
	assert( pt0 != pt1 );
		
	// put on first cap
	switch ( path_end )
	{
	case path_end_square:
		{
		long dx = pt1.x - pt0.x;
		long dy = pt1.y - pt0.y;
		double dd = width * 0.5 / sqrt( ((double)dx)*dx + ((double)dy)*dy );
		dx = lround( dx * dd );
		dy = lround( dy * dd );
		points1.push_back( point( pt0.x - dx - dy, pt0.y + dx - dy ) );
		points2.push_back( point( pt0.x - dx + dy, pt0.y - dx - dy ) ); 
		}
		break;

	case path_end_flush:
		{
		long dx = pt1.x - pt0.x;
		long dy = pt1.y - pt0.y;
		double dd = width * 0.5 / sqrt( ((double)dx)*dx + ((double)dy)*dy );
		dx = lround( dx * dd );
		dy = lround( dy * dd );
		points1.push_back( point( pt0.x - dy, pt0.y + dx ) );
		points2.push_back( point( pt0.x + dy, pt0.y - dx ) ); 
		}
		break;
		
	case path_end_round:
		{
		// approximate with an octagon
		long dx = pt1.x - pt0.x;
		long dy = pt1.y - pt0.y;
		double dd = width * 0.5 / sqrt( ((double)dx)*dx + ((double)dy)*dy );
		long dx_2 = lround( dx * dd * oct_factor );
		long dy_2 = lround( dy * dd * oct_factor );
		dx = lround( dx * dd );
		dy = lround( dy * dd );
		points1.push_back( point( pt0.x - dx - dy_2, pt0.y + dx_2 - dy ) );
		points2.push_back( point( pt0.x - dx + dy_2, pt0.y - dx_2 - dy ) ); 
		points1.push_back( point( pt0.x - dx_2 - dy, pt0.y + dx - dy_2 ) );
		points2.push_back( point( pt0.x - dx_2 + dy, pt0.y - dx - dy_2 ) ); 
		}
		break;

	default:
		throw std::runtime_error( "Unimplemented path-end when converting to polygon." );
	}

	// put in middle points
	while ( lp != end() )
	{
		assert( pt0 != pt1 );
		
		// find next unique point
		point pt2 = *lp++;
		if ( pt2 == pt1 ) continue;
		

		long dx1 = pt1.x - pt0.x;
		long dy1 = pt1.y - pt0.y;
		double dd1 = width * 0.5f / sqrt( ((double)dx1)*dx1 + ((double)dy1)*dy1 );
		dx1 = lround( dx1 * dd1 );
		dy1 = lround( dy1 * dd1 );
		long dx2 = pt2.x - pt1.x;
		long dy2 = pt2.y - pt1.y;
		double dd2 = width * 0.5f / sqrt( ((double)dx2)*dx2 + ((double)dy2)*dy2 );
		dx2 = lround( dx2 * dd2 );
		dy2 = lround( dy2 * dd2 );

		// determine intersection points
		points1.push_back( my_intersection( 
			point( pt0.x-dy1, pt0.y+dx1 ), 
			point( pt1.x-dy1, pt1.y+dx1 ),
			point( pt1.x-dy2, pt1.y+dx2 ),
			point( pt2.x-dy2, pt2.y+dx2 )
		) );
		points2.push_back( my_intersection( 
			point( pt0.x+dy1, pt0.y-dx1 ),
			point( pt1.x+dy1, pt1.y-dx1 ),
			point( pt1.x+dy2, pt1.y-dx2 ),
			point( pt2.x+dy2, pt2.y-dx2 )
		) );

		pt0 = pt1;
		pt1 = pt2;
	};

	// put on first cap
	switch ( path_end )
	{
	case path_end_square:
		{
		long dx = pt1.x - pt0.x;
		long dy = pt1.y - pt0.y;
		float dd = width * 0.5f / sqrt( ((double)dx)*dx + ((double)dy)*dy );
		dx = lround( dx * dd );
		dy = lround( dy * dd );
		points1.push_back( point( pt1.x + dx - dy, pt1.y + dx + dy ) );
		points2.push_back( point( pt1.x + dx + dy, pt1.y - dx + dy ) ); 
		}
		break;

	case path_end_flush:
		{
		long dx = pt1.x - pt0.x;
		long dy = pt1.y - pt0.y;
		double dd = width * 0.5 / sqrt( ((double)dx)*dx + ((double)dy)*dy );
		dx = lround( dx * dd );
		dy = lround( dy * dd );
		points1.push_back( point( pt1.x - dy, pt1.y + dx ) );
		points2.push_back( point( pt1.x + dy, pt1.y - dx ) ); 
		}
		break;

	case path_end_round:
		{
		// approximate with an octagon
		long dx = pt1.x - pt0.x;
		long dy = pt1.y - pt0.y;
		double dd = width * 0.5 / sqrt( ((double)dx)*dx + ((double)dy)*dy );
		long dx_2 = lround( dx * dd * oct_factor );
		long dy_2 = lround( dy * dd * oct_factor );
		dx = lround( dx * dd );
		dy = lround( dy * dd );
		points1.push_back( point( pt1.x + dx_2 - dy, pt1.y + dx + dy_2 ) );
		points2.push_back( point( pt1.x + dx_2 + dy, pt1.y - dx + dy_2 ) ); 
		points1.push_back( point( pt1.x + dx - dy_2, pt1.y + dx_2 + dy ) );
		points2.push_back( point( pt1.x + dx + dy_2, pt1.y - dx_2 + dy ) ); 
		}
		break;

	default:
		throw std::runtime_error( "Unimplemented path-end when converting to polygon." );
	}

	// copy the points
	std::copy( points1.begin(), points1.end(), std::back_inserter( p ) );
	std::copy( points2.rbegin(), points2.rend(), std::back_inserter( p ) );
}

bool path_shape::is_dot() const
{
	assert( !empty() );
	
	// an easy test to start
	if ( size()==1 ) return true;
	
	// we are looking for two unique points
	const_iterator lp = begin();
	// this is our first unique points
	point a = *lp++;
	// move through rest of the points
	do {
		// is this point different
		if ( a != *lp ) return false;
		// move to next point
		++lp;
	} while ( lp!=end() );
		
	// we did not find two unique points
	return true;
}
