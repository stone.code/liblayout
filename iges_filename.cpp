/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2012 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout.h"
#include "layout_export.h"
#include <fstream>
#include <stdexcept>

using namespace liblayout;

void	iges::write( layout const& view, layer_id layer, char const* filename )
{
	std::ofstream file;

	// create the file
	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	iges::write( view, layer, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the DXF file." );
	}
}

void	iges::write( layout const& view, char const* filename )
{
	std::ofstream file;

	// create the file
	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	iges::write( view, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the DXF file." );
	}
}
