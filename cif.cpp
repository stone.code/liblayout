/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2007 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Export to CalTech Interchange Format (CIF).
**
*/

#pragma warning (disable:4786)

#include "layout.h"
#include "layout_shapes.h"
#include <assert.h>
#include <ctype.h>
#include <exception>
#include <stack>
#include <stdlib.h>
#include <stdexcept>
#include <map>

using namespace liblayout;
using namespace std;

#define INVALID_CIF "Invalid CIF file.  "

layer_id cif::get_next_layer_id( cif::map_name_id const& m )
{
	// Find the maximum layer_id in the map
	layer_id max_id = 0;
	cif::map_name_id::const_iterator lp;
	for ( lp = m.begin(); lp != m.end(); ++lp )
	{
		layer_id tmp = lp->second;
		if ( tmp > max_id ) max_id = tmp;
	}
	// max_id should now hold the maximum currently used id number

	// We need the next id
	return max_id + 1;
}

void cif::update_id_names( map_id_name& lhs, map_name_id const& rhs )
{
	map_name_id::const_iterator lp;

	// Check all of the names in the map
	for ( lp = rhs.begin(); lp != rhs.end(); ++lp ) {
		// Can we find this id in the lhs map?
		map_id_name::iterator i = lhs.find( lp->second );
		if ( i == lhs.end() )
			lhs[ lp->second ] = lp->first;
	}
}

static void get_token( istream& in, string& token )
{
	token.erase();
	int	ch;

	ch = in.get();
	while( ch != ';' && in.good() )
	{
		token += toupper( ch );
		ch = in.get();
	};

	while ( isspace( token[0] ) ) 
		token.erase( token.begin() );
}

static	char const*	advance_cif_integer( char const* lex, long& value )
{
	assert( lex );
	value = atol(lex);
	if ( *lex=='+' || *lex=='-' ) ++lex;
	while ( isdigit(*lex) ) ++lex;
	return lex;
}

static	char const*	advance_cif_space( char const* lex )
{
	assert( lex );
	while ( isspace(*lex) ) ++lex;
	return lex;
}

static	char const* advance_cif_text( char const* lex )
{
	assert( lex );
	while ( isalpha( *lex ) ) ++lex;
	return lex;
}

static	char const*	advance_cif_point( char const* lex, point& value )
{
	assert( lex );

	lex = advance_cif_integer( lex, value.x );
	lex = advance_cif_space( lex );
	if ( *lex == ',' )
	{
		++lex;
		lex = advance_cif_space( lex );
	}
	lex = advance_cif_integer( lex, value.y );
	return lex;
}

static void interpret_comment( char const* lex, string& text )
{
	assert( lex[0] == '(' );
	text = ++lex;
	// drop last character if bracket
	if ( text[ text.length()-1 ] == ')' ) text.erase( text.begin() + text.length() - 1 );
}

static void interpret_9( char const* lex, string& name )
{
	assert( lex[0]=='9' );
	
	while( isdigit( *lex ) ) ++lex;
	lex = advance_cif_space( lex );

	char const* lex2 = lex;
	while ( isalnum( *lex2 ) || ispunct( *lex2 ) ) ++lex2;

	name.assign( lex, lex2 );
}

static void interpret_ds( char const* lex, int& id, long& scale_n, long& scale_d )
{
	long id_;

	assert( lex[0]=='D' );
	lex = advance_cif_text( lex );
	lex = advance_cif_space( lex );
	// some applications insert a pound symbol instead of using a space
	if ( *lex=='#' ) {
		++lex;
		lex = advance_cif_space( lex );
	};
	lex = advance_cif_integer( lex, id_ ); id = id_;
	lex = advance_cif_space( lex );
	lex = advance_cif_integer( lex, scale_n );
	lex = advance_cif_space( lex );
	// some applications insert a slash instead of using a space
	if ( *lex=='/' ) {
		++lex;
		lex = advance_cif_space( lex );
	}
	lex = advance_cif_integer( lex, scale_d );
}

static void interpret_b( char const* lex, rect& r )
{
	long w, h;
	point p(0,0 /* dummy values */);

	assert( lex[0]=='B' );

	lex = advance_cif_text( lex );
	lex = advance_cif_space( lex );
	lex = advance_cif_integer( lex, w );
	lex = advance_cif_space( lex );
	lex = advance_cif_integer( lex, h );
	lex = advance_cif_space( lex );
	lex = advance_cif_point( lex, p );

	// Check for negative width or height.
	// They shouldn't be present, but you never know.
	if ( w < 0 ) w = -w;
	if ( h < 0 ) h = -h;

	// Integer division by two, to avoid rounding errors, need to make sure
	// use of width and height are explicit
	coord_t left = p.x - w/2;
	coord_t bottom = p.y - h/2;

	r = rect( left, bottom, left + w, bottom + h );
	assert( r.valid() );
}

static void interpret_c( char const* lex, long& layout_id, liblayout::transform& t )
{
	assert( lex[0]=='C' );

	t = liblayout::transform(R0);

	lex = advance_cif_text( lex );
	lex = advance_cif_space( lex );
	// some applications write out a '#' symbol instead of a space
	if ( *lex=='#' ) {
		++lex;
		lex = advance_cif_space( lex );
	}
	lex = advance_cif_integer( lex, layout_id );
	lex = advance_cif_space( lex );
	if ( layout_id==0 )
		throw std::runtime_error( INVALID_CIF "Invalid cell reference." );

	// apply transforms
	while ( *lex && *lex!=';' )
	{
	switch ( *lex )
	{
		case 'M':
			++lex;
			if ( *lex == 'X' ) 
			{
				lex = advance_cif_text( lex );
				// CIF MX means reflect the x-axis
				// y-coords are unchanged
				t.reflect_x();
			}
			else if ( *lex == 'Y' )
			{
				lex = advance_cif_text( lex );
				// CIF MY means reflect the y-axis
				// x-coords are unchanged
				t.reflect_y();
			}
			else
			{
				throw std::runtime_error( INVALID_CIF "Unknown reflection specification." );
			}
			break;

		case 'R':
			{
				lex = advance_cif_text( lex );
				lex = advance_cif_space( lex );
				long c;
				lex = advance_cif_integer( lex, c );
				lex = advance_cif_space( lex );
				long s;
				lex = advance_cif_integer( lex, s );
				t.rotate( c, s );
			}
			break;
		case 'T':
			{ 
				lex = advance_cif_text( lex );
				lex = advance_cif_space( lex );
				point pt(0,0 /*dummy values*/ );
				lex = advance_cif_point( lex, pt );
				t.translate( pt );
			}
			break;
				
		default:
			throw std::runtime_error( INVALID_CIF "Unknown transformation specification." );
		}

		// advance past any space
		lex = advance_cif_space( lex );
	}

	assert( t.is_manhattan() );
}

static void interpret_l( char const* lex_original, string& name )
{
	assert( lex_original[0]=='L' );
	
	char const* lex = advance_cif_text( lex_original );
	lex = advance_cif_space( lex );
	
	// check to see if we have run to the end of the token
	if ( !*lex ) {
		// we have run to the end of the token
		// assume that the 'L' command and layer name
		// have been combined
		lex = lex_original + 1;
	}
	
	// rest of token is the layer name
	char const* lex2 = lex;
	while ( isalnum( *lex2 ) ) ++lex2;
	name.assign( lex, lex2 );		
	
	// check that the name is valid
	if ( name.empty() )
		throw std::runtime_error( INVALID_CIF "Layer name is empty." );
}

static void interpret_p( char const* lex, vector<point>& p )
{
	assert( lex[0]=='P' );
	assert( p.empty() );

	
	lex = advance_cif_text( lex );
	lex = advance_cif_space( lex );

	do {
		point pt( 0,0 /*dummy values*/);
		
		lex = advance_cif_point( lex, pt );
		lex = advance_cif_space( lex );

		p.push_back( pt );

		// some CIF files have points doubled up
		if ( p[p.size()-2] == p[p.size()-1] )
		{
			p.pop_back();
		}

	} while ( *lex && *lex != ';' );

	// check if last point is equal to first point
	if ( p.front() == p.back() )
	{
		p.pop_back();
	}
}

static void interpret_r( char const* lex, coord_t& diameter, point& center )
{
	assert( lex[0]=='R' );
	
	lex = advance_cif_text( lex );
	lex = advance_cif_space( lex );
	lex = advance_cif_integer( lex, diameter );
	lex = advance_cif_space( lex );
	lex = advance_cif_point( lex, center );
}

static void interpret_w( char const* lex, vector<point>& p, long& width )
{
	assert( lex[0]=='W' );
	assert( p.empty() );

	
	lex = advance_cif_text( lex );
	lex = advance_cif_space( lex );
	lex = advance_cif_integer( lex, width );
	lex = advance_cif_space( lex );

	do {
		point pt(0,0 /* dummy values */);
		
		lex = advance_cif_point( lex, pt );
		lex = advance_cif_space( lex );

		p.push_back( pt );

		// some CIF files have points doubled up
		if ( p[p.size()-2] == p[p.size()-1] )
		{
			p.pop_back();
		}

	} while ( *lex && *lex != ';' );
}

static void apply_p( int layer_id, layout* view, long scale_n, long scale_d, vector<point>& p )
{
	if ( p.size() < 3 ) {
		throw std::runtime_error(
			INVALID_CIF "Polygon has fewer than three distinct points." );
	}
	
	// apply scaling
	if ( scale_n != scale_d )
	{
		for ( vector<point>::iterator lp = p.begin(); lp != p.end(); ++lp )
		{
			lp->x = lp->x * scale_n / scale_d;
			lp->y = lp->y * scale_n / scale_d;
		}
	}

	// create shape
	view->insert( layer_id, p );
}

static void apply_w( int layer_id, layout* view, long scale_n, long scale_d, vector<point>& p, long width )
{
	if ( p.size() < 2 ) {
		throw std::runtime_error(
			INVALID_CIF "Path has fewer than two distinct points." );
	}
	
	// apply scaling
	if ( scale_n != scale_d )
	{
		width = width * scale_n / scale_d;
		for ( vector<point>::iterator lp = p.begin(); lp != p.end(); ++lp )
		{
			lp->x = lp->x * scale_n / scale_d;
			lp->y = lp->y * scale_n / scale_d;
		}
	}

	// create shape
	if ( width==0 ) width = 1;
	view->insert( layer_id, p, width, path_end_square );
}

layout*	cif::import( std::list<layout>& layouts, cif::map_name_id const& layer_map, 
	cif::on_comment_handler on_comment, void* on_comment_userdata, 
	std::istream& in )
{
	string			token;
	layout*			ret;
	stack<layout*>		view;
	stack<long>		scale_n;
	stack<long>		scale_d;
	int			layer_id = 0;
	map<int,layout*>	layout_ids;
	long			composites = 0;

	// create top level view
	layouts.push_back( layout() );
	view.push( &layouts.back() );
	scale_n.push( 1 );
	scale_d.push( 1 );
	ret = view.top();

	get_token( in, token );

	while ( !token.empty() )
	{
		switch ( token[0] )
		{
		case '(':
			// Comment token
			// There is nothing to do unless we have some user specified action
			if ( on_comment ) {
				std::string text;
				interpret_comment( token.c_str(), text );
				(*on_comment)( on_comment_userdata, text.c_str() );
			}
			// ignore comment
			break;

		case 'D':
			switch ( token[1] )
			{
			case 'S':
				{
					int		id;
					long	this_scale_n, this_scale_d;

					// interpret the token
					interpret_ds( token.c_str(), id, this_scale_n, this_scale_d );

					// create a new instance
					layouts.push_back( layout() );
					view.push( &layouts.back() );
					scale_n.push( this_scale_n );
					scale_d.push( this_scale_d );
					layout_ids.insert( std::make_pair( id, view.top() ) );
					layer_id = 0;

					// update counter
					++composites;
				};
				break;

			case 'F':
				{
					// stop with new instance
					view.pop();
					scale_n.pop();
					scale_d.pop();
					layer_id = 0;
				}
				break;

			default:
				// don't know this token
				{
					std::string message( "Unknown token (" );
					message.append( token );
					message.append( ") found during CIF import." );
					throw std::runtime_error( message );
				}
				break;
			};
			break;

		case 'E':
			// and that is the end of the cif file input
			break;

		case 'L':
			{
				// get the layer name
				std::string name;
				interpret_l( token.c_str(), name );
				// get the layer ID
				cif::map_name_id::const_iterator f = layer_map.find( name );
				if ( f == layer_map.end() ) {
					std::string message( "Unknown layer name (" );
					message.append( name );
					message.append( ") found during CIF import." );
					throw std::runtime_error( message );
				}
				assert( f != layer_map.end() );
				layer_id = (*f).second;
			}
			break;
                
		case 'B':
			// create a box
			{
				// get dimensions of the box from token
				rect r(0,0,0,0 /*dummy values*/);
				interpret_b( token.c_str(), r );
				// apply scaling
				r.x1 = r.x1 * scale_n.top() / scale_d.top();
				r.y1 = r.y1 * scale_n.top() / scale_d.top();
				r.x2 = r.x2 * scale_n.top() / scale_d.top();
				r.y2 = r.y2 * scale_n.top() / scale_d.top();
				// create shape
				view.top()->insert( layer_id, r );
			}
			break;

		case 'P':
			{
				// get dimensions of polygon from token
				vector<point> p;
				interpret_p( token.c_str(), p );
				// add the polygon to the layout
				apply_p( layer_id, view.top(), scale_n.top(), scale_d.top(), p );
			}
			break;
			
		case 'R':
			{
				// get parameters of circle from token
				coord_t diameter;
				point center(0,0 /*dummy values*/);
				interpret_r( token.c_str(), diameter, center );
				// apply scaling
				diameter = diameter * scale_n.top() / scale_d.top();
				center.x = center.x * scale_n.top() / scale_d.top();
				center.y = center.y * scale_n.top() / scale_d.top();
				// create the shape
				view.top()->insert( layer_id, diameter/2, center );
			}
			break;
			
		case 'W':
			{
				// get dimensions of path from token
				long width;
				vector<point> p;
				interpret_w( token.c_str(), p, width );
				// add the path to the layout
				apply_w( layer_id, view.top(), scale_n.top(), scale_d.top(), p, width );
			}
			break;

		case 'C':
			{
				long		layout_id;
				transform	t;

				interpret_c( token.c_str(), layout_id, t );
				// apply scaling
				if ( scale_n.top() != scale_d.top() )
				{
					t(0,2) = t(0,2) * scale_n.top() / scale_d.top();
					t(1,2) = t(1,2) * scale_n.top() / scale_d.top();
				}
				// create instance
				map<int,layout*>::iterator cell = layout_ids.find( layout_id );
				if ( cell != layout_ids.end() )
					view.top()->insert( cell->second, t );
				else
					throw std::runtime_error( INVALID_CIF "Cell reference does not indicate any prior cell." );
			}
			break;

		case '9':
			{
				// get the layer name
				std::string name;
				interpret_9( token.c_str(), name );
				view.top()->set_name( name );
			}
			break;

		default:
			// don't know this token
			{
				std::string message( "Unknown token (" );
				message.append( token );
				message.append( ") found during CIF import." );
				throw std::runtime_error( message );
			}
			break;
		}

		// get the next token
		get_token( in, token );
	};

	// check if layout is empty
	if ( ret->empty() && composites > 0 )
	{
		// check if there is a single, unreferenced layout
		if ( composites > 0 && &layouts.back() != ret )
		{
			transform t(R0);
			ret->insert( & layouts.back(), t );
		}
		else
		{
			throw std::runtime_error( "Layout is empty!" );
		}
	}

	assert( ! ret->empty() );
	return ret;
}

layout*	cif::import( std::list<layout>& layouts, cif::map_name_id& layer_map, 
	cif::on_comment_handler on_comment, void* on_comment_userdata, 
	cif::on_layer_handler on_layer, void* on_layer_userdata,
	std::istream& in )
{
	string			token;
	layout*			ret;
	stack<layout*>		view;
	stack<long>		scale_n;
	stack<long>		scale_d;
	int			layer_id = 0;
	map<int,layout*>	layout_ids;
	long			composites = 0;
	
	if ( !on_layer ) throw std::invalid_argument( "Parameter 'on_layer' can not be null in call to cif::import." );

	// create top level view
	layouts.push_back( layout() );
	view.push( &layouts.back() );
	scale_n.push( 1 );
	scale_d.push( 1 );
	ret = view.top();

	get_token( in, token );

	while ( !token.empty() )
	{
		switch ( token[0] )
		{
		case '(':
			// Comment token
			// There is nothing to do unless we have some user specified action
			if ( on_comment ) {
				std::string text;
				interpret_comment( token.c_str(), text );
				(*on_comment)( on_comment_userdata, text.c_str() );
			}
			// ignore comment
			break;

		case 'D':
			switch ( token[1] )
			{
			case 'S':
				{
					int		id;
					long	this_scale_n, this_scale_d;

					// interpret the token
					interpret_ds( token.c_str(), id, this_scale_n, this_scale_d );

					// create a new instance
					layouts.push_back( layout() );
					view.push( &layouts.back() );
					scale_n.push( this_scale_n );
					scale_d.push( this_scale_d );
					layout_ids[ id ] = view.top();
					layer_id = 0;

					// update counter
					++composites;
				};
				break;

			case 'F':
				{
					// stop with new instance
					view.pop();
					scale_n.pop();
					scale_d.pop();
					layer_id = 0;
				}
				break;

			default:
				// don't know this token
				{
					std::string message( "Unknown token (" );
					message.append( token );
					message.append( ") found during CIF import." );
					throw std::runtime_error( message );
				}
				break;
			};
			break;

		case 'E':
			// and that is the end of the cif file input
			break;

		case 'L':
			{
				// get the layer name
				std::string name;
				interpret_l( token.c_str(), name );
				// get the layer ID
				cif::map_name_id::const_iterator f = layer_map.find( name );
				if ( f == layer_map.end() ) {
					// Create a new entry for the unknown layer name
					liblayout::layer_id id = get_next_layer_id( layer_map );
					assert( id != 0 );
					layer_map[ name ] = id;
					f = layer_map.find( name );
					// Inform caller of update
					on_layer( on_layer_userdata, f->first.c_str(), f->second );
				}
				assert( f != layer_map.end() );
				layer_id = (*f).second;
			}
			break;
                
		case 'B':
			// create a box
			{
				// get dimensions of the box from token
				rect r(0,0,0,0 /*dummy values*/);
				interpret_b( token.c_str(), r );
				// apply scaling
				r.x1 = r.x1 * scale_n.top() / scale_d.top();
				r.y1 = r.y1 * scale_n.top() / scale_d.top();
				r.x2 = r.x2 * scale_n.top() / scale_d.top();
				r.y2 = r.y2 * scale_n.top() / scale_d.top();
				// create shape
				view.top()->insert( layer_id, r );
			}
			break;

		case 'P':
			{
				// get dimensions of polygon from token
				vector<point> p;
				interpret_p( token.c_str(), p );
				// add the polygon to the layout
				apply_p( layer_id, view.top(), scale_n.top(), scale_d.top(), p );
			}
			break;
			
		case 'R':
			{
				// get parameters of circle from token
				coord_t diameter;
				point center(0,0 /*dummy values*/);
				interpret_r( token.c_str(), diameter, center );
				// apply scaling
				diameter = diameter * scale_n.top() / scale_d.top();
				center.x = center.x * scale_n.top() / scale_d.top();
				center.y = center.y * scale_n.top() / scale_d.top();
				// create the shape
				view.top()->insert( layer_id, diameter/2, center );
			}
			break;
			
		case 'W':
			{
				// get dimensions of path from token
				long width;
				vector<point> p;
				interpret_w( token.c_str(), p, width );
				// add the path to the layout
				apply_w( layer_id, view.top(), scale_n.top(), scale_d.top(), p, width );
			}
			break;

		case 'C':
			{
				long		layout_id;
				transform	t;

				interpret_c( token.c_str(), layout_id, t );
				// apply scaling
				if ( scale_n.top() != scale_d.top() )
				{
					t(0,2) = t(0,2) * scale_n.top() / scale_d.top();
					t(1,2) = t(1,2) * scale_n.top() / scale_d.top();
				}
				// create instance
				map<int,layout*>::iterator cell = layout_ids.find( layout_id );
				if ( cell != layout_ids.end() )
					view.top()->insert( cell->second, t );
				else
					throw std::runtime_error( INVALID_CIF "Cell reference does not indicate any prior cell." );
			}
			break;

		case '9':
			{
				// get the layer name
				std::string name;
				interpret_9( token.c_str(), name );
				view.top()->set_name( name );
			}
			break;

		default:
			// don't know this token
			{
				std::string message( "Unknown token (" );
				message.append( token );
				message.append( ") found during CIF import." );
				throw std::runtime_error( message );
			}
			break;
		}

		// get the next token
		get_token( in, token );
	};

	// check if layout is empty
	if ( ret->empty() && composites > 0 )
	{
		// check if there is a single, unreferenced layout
		if ( composites > 0 && &layouts.back() != ret )
		{
			transform t(R0);
			ret->insert( & layouts.back(), t );
		}
		else
		{
			throw std::runtime_error( "Layout is empty!" );
		}
	}

	assert( ! ret->empty() );
	return ret;
}

cif::strings	cif::import_layers( std::istream& in )
{
	std::set<string> layers;
	string	token;
	get_token( in, token );

	while ( !token.empty() )
	{
		if ( token[0] == 'L' )
		{
			// get the layer name
			std::string name;
			interpret_l( token.c_str(), name );
			// add to set
			layers.insert( name );
		}
		else if ( token[0] == 'E' )
		{
			// and that is the end of the cif file input
			// do nothing
		}

		// get the next token
		get_token( in, token );
	};

	strings ret;
	std::set<std::string>::const_iterator lp;
	for ( lp = layers.begin(); lp != layers.end(); ++lp )
	{
		ret.push_back( *lp );
	}
	return ret;
}

bool	cif::is_file_cif( std::istream& in )
{
	int ch;

	while ( (ch = in.get()) && in.good() )
	{
		// check for spaces
		if ( isspace( ch ) ) continue;

		// check for command
		switch ( toupper( ch ) )
		{
		case '(':
		case '9':
		case 'B':
		case 'C':
		case 'L':
		case 'P':
		case 'R':
		case 'W':
			break;

		case 'D':
			ch = in.get();
			if ( ch != 'S' && ch != 'F' ) 
				return false;
			break;

		case 'E':
			return true;

		default:
			return false;

		}

		// skip to semicolon
		do {
			ch = in.get();
			if ( !in.good() ) return false;
		} while ( ch != ';' );
	};

	return true;
}
