/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Export to CalTech Interchange Format (CIF).
**
*/

#pragma warning (disable:4786)

#include "layout.h"
#include "layout_shapes.h"
#include <algorithm>
#include <assert.h>
#include <stdexcept>
#include <map>

using namespace liblayout;
using namespace std;

typedef char itoa_buffer_t[32];

static char* my_itoa(int value, itoa_buffer_t workArea )
{
	char buffer[31];
	char* ptr;
	char* ptr2;
	
	if ( value==0 ) {
		workArea[0] = '0';
		workArea[1] = '\0';
		return workArea;
	};
	if ( value<0 ) {
		workArea[0] = '-';
		value = -value;
	};
	
	ptr = buffer;
	while ( value ) {
		*ptr++ = "0123456789abcdef"[value % 10];
		assert( ptr < buffer + sizeof(buffer) + 1 );
		value /= 10;
	};

	/* reverse characters */
	ptr2 = workArea;
	if ( *ptr2=='-' ) 
		++ptr2;
	while (ptr>buffer) 
		*ptr2++ = *--ptr;
	*ptr2 = '\0';
	assert( ptr2 < workArea + sizeof(workArea) );
	return workArea;
}

static void export_transform( liblayout::transform const& t, ostream& out )
{
	manhattan_transform mt = t.get_manhattan_transform();
	switch ( mt )
	{
	case R0:
		// do nothing
		break;
	case R90:
		out << " R 0 1";
		break;
	case R180:
		out << " R -1 0";
		break;
	case R270:
		out << " R 0 -1";
		break;
	case MX:
		out << " MX";
		break;
	case MY:
		out << " MY";
		break;
	case MXR90:
		out << " MX R 0 1";
		break;
	default:
		out << " ??";
		break;
	}

	if ( t(0,2) != 0 || t(1,2) != 0 )
	{
		out << " T" << t(0,2) << ',' << t(1,2);
	}
}

static void export_layer( layer_id layer, cif::map_id_name& layer_map, bool keep_unknown_layers, std::ostream& out )
{
	if ( layer_map.find( layer ) == layer_map.end() )
	{
		if ( keep_unknown_layers )
		{
			std::string n( "unknown_layer_" );
			itoa_buffer_t buffer;
			n.append( my_itoa( layer, buffer ) );
			layer_map[layer] = n;
		}
		else
		{
			itoa_buffer_t buffer;

			std::string message( "Unknown layer id (" );
			message.append( my_itoa( layer, buffer ) );
			message.append( ")." );
			throw std::runtime_error( message );
		}
	}
	out << "L " << layer_map[ layer ] << ';' << std::endl;
}

static void export_shape( shape::imp const* view, std::ostream& out )
{
	// write out shape
	rect_shape const* box;
	path_shape const* path;
	polygon_shape const* poly;
	
	if ( box = dynamic_cast<rect_shape const*>( view ) )
	{
		out << "B " << box->width() << ' ' << box->height() << ' ' << box->center_x() << ',' << box->center_y() << ';' << std::endl;
	}
	else if ( path = dynamic_cast<path_shape const*>( view ) )
	{
		out << "W " << path->width;
		path_shape::const_iterator lp;
		for ( lp = path->begin(); lp != path->end(); ++lp )
		{
			out << ' ' << lp->x << ',' << lp->y;
		}
		out << ';' << std::endl;
	}
	else if ( poly = dynamic_cast<polygon_shape const*>( view ) )
	{
		out << "P ";
		polygon_shape::const_iterator lp;
		for ( lp = poly->begin(); lp != poly->end(); ++lp )
		{
			out << ' ' << lp->x << ',' << lp->y;
		}
		out << ';' << std::endl;
	}
	else
	{
		// convert to a polygon
		polygon p;
		view->get_polygon( p );
		out << "P ";
		polygon_shape::const_iterator lp;
		for ( lp = p.begin(); lp != p.end(); ++lp )
		{
			out << ' ' << lp->x << ',' << lp->y;
		}
		out << ';' << std::endl;
	}
}

static void export_sub( composite const view, cif::map_id_name& layer_map, map<void const*,long>& layout_ids, bool keep_unknown_layers, ostream& out )
{
	std::vector<shape> shapes;
	
	// get all of the shapes for this composite
	view.get_shapes( shapes );
	std::sort( shapes.begin(), shapes.end() );
	
	// handle the case of an empty composite
	if ( shapes.empty() ) {
		out << "DS " << layout_ids[view.get()] << " 1 1;\n";
		out << "DF;\n";
	}
	
	// start the cell
	layer_id layer = shapes.front().layer();
	out << "DS " << layout_ids[view.get()] << " 1 1;\n";
	export_layer( layer, layer_map, keep_unknown_layers, out );

	// for each shape
	std::vector<shape>::const_iterator lp;
	for ( lp = shapes.begin(); lp != shapes.end(); ++lp )
	{
		// update the shape if necessary
		if ( lp->layer() != layer )
		{
			layer = lp->layer();
			export_layer( layer, layer_map, keep_unknown_layers, out );
		}
		// write out this shape
		export_shape( lp->get(), out );
	}

	// and close the cell
	out << "DF;\n";
}

static void export_sub( layout const& view, cif::map_id_name& layer_map, map<layout const*,long>& layout_ids, bool keep_unknown_layers, ostream& out )
{
	layer_id layer;

	// make sure that any referenced layouts are written first
	// these will be buried as a layout composite
	layout::const_composite_iterator clp;
	for ( clp = view.composites_begin(); clp != view.composites_end(); ++clp )
	{
		// is the composite a layout reference?
		layout_composite const* view = dynamic_cast<layout_composite const*>( clp->get() );
		if ( !view ) continue;
		
		// if the layout not already written to file,
		// we need to add it now
		if ( layout_ids.find( view->get_layout() ) == layout_ids.end() )
		{
			assert( view->get_layout() );
			export_sub( *view->get_layout(), layer_map, layout_ids, keep_unknown_layers, out );
			assert( layout_ids.find( view->get_layout() ) != layout_ids.end() );
		}
	}

	// start writing out the geometry for this layout
	layout_ids[ &view ] = layout_ids.size() + 1;
	out << "DS " << layout_ids[&view] << " 1 1;" << std::endl;

	// write out the shapes
	if ( view.shapes_size() > 0 )
	{
		// cheat to sort the shapes
		// this will mean fewer layer changes
		const_cast<layout*>(&view)->shapes_sort();
		layer = view.shapes_front().layer();
		export_layer( layer, layer_map, keep_unknown_layers, out );
	};

	layout::const_shape_iterator slp;
	for ( slp = view.shapes_begin(); slp != view.shapes_end(); ++slp )
	{
		// update the shape if necessary
		if ( slp->layer() != layer )
		{
			layer = slp->layer();
			export_layer( layer, layer_map, keep_unknown_layers, out );
		}
		// write out this shape
		export_shape( slp->get(), out );
	}

	// write out composites
	for ( clp = view.composites_begin(); clp != view.composites_end(); ++clp )
	{
		// is the composite a layout reference?
		layout_composite const* view = dynamic_cast<layout_composite const*>( clp->get() );
		if ( view )
		{
			assert( layout_ids.find( view->get_layout() ) != layout_ids.end() );
			if ( view->get_rows() == 1 && view->get_cols() == 1 )
			{
				out << "C " << layout_ids[ view->get_layout() ];
				export_transform( view->get_transform(), out );
				out << ';' << std::endl;
			}
			else
			{
				int lpx,lpy;

				for ( lpx = 0; lpx < view->get_cols(); ++lpx )
				for ( lpy = 0; lpy < view->get_rows(); ++lpy )
				{
					out << "C " << layout_ids[ view->get_layout() ];
					liblayout::transform t = view->get_transform();
					coord_t x = lpx * view->get_delta_c().x + lpy * view->get_delta_r().x;
					coord_t y = lpx * view->get_delta_c().y + lpy * view->get_delta_r().y;
					t.translate( x, y );
					export_transform( view->get_transform(), out );
					out << ';' << std::endl;
				}
			}
		}
		else
		{
			std::vector<shape> shapes;
			clp->get_shapes( shapes );

			std::vector<shape>::const_iterator slp;
			for ( slp = shapes.begin(); slp != shapes.end(); ++slp )
			{
				// update the shape if necessary
				if ( slp->layer() != layer )
				{
					layer = slp->layer();
					export_layer( layer, layer_map, keep_unknown_layers, out );
				}
				// write out this shape
				export_shape( slp->get(), out );
			}
		}
	}

	out << "DF;" << std::endl;
}

void	cif::write( layout const& view, cif::map_id_name& layer_map, bool keep_unknown_layers, std::ostream& out )
{
	map<layout const*,long>	layout_ids;
	out << "(CIF output written by " PACKAGE_NAME " " PACKAGE_VERSION ");" << std::endl;

	// check if we are looking at a shell view
	long shapes = view.shapes_size();
	long composites = view.composites_size();
	if ( shapes == 0 && composites == 1)
	{
		layout_composite const * l = dynamic_cast<layout_composite const*>( view.composites_front().get() );
		if ( l ) {
			assert( l->get_layout() );
			export_sub( *l->get_layout(), layer_map, layout_ids, keep_unknown_layers, out );
		}
		else {
			export_sub( view, layer_map, layout_ids, keep_unknown_layers, out );
		}
	}
	else
	{
		export_sub( view, layer_map, layout_ids, keep_unknown_layers, out );
	}

	
	out << "C " << layout_ids.size() << ';' << std::endl;
	out << "E;" << std::endl;
}
