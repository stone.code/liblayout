/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2008 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains wrappers for the ANSYS export routines.  These
** functions overload equivalent functions, except that they take a 
** null terminated string, open a file, and then replace the filename
** with the iostream.
**
*/

#include "layout_export.h"
#include <fstream>
#include <stdexcept>

using namespace liblayout;

void ansys::write( layout const& view, layer_id layer, char const* filename )
{
	std::ofstream file;

	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	ansys::write( view, layer, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the cif file." );
	}
	file.close();
}

void ansys::write( layout const& view, char const* filename )
{
	std::ofstream file;

	file.open( filename, std::ios::out );
	if ( !file )
	{
		throw std::runtime_error( std::string( "Could not open the following file for writing:  " ) + filename );
	}
	ansys::write( view, file );
	if ( !file )
	{
		throw std::runtime_error( "Error writing out the cif file." );
	}
	file.close();
}
