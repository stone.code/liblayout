/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2004-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** Description not written.
**
*/

#include "layout_shapes.h"
#include <assert.h>
#include <sstream>
#include <stdexcept>

using namespace liblayout;

layout_name_composite::layout_name_composite( char const* name, transform const& t )
: name_(name), t_(t), delta_r(0,1), delta_c(1,0)
{
	cols = 1;
	rows = 1;
	assert( valid() );
}

layout_name_composite::layout_name_composite( char const* name, transform const& t, int rows_, int cols_, point delta_r_, point delta_c_ )
: name_(name), t_(t), delta_r( delta_r_), delta_c( delta_c_ )
{
	cols = cols_;
	rows = rows_;
	assert( valid() );
}

layout_name_composite::layout_name_composite( std::string const& name, transform const& t, int rows_, int cols_, point delta_r_, point delta_c_ )
: name_(name), t_(t), delta_r( delta_r_), delta_c( delta_c_ )
{
	cols = cols_;
	rows = rows_;
	assert( valid() );
}

rect layout_name_composite::bounds() const
{
	throw std::runtime_error( "Can not find bounds on name referenced layout." );
}

int layout_name_composite::contains_point( point pt ) const
{
	throw std::runtime_error( "Can not find contains point on name referenced layout." );
}

composite::imp*	layout_name_composite::dereference( std::list<layout>& layouts) const
{
	assert( valid() );

	// get the actual layout
	layout* l = get_layout( layouts );
	if ( !l ) {
		std::stringstream msg;
		msg << "Could not locate the requested layout.  The name \"" << name_ << "\" was not present." << '\0';
		throw std::runtime_error( msg.str() );
	}

	// create a new composite
	layout_composite* newptr = new layout_composite( l, t_, rows, cols, delta_r, delta_c );
	assert( newptr );
	assert( newptr->valid() );

	return newptr;
}

composite::imp* layout_name_composite::eval( transform const& t ) const
{
	return new layout_name_composite( name_, t * t_, rows, cols, delta_r, delta_c );
}

void layout_name_composite::get_layers( std::set<layer_id>& lhs ) const
{
	throw std::runtime_error( "Can not get layers from name referenced layout." );
}

layout* layout_name_composite::get_layout( std::list<layout>& layouts ) const
{
	std::list<layout>::iterator lp;
	for ( lp = layouts.begin(); lp != layouts.end(); ++lp )
	{
		if ( lp->get_name() == name_ ) return &* lp;
	}

	return 0;
}

void layout_name_composite::get_shapes( std::vector<shape>& lhs ) const
{
	throw std::runtime_error( "Can not get layers from name referenced layout." );
}

void layout_name_composite::get_shapes( std::vector<shape>& lhs, layer_id layer ) const
{
	throw std::runtime_error( "Can not get layers from name referenced layout." );
}

bool layout_name_composite::has_bounds() const
{
	throw std::runtime_error( "Can not check for bounds from name referenced layout." );
}

void layout_name_composite::set_cols( int r )
{
	if ( r < 1 ) throw std::logic_error( "In a layout array composite, the number of rows must be at least 1." );
	rows = r;
}

void layout_name_composite::set_rows( int c )
{
	if ( c < 1 ) throw std::logic_error( "In a layout array composite, the number of columns must be at least 1." );
	cols = c;
}

void layout_name_composite::set_spacing( point dr, point dc )
{
	delta_r = dr;
	delta_c = dc;
}

bool layout_name_composite::valid() const
{
	if ( name_.empty() ) return false;
	if ( cols<=0 ) return false;
	if ( rows<=0 ) return false;
	if ( !t_.valid() ) return false;
	if ( rows>1 && delta_r.x==0 && delta_r.y==0 ) return false;
	if ( cols>1 && delta_c.x==0 && delta_c.y==0 ) return false;
	
	return true;
}
